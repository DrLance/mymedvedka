export default [
  {
    id: 1,
    type_event: 5,
    name: 'Рекомендованное посещение врачей',
    pathName: 'visits'
  },
  {
    id: 2,
    type_event: 6,
    name: 'Нормы анализов, моча',
    pathName: 'pee'
  },
  {
    id: 3,
    type_event: 7,
    name: 'Нормы анализов, кровь по возрастам',
    pathName: 'blood'
  },
  {
    id: 4,
    type_event: 8,
    name: 'Таблица прикорма по месяцам',
    pathName: 'month'
  },
  {
    id: 5,
    type_event: 9,
    name: 'Нормы сна',
    pathName: 'sleep'
  },
  {
    id: 6,
    type_event: 10,
    name: 'Нормы по зубам',
    pathName: 'tooth'
  },
  {
    id: 7,
    type_event: 11,
    name: 'Таблица Размер одежды по росту',
    pathName: 'size'
  },
  {
    id: 8,
    type_event: 12,
    name: 'Таблица размеров обуви',
    pathName: 'shoes'
  },
  {
    id: 9,
    type_event: 13,
    name: 'Таблица нормы роста и веса',
    pathName: 'weight'
  }
];
