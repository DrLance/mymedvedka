import moment from 'moment';
import { Notifications, Permissions } from 'expo';
import _ from 'lodash';

import { tableTitle, rowData } from '../data/dataWeight';
import dataVacination from '../data/dataVacinations';

moment.locale('ru');

const localNotification = {
  title: '',
  body: '',
  ios: { sound: true },
  android: {
    sound: true,
    priority: 'high',
    sticky: false,
    vibrate: true,
    icon: '../../assets/images/icon.png'
  }
};

Notifications.addListener(e => {
  if (e.origin === 'selected') {
    Notifications.cancelScheduledNotificationAsync(e.notificationId);
  }

  return e;
});

const schedulingOptions = {
  time: new Date()
};

export const addNotification = async (date, title, text) => {
  let idNotify = null;
  const { status } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
  if (status !== 'granted') {
    await Permissions.askAsync(Permissions.NOTIFICATIONS);
  }

  localNotification.title = title;
  localNotification.body = text;
  schedulingOptions.time = date.getTime();

  if (date > Date.now()) {
    idNotify = await Notifications.scheduleLocalNotificationAsync(localNotification, schedulingOptions);
  }

  return idNotify;
};

export const calculateNotifications = dateBird => {
  const events = calculateEvents(dateBird);
  const notifications = [];
  const gg = _.orderBy(events, 'start_event', 'desc');

  let tmpStart = new Date();

  _.forEach(gg, event => {
    const se = new Date(event.start_event);
    if (tmpStart.getTime() !== se.getTime()) {
      tmpStart = se;
      _.forEach(dataVacination, elem => {
        addNotification(new Date(event.start_event), event.name, elem.body);
        notifications.push({
          name: elem.body,
          type: 'notify',
          type_event: 14,
          start_event: moment(event.start_event)
            .subtract(elem.day, 'days')
            .toDate()
        });
      });
    }
  });

  return notifications;
};

const formatShowDate = (date, pattern = 'DD-MM-YYYY') => {
  let result = '';

  if (moment(new Date(date)).isValid()) {
    result = moment(new Date(date)).format(pattern);
  }

  return result;
};

const formatDateForMongo = (date, pattern) => moment(date, pattern).toDate();

export const bitrhDateDiff = (birthDate, currentDate) => {
  if (moment(new Date(birthDate)).isValid) {
    const curent = moment(new Date(currentDate));

    return moment(curent).diff(new Date(birthDate), 'days');
  }

  return null;
};

const addNotificationDate = (date, diffDate, is24H = false) => {
  const dd = moment(new Date(date));
  const nd = moment();
  let delta = dd.diff(nd, 'days') - diffDate;

  if (is24H) {
    delta = dd.diff(nd, 'days') - 1;
    return moment(nd.add(delta, 'd').add(24, 'h'));
  }
  return moment(nd.add(delta, 'd'));
};

const addYears = year => moment().subtract(year, 'months');

const getWeightXY = (age = 0) => {
  const objNorm = { normW: 0, normH: 0, downW: 0, downH: 0 };

  tableTitle.forEach((val, i) => {
    if (/(года?|лет|мес)/gi.test(val)) {
      const tmpAge = val.substring(0, val.indexOf(' '));

      if (age === tmpAge) {
        const tmpStrH = rowData[2][i];
        const tmpStrW = rowData[0][i];

        objNorm.normH = tmpStrH.substring(tmpStrH.indexOf('-') + 1);
        objNorm.normW = tmpStrW.substring(tmpStrW.indexOf('-') + 1);

        objNorm.downH = tmpStrH.substring(0, tmpStrH.indexOf('-'));
        objNorm.downW = tmpStrW.substring(0, tmpStrW.indexOf('-'));
      }
    }
  });

  return objNorm;
};

const calculateEvents = dateBird => {
  const result = [];
  const tmpDateBird = new Date(dateBird);
  // Гепатит B
  result.push({
    name: 'Гепатит B',
    type: 'hpt_1',
    start_event: moment(tmpDateBird)
      .add(24, 'hours')
      .utc()
      .toDate()
  });
  result.push({
    name: 'Гепатит B',
    type: 'hpt_2',
    start_event: moment(tmpDateBird)
      .add(3, 'months')
      .utc()
      .toDate()
  });
  result.push({
    name: 'Гепатит B',
    type: 'hpt_3',
    start_event: moment(tmpDateBird)
      .add(6, 'months')
      .utc()
      .toDate()
  });

  // Туберкулез
  result.push({
    name: 'Туберкулез',
    type: 'tbr_1',
    start_event: moment(tmpDateBird)
      .add(5, 'days')
      .utc()
      .toDate()
  });

  // Дифтерия, коклюш, столбняк
  result.push({
    name: 'Дифтерия, коклюш, столбняк',
    type: 'dks_1',
    start_event: moment(tmpDateBird)
      .add(3, 'months')
      .utc()
      .toDate()
  });
  result.push({
    name: 'Дифтерия, коклюш, столбняк',
    type: 'dks_2',
    start_event: moment(tmpDateBird)
      .add(4, 'months')
      .utc()
      .toDate()
  });
  result.push({
    name: 'Дифтерия, коклюш, столбняк',
    type: 'dks_3',
    start_event: moment(tmpDateBird)
      .add(6, 'months')
      .utc()
      .toDate()
  });
  result.push({
    name: 'Дифтерия, коклюш, столбняк',
    type: 'dks_4',
    start_event: moment(tmpDateBird)
      .add(20, 'months')
      .utc()
      .toDate()
  });
  result.push({
    name: 'Дифтерия, коклюш, столбняк',
    type: 'dks_5',
    start_event: moment(tmpDateBird)
      .add(7, 'years')
      .utc()
      .toDate()
  });
  result.push({
    name: 'Дифтерия, коклюш, столбняк',
    type: 'dks_6',
    start_event: moment(tmpDateBird)
      .add(14, 'years')
      .utc()
      .toDate()
  });
  result.push({
    name: 'Дифтерия, коклюш, столбняк',
    type: 'dks_7',
    start_event: moment(tmpDateBird)
      .add(18, 'years')
      .utc()
      .toDate()
  });

  // Полиомиелит
  result.push({
    name: 'Полиомиелит',
    type: 'pol_1',
    start_event: moment(tmpDateBird)
      .add(3, 'months')
      .utc()
      .toDate()
  });
  result.push({
    name: 'Полиомиелит',
    type: 'pol_2',
    start_event: moment(tmpDateBird)
      .add(4, 'months')
      .utc()
      .toDate()
  });
  result.push({
    name: 'Полиомиелит',
    type: 'pol_3',
    start_event: moment(tmpDateBird)
      .add(6, 'months')
      .utc()
      .toDate()
  });
  result.push({
    name: 'Полиомиелит',
    type: 'pol_4',
    start_event: moment(tmpDateBird)
      .add(6, 'years')
      .utc()
      .toDate()
  });
  result.push({
    name: 'Полиомиелит',
    type: 'pol_5',
    start_event: moment(tmpDateBird)
      .add(14, 'years')
      .utc()
      .toDate()
  });

  // Корь, краснуха, паротит
  result.push({
    name: 'Корь, краснуха, паротит',
    type: 'kkp_1',
    start_event: moment(tmpDateBird)
      .add(12, 'months')
      .utc()
      .toDate()
  });
  result.push({
    name: 'Корь, краснуха, паротит',
    type: 'kkp_2',
    start_event: moment(tmpDateBird)
      .add(6, 'years')
      .utc()
      .toDate()
  });

  // Грипп
  result.push({
    name: 'Грипп',
    type: 'grp_1',
    start_event: moment(tmpDateBird)
      .add(18, 'months')
      .utc()
      .toDate()
  });
  result.push({
    name: 'Грипп',
    type: 'grp_2',
    start_event: moment(tmpDateBird)
      .add(20, 'months')
      .utc()
      .toDate()
  });
  result.push({
    name: 'Грипп',
    type: 'grp_3',
    start_event: moment(tmpDateBird)
      .add(6, 'years')
      .utc()
      .toDate()
  });
  return result;
};

const checkFromNow = first => {
  const fDate = moment(new Date(first));

  return fDate < moment();
};

const validateEmail = email => {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
};

export {
  formatShowDate,
  formatDateForMongo,
  addNotificationDate,
  addYears,
  getWeightXY,
  calculateEvents,
  checkFromNow,
  validateEmail
};
