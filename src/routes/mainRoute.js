import React, { Component } from 'react';
import { ScreenOrientation } from 'expo';
import { Image } from 'react-native';
import {
  createStackNavigator,
  createBottomTabNavigator,
  createSwitchNavigator,
  createAppContainer
} from 'react-navigation';

import WelcomeScreen from '../screens/WelcomeScreen';
import MainScreen from '../screens/MainScreen';
import GraphScreen from '../screens/GraphScreen';
import NormalyzScreen from '../screens/NormalyzScreen';
import MedicalCardScreen from '../screens/MedicalCardScreen';
import SettingsScreen from '../screens/SettingsScreen';
import AddChildScreen from '../screens/AddChildScreen';
import WriteDevelopScreen from '../screens/WriteDevelopScreen';
import ChangeProfileScreen from '../screens/ChangeProfileScreen';
import NeedToBeScreen from '../screens/NeedToBeScreen';
import InternalScreen from '../screens/InternalScreen';
import AddEventScreen from '../screens/AddEventScreen';
import ChartScreen from '../screens/ChartScreen';
import PhotoScreen from '../screens/PhotoScreen';
import TableScreen from '../screens/TableScreen';
import AuthScreen from '../screens/AuthScreen';
import LoginScreen from '../screens/LoginScreen';
import EditEventScreen from '../screens/EditEventScreen';
import PhotoGalleryScreen from '../screens/PhotoGalleryScreen';
import UpdateEventScreen from '../screens/UpdateEventScreen';
import Term from '../screens/Term';

import { CalendarTabCfg, MainTabCfg } from '../config/tabconfig';

class Route extends Component {
  portrait = () => {
    ScreenOrientation.allow(ScreenOrientation.Orientation.PORTRAIT);
  };

  landscape = () => {
    ScreenOrientation.allow(ScreenOrientation.Orientation.LANDSCAPE_LEFT);
  };

  render() {
    const SettingsNav = createStackNavigator(
      {
        settings: { screen: SettingsScreen, path: 'settings' },
        addchild: { screen: AddChildScreen, path: 'addchild' },
        chageprofile: { screen: ChangeProfileScreen, path: 'chageprofile' },
        writedevelop: { screen: WriteDevelopScreen, path: 'writedevelop' },
        needtobe: { screen: NeedToBeScreen, path: 'needtobe' },
        internal: { screen: InternalScreen, path: 'internal' },
        photo: { screen: PhotoScreen, path: 'photo' },
        chart: { screen: ChartScreen, path: 'chart' },
        table: { screen: TableScreen, path: 'table' }
      },
      {
        initialRouteName: 'settings',
        headerTitleStyle: { fontFamily: 'qunelas-semi-bold' }
      }
    );

    const CalendarStack = createStackNavigator(
      {
        calendar: {
          screen: MainScreen,
          path: 'calendar'
        },
        addevent: { screen: AddEventScreen, path: 'addevent' },
        editevent: { screen: EditEventScreen, path: 'editevent' },
        updateevent: { screen: UpdateEventScreen, path: 'updateevent' }
      },
      {
        initialRouteName: 'calendar',
        headerTitleStyle: { fontFamily: 'qunelas-semi-bold' }
      }
    );

    const MedicalStack = createStackNavigator(
      {
        medical: { screen: MedicalCardScreen },
        photogallery: { screen: PhotoGalleryScreen, path: 'photogallery' }
      },
      {
        initialRouteName: 'medical',
        headerTitleStyle: { fontFamily: 'qunelas-semi-bold' }
      }
    );

    const MainNav = createBottomTabNavigator(
      {
        calendar: {
          screen: CalendarStack,
          navigationOptions: {
            title: 'Календарь',
            tabBarIcon: ({ tintColor }) => (
              <Image
                style={[
                  {
                    tintColor,
                    height: 23.5,
                    width: 21.5
                  }
                ]}
                source={require('../../assets/images/school-calendar.png')}
              />
            )
          }
        },
        graph: { screen: GraphScreen },
        normal: { screen: NormalyzScreen },
        medical: {
          screen: MedicalStack,
          navigationOptions: {
            title: 'Мед. карта',

            tabBarIcon: ({ tintColor }) => (
              <Image
                source={require('../../assets/images/photo-camera.png')}
                style={[
                  {
                    tintColor,
                    height: 19,
                    width: 25
                  }
                ]}
              />
            )
          }
        }
      },

      CalendarTabCfg
    );

    const AuthStack = createStackNavigator(
      {
        auth: { screen: AuthScreen },
        login: { screen: LoginScreen },
        term: { screen: Term }
      },
      { initialRouteName: 'auth', headerMode: 'none' }
    );

    const MainNavigation = createSwitchNavigator(
      {
        welcome: { screen: WelcomeScreen },
        auth: { screen: AuthStack },
        main: { screen: MainNav },
        calendasettings: { screen: SettingsNav }
      },
      {
        initialRouteName: 'welcome',
        headerMode: 'none'
      }
    );

    const defaultGetStateForAction = MainNav.router.getStateForAction;

    MainNav.router.getStateForAction = (action, state) => {
      if (action.routeName || action.type === 'Navigation/BACK') {
        if (action.routeName === 'graph' || action.routeName === 'table') {
          this.landscape();
          return defaultGetStateForAction(action, state);
        }
        this.portrait();
        return defaultGetStateForAction(action, state);
      }

      return defaultGetStateForAction(action, state);
    };

    const AppContainer = createAppContainer(MainNavigation);

    return <AppContainer />;
  }
}

export default Route;
