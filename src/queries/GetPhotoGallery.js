import gql from 'graphql-tag';

export default gql`
  query GetFiles($folder_id: ID) {
    user {
      id
      email
      getFiles(folder_id: $folder_id) {
        file_id
        folder_id
        id
        name
      }
    }
  }
`;
