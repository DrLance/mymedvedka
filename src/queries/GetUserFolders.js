import gql from 'graphql-tag';

export default gql`
  {
    user {
      id
      email
      folders {
        id
        user_id
        name
        description
        count
      }
    }
  }
`;
