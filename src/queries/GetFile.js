import gql from 'graphql-tag';

export default gql`
  query GetFile($file_id: ID) {
    user {
      getFile(file_id: $file_id) {
        id
        file
        name
      }
    }
  }
`;
