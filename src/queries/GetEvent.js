import gql from 'graphql-tag';

export default gql`
  query GetEvent($id: ID) {
    user {
      id
      email
      getEvent(id: $id) {
        id
        user_id
        kid_id
        name
        start_date
        end_date
        description
        type_event
      }
    }
  }
`;
