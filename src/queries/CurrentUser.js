import gql from 'graphql-tag';

export default gql`
  {
    user {
      id
      email
      kids {
        id
        name
        isActive
        date_birth
      }
      activeKid {
        id
        date_birth
      }
      events {
        id
        user_id
        kid_id
        name
        start_date
        end_date
        description
        type_event
      }
      tips {
        id
        day
        description
      }
    }
  }
`;
