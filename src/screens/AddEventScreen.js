/* eslint-disable global-require */
import React, { Component } from 'react';
import { View, Switch, Text, TextInput, DatePickerAndroid, DatePickerIOS, Platform, Modal } from 'react-native';
import { Icon, H2, Button, Content } from 'native-base';
import { graphql } from 'react-apollo';

import SwitchCustom from '../components/Switch';
import Tip from '../components/Tip';
import mainStyles from '../config/styles';
import { formatShowDate, formatDateForMongo, addNotification, addNotificationDate } from '../utils/util';
import { addEvent } from '../mutations/Event';
import query from '../queries/CurrentUser';

type Props = {
  navigation: Function,
  mutate: Function,
  data: Object
};
class AddEventScreen extends Component<Props> {
  static navigationOptions = ({ navigation }) => ({
    headerTintColor: '#525252',
    headerStyle: { backgroundColor: '#fff' },
    title: 'Вернуться',
    headerLeft: <Icon name="arrow-back" style={mainStyles.headerBack} onPress={() => navigation.goBack(null)} />,
    tabBarVisible: false
  });

  constructor(props) {
    super(props);
    if (this.props.navigation.state.params) {
      const { selectedDay } = this.props.navigation.state.params;
      this.state = {
        selectedDay,
        nameVaccination: '',
        renderIosPicker: false,
        datePicker: new Date(),
        notifyDate: null,
        isModal: false,
        remindText: '',
        isRemind: false,
        remind24: {
          name: 'Напомнить за 24 часа',
          age: '',
          ageMod: '',
          isEnable: false
        },
        remind3: {
          name: 'Напомнить за 3 дня',
          age: '',
          ageMod: '',
          isEnable: false
        }
      };
    }
  }

  onChangeHandler = type => {
    const { remind24, remind3 } = this.state;

    const nowDate = new Date(this.state.selectedDay.dateString);
    const notifyDate = addNotificationDate(nowDate, type, type === 24);

    if (nowDate && notifyDate < new Date()) {
      return this.checkDataPicker();
    }

    if (type === 24) {
      remind24.isEnable = !this.state.remind24.isEnable;
      this.setState({ remind24, remindText: notifyDate.format('DD-MM-YYYY hh:mm'), notifyDate });
    } else if (type === 3) {
      remind3.isEnable = !this.state.remind3.isEnable;
      this.setState({ remind3, remindText: notifyDate.format('DD-MM-YYYY hh:mm'), notifyDate });
    } else if (type === 'remind') {
      this.setState({ isRemind: !this.state.isRemind, remindText: notifyDate.format('DD-MM-YYYY hh:mm'), notifyDate });
    }
  };

  onPressAddEvent() {
    if (this.props.data.user) {
      const { id, activeKid } = this.props.data.user;
      const nowDate = `${
        this.state.selectedDay.dateString
      } ${new Date().getHours()}:${new Date().getMinutes()}:${new Date().getSeconds()}`;

      const date = formatDateForMongo(nowDate, 'YYYY-MM-DD hh:mm:ss');

      this.props
        .mutate({
          variables: {
            user_id: id,
            kid_id: activeKid.id,
            start_date: date,
            end_date: date,
            name: this.state.nameVaccination,
            description: '',
            type_event: this.props.navigation.state.params.type,
            isActive: false
          },
          refetchQueries: [{ query }]
        })
        .then(({ data }) => {
          if (data) {
            if (this.state.notifyDate) {
              const typeV = this.props.navigation.getParam('type');
              const txtTitle = typeV === 1 ? 'Назначена прививка:' : 'Назначен прием врача:';
              const nDate = new Date(this.state.notifyDate.add(2, 'minutes'));

              addNotification(
                nDate,
                `${txtTitle} ${this.state.nameVaccination}`,
                this.state.notifyDate.format('DD-MM-YYYY')
              );
            }

            this.props.navigation.goBack(null);
          }
        });
    }
  }

  onChangeText(text) {
    this.setState({ nameVaccination: text });
  }

  onChangeDateIOS = date => {
    this.setState({ datePicker: date, remindText: formatShowDate(date, 'DD-MM-YYYY HH:MM'), notifyDate: date });
  };

  checkDataPicker = () => {
    if (Platform.OS !== 'ios') {
      return this.renderDataPicker();
    }

    return this.setState({ renderIosPicker: true, isModal: true });
  };

  closeModal = async () => {
    this.setState({ isModal: false });
  };

  renderDataPicker = async name => {
    const { action, year, month, day } = await DatePickerAndroid.open({ date: new Date(), mode: 'spinner' });
    const current = new Date();
    const date = new Date(year, month, day, current.getHours(), current.getMinutes() + 2);

    if (action !== DatePickerAndroid.dismissedAction) {
      this.setState({
        notifyDate: new Date(year, month, day),
        remindText: formatShowDate(date, 'DD-MM-YYYY HH:MM')
      });
    }
  };

  render() {
    return (
      <Content style={{ flex: 1 }} padder contentContainerStyle={styles.container}>
        <H2 style={{ color: '#4b4b4b', fontFamily: 'qunelas-semi-bold' }}>
          {this.props.navigation.state.params.type === 1 ? 'Добавить прививку' : 'Добавить прием врача'}
        </H2>
        <View style={[styles.tableContainer, { height: 60, backgroundColor: '#e7f1ff', marginTop: 5 }]}>
          <Text style={mainStyles.txtAddChild}>Название</Text>
          <TextInput
            onChangeText={text => this.onChangeText(text)}
            underlineColorAndroid="transparent"
            style={[
              mainStyles.txtAddEventName,
              { width: '25%', textDecorationLine: 'underline', borderBottomWidth: 0.5 }
            ]}
          />
        </View>
        <View style={[styles.tableContainer, { height: 120, backgroundColor: '#fbfbfb' }]}>
          <View>
            <Text style={mainStyles.txtAddChild}>Дата</Text>
          </View>
          <View style={{ flexDirection: 'column', width: '30%', alignItems: 'flex-end', justifyContent: 'center' }}>
            <Text style={mainStyles.txtAddEventName}>{formatShowDate(this.state.selectedDay.dateString)}</Text>
          </View>
        </View>
        <SwitchCustom item={this.state.remind24} onValueChange={item => this.onChangeHandler(24, item)} />
        <SwitchCustom item={this.state.remind3} onValueChange={item => this.onChangeHandler(3, item)} />
        <View style={styles.containerSwitch}>
          <Text style={mainStyles.txtAddEventName}>Напомнить о сдаче анализов</Text>
          <Switch
            onTintColor="#49d060"
            thumbTintColor="#fefefe"
            tintColor="#e7e7e7"
            value={this.state.isRemind}
            onValueChange={() => this.onChangeHandler('remind')}
          />
        </View>
        <Text style={[mainStyles.txtAddEventSmall, { width: '80%' }]}>
          Напомнить о необходимости сдать анализы и записаться на прием педиатра
        </Text>
        <Text style={mainStyles.txtAddEventSmallUnder}>{`Дата напоминания: ${this.state.remindText}`}</Text>

        <Button
          block
          transparent
          large
          style={{ backgroundColor: '#e7f1ff', marginTop: 10 }}
          onPress={() => this.onPressAddEvent()}
        >
          <Text style={mainStyles.txtAddChildBtn}>
            {this.props.navigation.state.params.type === 1 ? 'Добавить прививку' : 'Добавить прием врача'}
          </Text>
        </Button>

        <Tip style={{ paddingTop: 10 }} textTip="Напоминания можно установить только после добавления прививки." />
        <Modal onRequestClose={this.closeModal} visible={this.state.isModal}>
          <View style={{ justifyContent: 'flex-end' }}>
            {this.state.renderIosPicker ? (
              <DatePickerIOS
                timeZoneOffsetInMinutes={180}
                mode="datetime"
                date={this.state.datePicker}
                onDateChange={this.onChangeDateIOS}
              />
            ) : null}
            <Button full style={{ backgroundColor: '#e3f6ef' }} onPress={this.closeModal}>
              <Text style={mainStyles.txtAddChildBtn}>Выбрать и закрыть</Text>
            </Button>
          </View>
        </Modal>
      </Content>
    );
  }
}

const styles = {
  container: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
    backgroundColor: '#fff'
  },
  tableContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  containerSwitch: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingBottom: 10,
    paddingTop: 10
  }
};

export default graphql(query)(graphql(addEvent)(AddEventScreen));
