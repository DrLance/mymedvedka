import React, { Component } from 'react';
import { Text } from 'react-native';
import { Icon, Container, Content, H2, Form, Item, Input, Label, Button } from 'native-base';

import mainStyles from '../config/styles';
import { validateEmail } from '../utils/util';

class WriteDevelopScreen extends Component {
  static navigationOptions = ({ navigation, screenProps }) => ({
    headerTintColor: '#525252',
    headerStyle: {
      backgroundColor: '#fff'
    },
    title: 'Вернуться',
    headerLeft: <Icon name="arrow-back" style={mainStyles.headerBack} onPress={() => navigation.goBack()} />
  });

  state = {
    name: '',
    email: '',
    message: '',
    isNameEmpty: false,
    isMessageEmpty: false,
    isValidEmail: true
  };

  onPressSend = () => {
    if (!this.state.name) {
      return this.setState({ isNameEmpty: true });
    }

    if (!validateEmail(this.state.email)) {
      return this.setState({ isValidEmail: false });
    }

    if (!this.state.message) {
      return this.setState({ isMessageEmpty: true });
    }

    if (!this.state.isMessageEmpty && !this.state.isNameEmpty && this.state.isValidEmail) {
      const { name, email, message } = this.state;
      fetch('http://192.168.20.84:4000/send_email', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          name,
          email,
          message
        })
      })
        .then(response => response.json())
        .then(({ error }) => {
          if (!error) {
            this.setState({ name: '', email: '', message: '' });
          }
        })
        .catch(err => console.log(err));
    }

    return null;
  };

  render() {
    return (
      <Container>
        <Content padder style={{ backgroundColor: '#fff' }}>
          <H2 style={styles.h2}>Написать разработчикам</H2>
          <Form>
            <Label style={[mainStyles.txtAddChild, { marginVertical: 10 }]}>Ваше Имя</Label>
            <Item error last style={styles.input}>
              <Input
                placeholderTextColor={this.state.isNameEmpty ? 'red' : '#bfbfbf'}
                value={this.state.name}
                placeholder="Иван"
                style={{ fontFamily: 'qunelas-semi-bold' }}
                onChangeText={name => this.setState({ name })}
              />
            </Item>
            <Label style={[mainStyles.txtAddChild, { marginVertical: 10 }]}>Ваше E-mail</Label>
            <Item last style={[styles.input, { backgroundColor: this.state.isValidEmail ? '#e7f1ff' : 'red' }]}>
              <Input
                placeholderTextColor="#bfbfbf"
                value={this.state.email}
                placeholder="info@info.ru"
                style={{ fontFamily: 'qunelas-semi-bold' }}
                onChangeText={email => this.setState({ email, isValidEmail: true })}
              />
            </Item>
            <Label style={[mainStyles.txtAddChild, { marginVertical: 10 }]}>Ваше сообщение</Label>
            <Item last style={styles.input}>
              <Input
                placeholderTextColor={this.state.isMessageEmpty ? 'red' : '#bfbfbf'}
                value={this.state.message}
                placeholder="Комментарий"
                style={styles.inputComment}
                multiline
                autoGrow
                numberOfLines={4}
                onChangeText={message => this.setState({ message, isMessageEmpty: false })}
              />
            </Item>
          </Form>
          <Button large block style={styles.btn} onPress={this.onPressSend}>
            <Text style={mainStyles.txtAddChildBtn}>Отправить</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}
const styles = {
  h2: {
    color: '#4b4b4b',
    marginBottom: 20,
    fontFamily: 'qunelas-semi-bold'
  },
  input: {
    backgroundColor: '#e7f1ff',
    borderBottomWidth: 0
  },
  inputComment: {
    fontFamily: 'qunelas-semi-bold',
    textAlignVertical: 'auto'
    //height: 200
  },
  btn: { backgroundColor: '#e7ffee', marginTop: 15 }
};

export default WriteDevelopScreen;
