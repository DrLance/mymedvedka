/* eslint-disable global-require */
import React, { Component } from 'react';
import { View, Text, Image, ImageBackground, PanResponder, Animated, Platform, Modal, TextInput } from 'react-native';
import { Camera, Permissions } from 'expo';
import { Container, Content, Button } from 'native-base';
import { graphql, compose, Query, Mutation } from 'react-apollo';
import { ReactNativeFile } from 'apollo-upload-client';
import _ from 'lodash';
import HeaderButton from '../components/HeaderButton';
import MedicalNotify from '../components/MedicalNotify';
import MedicalPhoto from '../components/MedicalPhoto';
import Tip from '../components/Tip';
import mainStyles from '../config/styles';
import { addFolder, uploadFile, renameFolder } from '../mutations/Folder';
import query from '../queries/GetUserFolders';
import FolderImage from '../components/FolderImage';
import FolderImageAdd from '../components/FolderImageAdd';
import { formatShowDate } from '../utils/util';

const defaultImg = require('../../assets/images/photo-camera-list.png');

let PHOTO_BANK = [];

const arrFoldersLayout = [];

let parentLayout = null;

type Props = {
  navigation: any,
  addFolder: Function,
  data: Object,
  uploadFile: Function
};

class MedicalCardScreen extends Component<Props> {
  static navigationOptions = () => ({ header: null });

  state = {
    errors: 'Можете добавить папки (не более 2х) и поместить туда изображения.',
    hasCameraPermission: null,
    type: Camera.Constants.Type.back,
    isRenderCamera: false,
    isScroll: false,
    isRenameModal: false,
    renameFolderID: 0,
    newFolderName: ''
  };

  onPressCamera = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);

    this.setState({ hasCameraPermission: status === 'granted', isRenderCamera: true });
  };

  onPressSettings = () => {
    this.props.navigation.navigate('settings');
  };

  onPressPhoto = () => {
    this.props.navigation.navigate('photo');
  };

  onPressAddFolder = () => {
    if (this.props.data && this.props.data.user && this.props.data.user.folders) {
      const { folders, id } = this.props.data.user;

      if (folders.length < 2) {
        this.props
          .addFolder({
            variables: {
              user_id: id,
              name: folders.length === 0 ? 'Анализов' : 'Выписки'
            },
            refetchQueries: [{ query }]
          })
          .catch(res => {
            const errors = res.graphQLErrors.map(error => error.message);
            this.setState({ errors });
          });
      }
    }
  };

  onRequestClose() {
    this.setState({ isRenameModal: false });
  }

  onPressNav = folder_id => {
    this.props.navigation.navigate('photogallery', { folder_id });
  };

  onPressTake = async () => {
    if (this.camera) {
      const photo = await this.camera.takePictureAsync({ quality: 0.8, exif: true });
      if (PHOTO_BANK.length < 2) {
        PHOTO_BANK.push(photo);
      }
      this.setState({ isRenderCamera: false });
    }
  };

  onLongPressFolder = id => {
    this.setState({ isRenameModal: true, renameFolderID: id });
  };

  getPanResponder(index) {
    return PanResponder.create({
      onMoveShouldSetResponderCapture: () => true,
      onMoveShouldSetPanResponderCapture: () => true,
      onPanResponderMove: Animated.event([
        null,
        {
          dx: this.pan[index].x,
          dy: this.pan[index].y
        }
      ]),
      onPanResponderRelease: (e, gesture) => {
        const { find, folderId } = this.isDropZone(gesture);
        if (find) {
          const { uri } = PHOTO_BANK[index];
          const uploadUri = Platform.OS === 'ios' ? uri.replace('file://', '') : uri;

          const fileRn = new ReactNativeFile({
            uri: uploadUri,
            type: 'image/jpeg',
            name: formatShowDate(new Date(), 'DD_MM_YYYY_hh_mm_ss')
          });

          this.props
            .uploadFile({
              variables: {
                folder_id: folderId,
                file: fileRn
              },
              options: { refetchQueries: [{ query }] }
            })
            .then(res => {
              if (res) {
                PHOTO_BANK = _.filter(PHOTO_BANK, elem => elem.uri !== uri);
                this.setState({ isScroll: false });
              }
            });
        } else {
          Animated.spring(this.pan[index], { toValue: { x: 0, y: 0 } }).start();
        }
      }
    });
  }

  isDropZone = gesture => {
    const padder = 30;
    const result = { find: false, folderId: null };
    arrFoldersLayout.forEach(value => {
      const coordX = parentLayout.x + value.layout.x + value.layout.width / 2;
      const coordY = parentLayout.y + value.layout.height;
      if (gesture.moveY >= coordY && gesture.moveX >= coordX && gesture.moveX <= coordX + padder) {
        result.find = true;
        result.folderId = value.id;
      }
    });
    return result;
  };

  saveNewFolderName = () => {
    this.props
      .renameFolder({
        variables: {
          folder_id: this.state.renameFolderID,
          name: this.state.newFolderName
        },
        options: { refetchQueries: [{ query }] }
      })
      .then(res => {
        if (res) {
          this.setState({ isRenameModal: false });
        }
      });
  };

  renderPhotoBank() {
    this.pan = PHOTO_BANK.map(() => new Animated.ValueXY());
    return PHOTO_BANK.map((v, i) => (
      <Animated.View key={i} style={this.pan[i].getLayout()} {...this.getPanResponder(i).panHandlers}>
        <MedicalPhoto image={v.uri ? v : defaultImg} style={{ marginRight: 10, marginTop: 5 }} />
      </Animated.View>
    ));
  }

  renderCamera() {
    const { hasCameraPermission } = this.state;
    if (hasCameraPermission === null) {
      return <View />;
    } if (hasCameraPermission === false) {
      return <Text style={styles.txtBtn}>Нет доступа к камере</Text>;
    }
    return (
      <Camera
        ref={ref => {
          this.camera = ref;
        }}
        style={styles.containerPhoto}
        type={this.state.type}
      >
        <Button block style={{ width: '80%', marginBottom: 20, alignSelf: 'center' }} onPress={this.onPressTake}>
          <Text style={[styles.txtItem, { textAlign: 'center', color: '#fff' }]}>Сделать фото</Text>
        </Button>
      </Camera>
    );
  }

  renderFolder = () => (
    <Query query={query}>
      {({ loading, error, data }) => {
        if (loading) return null;
        if (error) return `Error!: ${error}`;

        if (!data.user) return null;

        return data.user.folders.map(v => (
          <FolderImage
            onPress={() => this.onPressNav(v.id)}
            onLongPress={() => this.onLongPressFolder(v.id)}
            key={v.id}
            data={v}
            onLayout={event => {
              arrFoldersLayout.push({ id: v.id, layout: event.nativeEvent.layout });
            }}
          />
        ));
      }}
    </Query>
  );

  render() {
    return this.state.isRenderCamera ? (
      this.renderCamera()
    ) : (
      <Container backgroundColor="#fff">
        <ImageBackground style={mainStyles.imageHeaderMedical} source={require('../../assets/images/back_normal.png')}>
          <View style={styles.headerContainer}>
            <HeaderButton image={require('../../assets/images/settings.png')} onPress={this.onPressSettings} />
            <Text style={mainStyles.txtHeader}>Ваша медицинская карта</Text>
          </View>
        </ImageBackground>
        <Content padder showsVerticalScrollIndicator={false} scrollEnabled={this.state.isScroll}>
          <MedicalNotify onPress={this.onPressPhoto} />
          <Text style={mainStyles.txtMedicalMain}>Фотогалерея для сохранения истории</Text>
          <View style={styles.photoContainer}>
            <MedicalPhoto image={defaultImg} onPress={this.onPressCamera} style={{ marginRight: 10, marginTop: 5 }} />
            {this.renderPhotoBank()}
          </View>
          <View
            style={{ flexDirection: 'row', marginVertical: 20, justifyContent: 'space-around' }}
            onLayout={event => {
              parentLayout = event.nativeEvent.layout;
            }}
          >
            {this.renderFolder()}
            <FolderImageAdd onPress={this.onPressAddFolder} />
          </View>
          <Tip textTip={this.state.errors} />
          <Modal
            transparent
            onRequestClose={this.onRequestClose}
            animationType="slide"
            visible={this.state.isRenameModal}
          >
            <View
              style={{
                flex: 1,
                justifyContent: 'flex-end',
                padding: 8,
                marginBottom: 60,
                alignItems: 'center'
              }}
            >
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderRadius: 5,
                  backgroundColor: 'rgba(1,1,1,0.2)',
                  width: '100%'
                }}
              >
                <Text style={[mainStyles.txtFolderHeader, { fontSize: 17 }]}>Название:</Text>
                <TextInput
                  style={[mainStyles.txtFolderHeader, { width: 80, marginLeft: 5 }]}
                  value={this.state.newFolderName}
                  onChangeText={text => this.setState({ newFolderName: text })}
                />
              </View>

              <View style={{ flexDirection: 'row', marginTop: 5 }}>
                <Button
                  style={{
                    marginHorizontal: 20,
                    backgroundColor: '#7bbf76',
                    height: 30,
                    width: 100,
                    alignItems: 'center',
                    justifyContent: 'center'
                  }}
                  transparent
                  onPress={this.saveNewFolderName}
                >
                  <Text style={[mainStyles.txtFolderHeader, { color: '#fff' }]}>Сохранить</Text>
                </Button>
                <Button
                  style={{
                    marginHorizontal: 20,
                    backgroundColor: '#5c9fbc',
                    height: 30,
                    width: 100,
                    alignItems: 'center',
                    justifyContent: 'center'
                  }}
                  transparent
                  onPress={() => this.setState({ isRenameModal: false })}
                >
                  <Text style={[mainStyles.txtFolderHeader, { color: '#fff' }]}>Закрыть</Text>
                </Button>
              </View>
            </View>
          </Modal>
        </Content>
      </Container>
    );
  }
}

const styles = {
  containerPhoto: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: 10,
    marginTop: 20,
    marginBottom: '10%'
  },
  txtMain: {
    color: '#4b4b4b',
    fontSize: 32,
    textAlign: 'center',
    marginTop: 10
  },
  photoContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'flex-start',
    marginTop: 10
  },
  txtItem: {
    color: '#4c5864',
    fontSize: 16
  },
  txtItemAdd: {
    color: '#4c5864',
    fontSize: 16
  },
  txtSubItem: {
    color: '#878787',
    fontSize: 14
  }
};

export default compose(
  graphql(query),
  graphql(addFolder, { name: 'addFolder' }),
  graphql(uploadFile, { name: 'uploadFile' }),
  graphql(renameFolder, { name: 'renameFolder' })
)(MedicalCardScreen);
