/* eslint-disable global-require */
import React, { Component } from 'react';
import { View, Text, Image, ImageBackground, Modal, TouchableOpacity, SafeAreaView } from 'react-native';
import _ from 'lodash';
import { Calendar } from 'react-native-calendars';
import { ActionSheet, Root, Content, Container } from 'native-base';
import { graphql } from 'react-apollo';
import EventNotify from '../components/EventNotify';
import EventList from '../components/EventList';
import HeaderButton from '../components/HeaderButton';
import CalendarTodayTip from '../components/CalendarTodayTip';
import { calendarTheme } from '../config/calendar';
import mainStyles from '../config/styles';
import query from '../queries/CurrentUser';
import { formatShowDate, checkFromNow, bitrhDateDiff } from '../utils/util';

const BUTTONS = ['Добавить прививку', 'Добавить прием врача', 'Закрыть'];
const DESTRUCTIVE_INDEX = 2;
const CANCEL_INDEX = 2;
const selectedDay = { dateString: formatShowDate(new Date(), 'YYYY-MM-DD') };
const objToday = { startingDay: true, color: 'white', endingDay: true, textColor: '#648cdf' };
const objVaccine = { startingDay: true, color: '#fc6666', endingDay: true };
const objDoctor = { startingDay: true, color: '#66fcb7', endingDay: true };
let event = { [selectedDay.dateString]: objToday };

type Props = {
  data: Object,
  navigation: any
};

class MainScreen extends Component<Props> {
  static navigationOptions = () => ({
    header: null
  });

  static getDerivedStateFromProps(nextProps) {
    event = {};
    event[selectedDay.dateString] = objToday;
    if (nextProps.data && nextProps.data.user) {
      const { events } = nextProps.data.user;
      events.forEach(element => {
        if (element.description !== 'notify') {
          event[formatShowDate(element.start_date, 'YYYY-MM-DD')] = checkFromNow(element.start_date)
            ? objDoctor
            : objVaccine;
        }
      });
    }
    return { events_calendar: event };
  }

  state = {
    selected: selectedDay.dateString,
    selectedDate: selectedDay,
    events_calendar: null,
    currentMonth: new Date(),
    isModal: false,
    tipText: ''
  };

  onPressSettings = () => {
    this.props.navigation.navigate('settings');
  };

  onPressAddEvent = () => {
    ActionSheet.show(
      {
        options: BUTTONS,
        cancelButtonIndex: CANCEL_INDEX,
        destructiveButtonIndex: DESTRUCTIVE_INDEX,
        title: 'Выберите действие'
      },
      buttonIndex => {
        if (buttonIndex == '0') {
          this.props.navigation.navigate('addevent', { selectedDay: this.state.selectedDate, type: 1 });
        } else if (buttonIndex == '1') {
          this.props.navigation.navigate('addevent', { selectedDay: this.state.selectedDate, type: 2 });
        }
      }
    );
  };

  onPressEvent = item => {
    if (item.type_event != 14) {
      this.props.navigation.navigate('editevent', { eventId: item.id });
    } else {
      this.setState({ isModal: true, tipText: item.name });
    }
  };

  onPressMonth = month => {
    this.setState({ currentMonth: month.dateString });
  };

  onRequestClose = () => {
    this.setState({ isModal: false, tipText: '' });
  };

  pressDay = date => {
    const { events } = this.props.data.user;

    const filEvents = _.filter(events, element => {
      if (
        formatShowDate(element.start_date, 'YYYY-MM-DD') === date.dateString &&
        element.description &&
        element.description !== 'notify'
      ) {
        return element;
      }
      return null;
    });

    if (filEvents.length >= 1) {
      const btns = [];
      filEvents.forEach(element => btns.push(element.name));
      btns.push('Закрыть');
      ActionSheet.show(
        {
          options: btns,
          cancelButtonIndex: btns.length - 1,
          destructiveButtonIndex: btns.length - 1,
          title: 'Выберите действие'
        },
        buttonIndex => {
          if (buttonIndex !== btns.length - 1) {
            this.props.navigation.navigate('updateevent', { event: filEvents[buttonIndex] });
          }
        }
      );
    }

    this.setState({
      selected: date.dateString,
      selectedDate: date
    });
  };

  renderNotify = () => {
    if (this.props.data && this.props.data.user) {
      const { events } = this.props.data.user;

      const maxDate = _.find(events, elem => {
        const dd = new Date(elem.start_date).getTime();
        return dd > _.now();
      });

      if (maxDate) {
        return (
          <EventNotify
            txtHeader={formatShowDate(maxDate.start_date)}
            txtDescription={maxDate.name}
            image={require('../../assets/images/syringe.png')}
            style={{ position: 'absolute', top: '47%' }}
            onPress={() => this.onPressEvent(maxDate)}
            shadow
          />
        );
      }
    }
    return null;
  };

  renderArrowCalendar = direction => {
    if (direction === 'left') {
      return <Image style={{ height: 20, width: 11 }} source={require('../../assets/images/prev.png')} />;
    }
    return <Image style={{ height: 20, width: 11 }} source={require('../../assets/images/next.png')} />;
  };

  render() {
    let localEvents = [];

    if (this.props.data && this.props.data.user) {
      const { events, tips } = this.props.data.user;

      localEvents = _.filter(events, elem => {
        if (
          formatShowDate(elem.start_date, 'MM-YYYY') === formatShowDate(this.state.currentMonth, 'MM-YYYY') &&
          elem.description !== 'notify'
        ) {
          return elem;
        }

        return null;
      });

      _.forEach(events, event => {
        if (
          formatShowDate(event.start_date, 'DD-MM-YYYY') ===
            formatShowDate(this.state.selectedDate.dateString, 'DD-MM-YYYY') &&
          event.description === 'notify'
        ) {
          localEvents.push(event);
        }
      });

      tips.forEach(item => {
        const { date_birth } = this.props.data.user.activeKid;
        const tipDay = bitrhDateDiff(date_birth, this.state.selectedDate.dateString);
        if (item.day === tipDay) {
          localEvents.push({
            id: item.day,
            start_date: 'Совет дня',
            name: item.description,
            type_event: 14
          });
        }
      });
    }

    return (
      <Root style={{ flex: 1 }}>
        <View
          style={{
            flex: 0.52,
            justifyContent: 'space-between',
            alignItems: 'center'
          }}
        >
          <ImageBackground
            source={require('../../assets/images/back_normal.png')}
            style={{ width: '100%', height: '100%' }}
          >
            <View style={styles.headerContainer}>
              <HeaderButton image={require('../../assets/images/settings.png')} onPress={this.onPressSettings} />
              <Text style={mainStyles.txtHeader}>Календарь</Text>
              <HeaderButton
                image={require('../../assets/images/plus-button.png')}
                onPress={this.onPressAddEvent}
                right
                style={{ alignSelf: 'flex-end' }}
              />
            </View>
            <View style={styles.notifyContainer}>
              <CalendarTodayTip />
              <Calendar
                firstDay={1}
                style={mainStyles.calendar}
                monthFormat="MMMM yyyy"
                renderArrow={direction => this.renderArrowCalendar(direction)}
                onDayPress={day => {
                  this.pressDay(day);
                }}
                onMonthChange={month => this.onPressMonth(month)}
                theme={calendarTheme()}
                markedDates={Object.assign(
                  {
                    [this.state.selected]: {
                      startingDay: true,
                      color: 'white',
                      endingDay: true,
                      textColor: '#648cdf'
                    }
                  },
                  this.state.events_calendar
                )}
                markingType="period"
              />
            </View>
          </ImageBackground>
        </View>
        {this.renderNotify()}
        <View style={styles.eventList}>
          <EventList header onPress={this.onPressEvent} data={localEvents} />
        </View>

        <Modal onRequestClose={this.onRequestClose} animationType="fade" visible={this.state.isModal}>
          <Content padder contentContainerStyle={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <TouchableOpacity onPress={this.onRequestClose}>
              <Text style={[mainStyles.txtModalContent, { margin: 0, fontSize: 20 }]}>{this.state.tipText}</Text>
            </TouchableOpacity>
          </Content>
        </Modal>
      </Root>
    );
  }
}

const styles = {
  headerContainer: {
    flexDirection: 'row',
    padding: 10,
    paddingTop: 20,
    justifyContent: 'space-between',
    alignItems: 'flex-end'
  },
  calendarContainer: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start'
  },
  eventList: {
    flex: 0.48,
    backgroundColor: '#fff',
    flexDirection: 'column',
    justifyContent: 'center'
  },
  notifyContainer: {
    flex: 1,
    flexDirection: 'row',
    padding: 10,
    marginTop: 20,
    justifyContent: 'center',
    alignItems: 'flex-start'
  }
};

export default graphql(query)(MainScreen);
