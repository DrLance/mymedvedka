import React, { Component } from 'react';
import { View, Switch, Text } from 'react-native';

import SwitchCustom from '../components/Switch';
import Tip from '../components/Tip';

const data24 = { name: 'Напомнить за 24 часа', age: '', ageMod: '', isEnable: true };
const data3 = { name: 'Напомнить за 3 дня', age: '', ageMod: '', isEnable: false };
class NeedToBeScreen extends Component {
  state = {};
  render() {
    return (
      <View style={styles.container}>
        <View style={{ margin: 10 }}>
          <Text h4>13.12.2018</Text>
          <Text h3>Нужно сделать прививку</Text>
          <View style={[styles.tableContainer, { height: '10%', backgroundColor: '#e7f1ff', marginTop: 5 }]}>
            <Text style={styles.txtStyle}>Прививка</Text>
            <Text style={styles.txtStyle}>Гипатит Б</Text>
          </View>
          <View style={[styles.tableContainer, { height: '15%', backgroundColor: '#fbfbfb' }]}>
            <View>
              <Text style={styles.txtStyle}>Рекомендуемая дата</Text>
            </View>
            <View style={{ flexDirection: 'column', flex: 0.5, alignItems: 'center', justifyContent: 'center' }}>
              <Text style={styles.txtStyle}>13.04.2017</Text>
              <Text style={styles.txtSmalStyle}>Можно изменить в разделе "Календарь"</Text>
            </View>
          </View>
          <SwitchCustom item={data24} />
          <SwitchCustom item={data3} />
          <View style={styles.containerSwitch}>
            <Text style={styles.txtStyle}>Напомнить о сдаче анализов</Text>
            <Switch onTintColor="#49d060" thumbTintColor="#fefefe" tintColor="#e7e7e7" value={true} />
          </View>
          <Text style={[styles.txtSmalStyle, { width: '80%' }]}>
            Напомнить о необходимости сдать анализы и записаться на прием педиатра
          </Text>
          <Text style={[{ paddingTop: 20, textDecorationLine: 'underline' }, styles.txtStyle]}>
            Датая напоминания: 21.21.21, 17:00
          </Text>
          <Tip
            style={{ paddingTop: 20 }}
            textTip="Назначить прививки можно самостоятельно в разделе &quot;Календарь&quot;"
          />
        </View>
      </View>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    backgroundColor: '#fff'
  },
  tableContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  containerSwitch: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingBottom: 20,
    paddingTop: 20
  },
  txtStyle: {
    color: '#4c5864',
    fontSize: 16
  },
  txtSmalStyle: {
    color: '#919191',
    fontSize: 14
  }
};

export default NeedToBeScreen;
