import React from 'react';
import { ImageBackground, Text, SafeAreaView, StyleSheet } from 'react-native';
import { Content, Button } from 'native-base';

import termText from '../utils/term';

class Term extends React.PureComponent {
  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <ImageBackground
          style={{ flex: 1 }}
          source={require('../../assets/images/background.png')}
          resizeMode="stretch"
        >
          <Button
            onPress={() => {
              this.props.navigation.goBack();
            }}
            full
            style={styles.btn}
          >
            <Text style={styles.text}>Назад</Text>
          </Button>
          <Content padder>
            <Text style={styles.text}>{termText()}</Text>
          </Content>
        </ImageBackground>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  text: {
    color: '#fff',
    fontFamily: 'qunelas-bold'
  },
  btn: {
    backgroundColor: '#5c9fbc',
    alignSelf: 'center',
    width: '80%',
    marginTop: 60
  }
});

export default Term;
