/* eslint-disable global-require */
import React, { Component } from 'react';
import { View, Image, Dimensions } from 'react-native';
import { graphql } from 'react-apollo';
import { Table, TableWrapper, Cell } from 'react-native-table-component';
import { Content } from 'native-base';
import TableItem from '../components/Table/TableItem';
import TableTabBar from '../components/Table/TableTabBar';
import query from '../queries/CurrentUser';
import { formatShowDate, checkFromNow } from '../utils/util';
import mainStyles from '../config/styles';

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

const tableTitle = [
  'первые 24е часа',
  'первые 3-7 дней',
  '2 месяца',
  '3 месяца',
  '4,5 месяца',
  '6 месяцев',
  '7 месяцев',
  '8 месяцев',
  '12 месяцев',
  '15 месяцев',
  '18 месяцев',
  '20 месяцев',
  '6 лет',
  '7 лет',
  '12 - 13 лет',
  '14 лет',
  'Взрослые'
];

const tableData2 = ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''];
const tableVacination = [
  'Энджерикс В, Эувакс В, Регевак В',
  'БЦЖ, БЦЖ-М',
  'Энджерикс В, Эувакс В, Регевак В',
  'Энджерикс В, Эувакс В, Регевак В, АКДС, Пентаксим, Инфанрикс, Акт-Хиб, Хиберикс',
  'АКДС, Пентаксим, Инфанрикс, Акт-Хиб, Хиберикс',
  'Энджерикс В, Эувакс В, Регевак В, АКДС, Пентаксим, Инфанрикс, Акт-Хиб, Хиберикс',
  '',
  '',
  'Энджерикс В, Эувакс В, Регевак В, Приорикс, ЖКВ, ЖПВ',
  '',
  'АДКС, ОПВ, Пентаксим, Инфанрикс, Акт-Хиб, Хиберикс',
  'ОПВ',
  'Приорикс, ЖКВ, ЖПВ',
  'АДС-М, БЦЖ-М',
  'Прививка от вируса и папилломы человека',
  'АДС-М, БЦЖ, ОПВ',
  'АДС-М'
];

type Props = {
  data: any,
  navigation: any
};

class GraphScreen extends Component<Props> {
  static navigationOptions = () => ({
    title: 'График прививок',
    tabBarIcon: ({ tintColor }) => (
      <Image
        style={[
          {
            tintColor,
            height: 24,
            width: 24
          }
        ]}
        source={require('../../assets/images/insert-table.png')}
      />
    ),
    tabBarVisible: false
  });

  gripp = i => {
    if (this.props.data && this.props.data.user && this.props.data.user.events) {
      let startDate = '';
      this.props.data.user.events.forEach(elem => {
        if (elem.description === 'grp_1') {
          startDate = elem.start_date;
        }
      });

      if (i === 11 || i === 12) {
        return <TableItem txtDescription="ЕЖЕГОДНО" />;
      } else if (i === 10) {
        return (
          <TableItem done={checkFromNow(startDate)} txtHead="1-я прививка" txtDescription={formatShowDate(startDate)} />
        );
      }
    }
    return null;
  };

  hipatitB = i => {
    if (this.props.data && this.props.data.user && this.props.data.user.events) {
      const startDate = [];

      this.props.data.user.events.forEach(elem => {
        if (elem.description === 'hpt_1') {
          startDate[0] = elem.start_date;
        } else if (elem.description === 'hpt_2') {
          startDate[1] = elem.start_date;
        } else if (elem.description === 'hpt_3') {
          startDate[2] = elem.start_date;
        }
      });

      if (i === 0) {
        return (
          <TableItem
            done={checkFromNow(startDate[0])}
            txtHead="1-я прививка"
            txtDescription={formatShowDate(startDate[0])}
          />
        );
      } else if (i === 3) {
        return (
          <TableItem
            done={checkFromNow(startDate[1])}
            txtHead="1-я прививка"
            txtDescription={formatShowDate(startDate[1])}
          />
        );
      } else if (i === 5) {
        return (
          <TableItem
            done={checkFromNow(startDate[2])}
            txtHead="2-я прививка"
            txtDescription={formatShowDate(startDate[2])}
          />
        );
      }
    }
    return null;
  };

  tuber = i => {
    if (this.props.data && this.props.data.user && this.props.data.user.events) {
      const startDate = [];
      this.props.data.user.events.forEach(elem => {
        if (elem.description === 'tbr_1') {
          startDate[0] = elem.start_date;
        }
      });
      if (i === 1) {
        return (
          <TableItem
            done={checkFromNow(startDate[0])}
            txtHead="1-я прививка"
            txtDescription={formatShowDate(startDate[0])}
          />
        );
      }
    }
    return null;
  };

  difteria = i => {
    if (this.props.data && this.props.data.user && this.props.data.user.events) {
      const startDate = [];
      this.props.data.user.events.forEach(elem => {
        if (elem.description === 'dks_1') {
          startDate[0] = elem.start_date;
        } else if (elem.description === 'dks_2') {
          startDate[1] = elem.start_date;
        } else if (elem.description === 'dks_3') {
          startDate[2] = elem.start_date;
        } else if (elem.description === 'dks_4') {
          startDate[3] = elem.start_date;
        } else if (elem.description === 'dks_5') {
          startDate[4] = elem.start_date;
        } else if (elem.description === 'dks_6') {
          startDate[5] = elem.start_date;
        } else if (elem.description === 'dks_7') {
          startDate[6] = elem.start_date;
        }
      });
      if (i === 3) {
        return (
          <TableItem
            done={checkFromNow(startDate[0])}
            txtHead="1-я прививка"
            txtDescription={formatShowDate(startDate[0])}
          />
        );
      } else if (i === 4) {
        return (
          <TableItem
            done={checkFromNow(startDate[1])}
            txtHead="2-я прививка"
            txtDescription={formatShowDate(startDate[1])}
          />
        );
      } else if (i === 5) {
        return (
          <TableItem
            done={checkFromNow(startDate[2])}
            txtHead="3-я прививка"
            txtDescription={formatShowDate(startDate[2])}
          />
        );
      } else if (i === 11) {
        return (
          <TableItem
            done={checkFromNow(startDate[3])}
            txtHead="4-я прививка"
            txtDescription={formatShowDate(startDate[3])}
          />
        );
      } else if (i === 13) {
        return (
          <TableItem
            done={checkFromNow(startDate[4])}
            txtHead="5-я прививка"
            txtDescription={formatShowDate(startDate[4])}
          />
        );
      } else if (i === 15) {
        return (
          <TableItem
            done={checkFromNow(startDate[5])}
            txtHead="6-я прививка"
            txtDescription={formatShowDate(startDate[5])}
          />
        );
      } else if (i === 16) {
        return (
          <TableItem
            done={checkFromNow(startDate[6])}
            txtHead="7-я прививка"
            txtDescription={formatShowDate(startDate[6])}
          />
        );
      }
    }
    return null;
  };

  poli = i => {
    if (this.props.data && this.props.data.user && this.props.data.user.events) {
      const startDate = [];
      this.props.data.user.events.forEach(elem => {
        if (elem.description === 'pol_1') {
          startDate[0] = elem.start_date;
        } else if (elem.description === 'pol_2') {
          startDate[1] = elem.start_date;
        } else if (elem.description === 'pol_3') {
          startDate[2] = elem.start_date;
        } else if (elem.description === 'pol_4') {
          startDate[3] = elem.start_date;
        } else if (elem.description === 'pol_5') {
          startDate[4] = elem.start_date;
        }
      });
      if (i === 3) {
        return (
          <TableItem
            done={checkFromNow(startDate[0])}
            txtHead="1-я прививка"
            txtDescription={formatShowDate(startDate[0])}
          />
        );
      } else if (i === 4) {
        return (
          <TableItem
            done={checkFromNow(startDate[1])}
            txtHead="2-я прививка"
            txtDescription={formatShowDate(startDate[1])}
          />
        );
      } else if (i === 5) {
        return (
          <TableItem
            done={checkFromNow(startDate[2])}
            txtHead="3-я прививка"
            txtDescription={formatShowDate(startDate[2])}
          />
        );
      } else if (i === 12) {
        return (
          <TableItem
            done={checkFromNow(startDate[3])}
            txtHead="4-я прививка"
            txtDescription={formatShowDate(startDate[3])}
          />
        );
      } else if (i === 15) {
        return (
          <TableItem
            done={checkFromNow(startDate[4])}
            txtHead="5-я прививка"
            txtDescription={formatShowDate(startDate[4])}
          />
        );
      }
    }
    return null;
  };

  cor = i => {
    if (this.props.data && this.props.data.user && this.props.data.user.events) {
      const startDate = [];
      this.props.data.user.events.forEach(elem => {
        if (elem.description === 'kkp_1') {
          startDate[0] = elem.start_date;
        } else if (elem.description === 'kkp_2') {
          startDate[1] = elem.start_date;
        }
      });
      if (i === 8) {
        return (
          <TableItem
            done={checkFromNow(startDate[0])}
            txtHead="1-я прививка"
            txtDescription={formatShowDate(startDate[0])}
          />
        );
      } else if (i === 12) {
        return (
          <TableItem
            done={checkFromNow(startDate[1])}
            txtHead="2-я прививка"
            txtDescription={formatShowDate(startDate[1])}
          />
        );
      }
    }
    return null;
  };

  navCalendar() {
    this.props.navigation.navigate('calendar');
  }

  render() {
    return (
      <View style={{ backgroundColor: 'transparent', flex: 1 }}>
        <View style={{ flexDirection: 'row' }}>
          <TableTabBar navigation={this.props.navigation} onPressCalendar={this.navCalendar} />
          <Content horizontal={true}>
            <Content>
              <Table style={{ flexDirection: 'row', flex: 1 }} borderStyle={{ borderColor: '#332efa', borderWidth: 1 }}>
                <TableWrapper style={styles.tableWrap}>
                  <Cell data={['Инфекция/\nВозраст']} style={styles.head} textStyle={styles.textMainHead} />
                  {tableTitle.map((title, i) => (
                    <Cell key={i} data={title} style={styles.title} textStyle={styles.textHead} />
                  ))}
                </TableWrapper>
                <TableWrapper style={styles.tableWrap}>
                  <Cell data={['Гепатит B']} style={styles.head} textStyle={styles.textSecondHead} />
                  {tableData2.map((title, i) => (
                    <Cell
                      key={i}
                      data={this.hipatitB(i)}
                      style={[
                        styles.row,
                        {
                          backgroundColor: '#ffbaba'
                        }
                      ]}
                    />
                  ))}
                </TableWrapper>
                <TableWrapper style={styles.tableWrap}>
                  <Cell data={['Туберкулез']} style={styles.head} textStyle={styles.textSecondHead} />
                  {tableData2.map((title, i) => (
                    <Cell
                      key={i}
                      data={this.tuber(i)}
                      style={[
                        styles.row,
                        {
                          backgroundColor: '#ffc3d4'
                        }
                      ]}
                    />
                  ))}
                </TableWrapper>
                <TableWrapper style={styles.tableWrap}>
                  <Cell
                    data={['Дифтерия,\n коклюш,\n столбняк']}
                    style={styles.head}
                    textStyle={styles.textSecondHead}
                  />
                  {tableData2.map((title, i) => (
                    <Cell key={i} data={this.difteria(i)} style={[styles.row, { backgroundColor: '#ffefb7' }]} />
                  ))}
                </TableWrapper>
                <TableWrapper style={styles.tableWrap}>
                  <Cell data={['Полиомиелит']} style={styles.head} textStyle={styles.textSecondHead} />
                  {tableData2.map((title, i) => (
                    <Cell
                      key={i}
                      data={this.poli(i)}
                      style={[
                        styles.row,
                        {
                          backgroundColor: '#ddffb9'
                        }
                      ]}
                    />
                  ))}
                </TableWrapper>
                <TableWrapper style={styles.tableWrap}>
                  <Cell data={['Корь,\n краснуха,\n паротит']} style={styles.head} textStyle={styles.textSecondHead} />
                  {tableData2.map((title, i) => (
                    <Cell key={i} data={this.cor(i)} style={[styles.row, { backgroundColor: '#faffb0' }]} />
                  ))}
                </TableWrapper>
                <TableWrapper style={styles.tableWrap}>
                  <Cell data={['Грипп']} style={styles.head} textStyle={styles.textSecondHead} />
                  {tableData2.map((title, i) => (
                    <Cell key={i} data={this.gripp(i)} style={[styles.row, { backgroundColor: '#acddff' }]} />
                  ))}
                </TableWrapper>
                <TableWrapper style={{ width: 250 }}>
                  <Cell data={['Вакцина']} style={styles.head} textStyle={styles.textSecondHead} />
                  {tableData2.map((title, i) => (
                    <Cell
                      key={i}
                      data={tableVacination[i]}
                      textStyle={[mainStyles.txtTableItemDescr, { marginLeft: 6 }]}
                      style={[styles.row, { backgroundColor: '#acddff' }]}
                    />
                  ))}
                </TableWrapper>
              </Table>
            </Content>
          </Content>
        </View>
      </View>
    );
  }
}

const heightHead = (WIDTH - 60) / 13;
const styles = {
  head: {
    backgroundColor: '#3888ff',
    height: 60
  },
  textHead: {
    textAlign: 'center',
    color: '#fff',
    fontFamily: 'qunelas-medium',
    fontSize: HEIGHT / 62
  },
  textMainHead: {
    textAlign: 'center',
    color: '#fff',
    fontFamily: 'qunelas-semi-bold',
    fontSize: HEIGHT / 55
  },
  textSecondHead: {
    textAlign: 'center',
    color: '#fff',
    fontFamily: 'qunelas-semi-bold',
    fontSize: HEIGHT / 71
  },
  title: { height: heightHead, backgroundColor: '#3888ff' },
  row: { height: heightHead },
  text: { textAlign: 'center', color: '#fff', fontFamily: 'qunelas-medium', alignSelf: 'center' },
  tableWrap: {
    width: HEIGHT / 7.7
  }
};

export default graphql(query)(GraphScreen);
