/* eslint-disable global-require */
import React, { Component } from 'react';
import { View, Image, ImageBackground, Text, TextInput, Keyboard } from 'react-native';
import HeaderButton from '../components/HeaderButton';
import EventNotify from '../components/EventNotify';
import EventList from '../components/EventList';
import mainStyles from '../config/styles';
import data from '../data/dataNormal';

type Props = {
  navigation: Function
};

class NormalyzScreen extends Component<Props> {
  static navigationOptions = ({ navigation, screenProps }) => ({
    title: 'Нормы и анализы',
    headerTintColor: 'black',
    tabBarIcon: ({ tintColor }) => (
      <Image
        source={require('../../assets/images/prescription.png')}
        style={[
          {
            tintColor,
            height: 22,
            width: 19
          }
        ]}
      />
    )
  });

  state = { age: '', height: '', weight: '' };

  onPressSettings = () => {
    this.props.navigation.navigate('settings');
  };

  onPressItemList = item => {
    this.props.navigation.navigate('table', {
      pathName: item.pathName,
      kidInfo: {
        age: this.state.age,
        height: this.state.height,
        weight: this.state.weight
      }
    });
  };

  onChangeTextHandler = (value, type) => {
    const newValue = value.substring(0, value.indexOf(' ') + 1);
    console.log(newValue);
    switch (type) {
      case 1:
        this.setState({ age: value });
        break;
      case 2:
        this.setState({ height: value });
        break;
      case 3:
        this.setState({ weight: value });
        break;
      default:
        break;
    }
    return null;
  };

  onPressDetails = () => {
    Keyboard.dismiss();
    this.props.navigation.navigate('chart', {
      age: this.state.age,
      height: this.state.height,
      weight: this.state.weight
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.containerTop}>
          <ImageBackground
            source={require('../../assets/images/back_normal.png')}
            style={{
              width: '100%',
              height: '100%',
              justifyContent: 'space-between'
            }}
          >
            <View style={styles.headerContainer}>
              <HeaderButton image={require('../../assets/images/settings.png')} onPress={this.onPressSettings} />
              <Text style={mainStyles.txtHeader}>Нормы анализов вашего ребенка</Text>
            </View>
            <View
              style={{
                justifyContent: 'center',
                alignContent: 'center'
              }}
            >
              <View style={styles.containerInput}>
                <View style={{ alignSelf: 'flex-end', width: '100%' }}>
                  <Text style={[mainStyles.txtNormContent, { textAlign: 'right' }]}>Возраст:</Text>
                </View>
                <View style={{ justifyContent: 'flex-start', width: '100%' }}>
                  <TextInput
                    onChangeText={value => this.onChangeTextHandler(value, 1)}
                    value={this.state.age}
                    underlineColorAndroid="transparent"
                    placeholder="___мес"
                    maxLength={6}
                    placeholderTextColor="white"
                    keyboardType="numeric"
                    style={[mainStyles.txtNormContentValue, { marginTop: 5, marginLeft: 5, width: 80 }]}
                  />
                </View>
              </View>
              <View style={styles.containerInput}>
                <View style={{ alignSelf: 'flex-end', width: '100%' }}>
                  <Text style={[mainStyles.txtNormContent, { marginLeft: 10, textAlign: 'right' }]}>Рост:</Text>
                </View>
                <View style={{ justifyContent: 'flex-start', width: '100%' }}>
                  <TextInput
                    onChangeText={value => this.onChangeTextHandler(value, 2)}
                    value={this.state.height}
                    underlineColorAndroid="transparent"
                    placeholder="___см"
                    maxLength={5}
                    placeholderTextColor="white"
                    keyboardType="numeric"
                    style={[mainStyles.txtNormContentValue, { marginTop: 5, marginLeft: 5 }]}
                  />
                </View>
              </View>
              <View style={styles.containerInput}>
                <View style={{ alignSelf: 'flex-end', width: '100%' }}>
                  <Text style={[mainStyles.txtNormContent, { marginLeft: 10, textAlign: 'right' }]}>Вес:</Text>
                </View>
                <View style={{ justifyContent: 'flex-start', width: '100%' }}>
                  <TextInput
                    onChangeText={value => this.onChangeTextHandler(value, 3)}
                    value={`${this.state.weight}`}
                    underlineColorAndroid="transparent"
                    placeholder="___кг"
                    maxLength={5}
                    placeholderTextColor="white"
                    keyboardType="numeric"
                    style={[mainStyles.txtNormContentValue, { marginTop: 5, marginLeft: 5 }]}
                  />
                </View>
              </View>
            </View>
            <EventNotify
              txtDescription="График нормы роста и веса"
              onPress={this.onPressDetails}
              style={{ marginBottom: 20 }}
            />
          </ImageBackground>
        </View>
        <View style={styles.listContainer}>
          <EventList data={data} onPress={this.onPressItemList} />
        </View>
      </View>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    flexDirection: 'column'
  },
  containerTop: {
    flex: 0.52,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column'
  },

  containerInput: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  headerContainer: {
    flexDirection: 'row',
    padding: 10,
    marginTop: 11,
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  listContainer: {
    flex: 0.48,
    backgroundColor: '#fff',
    flexDirection: 'column',
    justifyContent: 'center'
  }
};

export default NormalyzScreen;
