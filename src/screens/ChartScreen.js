import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import { VictoryChart, VictoryLine, VictoryScatter, VictoryTheme } from 'victory-native';
import { Icon } from 'native-base';
import mainStyles from '../config/styles';
import { getWeightXY } from '../utils/util';

class ChartScreen extends Component {
  static navigationOptions = ({ navigation, screenProps }) => ({
    headerTintColor: '#525252',
    headerStyle: {
      backgroundColor: '#fff'
    },
    title: 'Вернуться',
    headerLeft: (
      <Icon
        name="arrow-back"
        style={{ marginLeft: 20, color: '#525252', fontSize: 32 }}
        onPress={() => navigation.navigate('normal')}
      />
    ),
    tabBarIcon: ({ tintColor }) => (
      <Image source={require('../../assets/images/school-calendar.png')} style={[{ tintColor }]} />
    )
  });

  onPressSettings = () => {
    this.props.navigation.navigate('settings');
  };
  render() {
    const { age, height, weight } = this.props.navigation.state.params;

    const { normH, normW, downH, downW } = getWeightXY(age);

    const highNormW = parseFloat(normW / 1000);
    const highNormH = parseFloat(normH);

    const downNormH = parseFloat(downH);
    const downNormW = parseFloat(downW / 1000);

    return (
      <View style={styles.container}>
        <View style={{ alignItems: 'stretch' }}>
          <Text style={[mainStyles.txtChart, { marginBottom: -40, marginTop: 30 }]}>Рост, см</Text>
          <VictoryChart domain={{ x: [0, 20], y: [0, 150] }} theme={VictoryTheme.material}>
            <VictoryLine
              style={{
                data: { stroke: '#ff6974' },
                parent: { border: '1px solid #ccc' }
              }}
              data={[{ x: 0, y: 0 }, { x: 11, y: 150 }]}
            />
            <VictoryLine
              style={{
                data: { stroke: '#79fe68' },
                parent: { border: '1px solid #ccc' }
              }}
              data={[{ x: 0, y: 0 }, { x: 20, y: 110 }]}
            />
            <VictoryLine
              style={{
                data: { stroke: '#fff83e' },
                parent: { border: '1px solid #ccc' }
              }}
              data={[{ x: 0, y: 0 }, { x: 17, y: 150 }]}
            />
            <VictoryScatter
              style={{ data: { fill: '#0086f9' } }}
              size={7}
              data={[{ x: parseInt(weight) ? parseInt(weight) : 0, y: parseInt(height) ? parseInt(height) : 0 }]}
            />
          </VictoryChart>
          <Text style={[mainStyles.txtChart, { textAlign: 'right', paddingRight: 30, marginTop: -20 }]}>Вес, кг</Text>
        </View>
        <View style={{ flexDirection: 'row', marginLeft: 30, alignItems: 'center' }}>
          <Text style={{ backgroundColor: '#ff6974', width: 100, height: 10 }} />
          <Text style={mainStyles.txtChart}>Верхняя граница нормы</Text>
        </View>
        <View style={{ flexDirection: 'row', marginLeft: 30, alignItems: 'center' }}>
          <Text style={{ backgroundColor: '#fff83e', width: 100, height: 10 }} />
          <Text style={mainStyles.txtChart}>Возраст</Text>
        </View>
        <View style={{ flexDirection: 'row', marginLeft: 30, alignItems: 'center' }}>
          <Text style={{ backgroundColor: '#79fe68', width: 100, height: 10 }} />
          <Text style={mainStyles.txtChart}>Нижняя граница нормы</Text>
        </View>
        <View style={{ flexDirection: 'row', marginLeft: 30, alignItems: 'center' }}>
          <Text style={{ backgroundColor: '#0086f9', width: 100, height: 10 }} />
          <Text style={mainStyles.txtChart}>Ваш ребенок</Text>
        </View>
      </View>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'flex-start',
    backgroundColor: '#fff'
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: 10,
    marginBottom: '10%'
  },
  containerChart: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
    backgroundColor: 'transparent'
  }
};

export default ChartScreen;
