/* eslint-disable global-require */
import React, { Component } from 'react';
import { graphql, compose } from 'react-apollo';
import { ImageBackground, DatePickerIOS, DatePickerAndroid, Platform, Text, Image } from 'react-native';
import { Button, Form, Item, Label, Input, Content } from 'native-base';
import _ from 'lodash';
import mutation from '../mutations/Register';
import { addEvent } from '../mutations/Event';
import query from '../queries/CurrentUser';

import { formatShowDate, formatDateForMongo, validateEmail, calculateNotifications } from '../utils/util';

type Props = {
  mutate: Function,
  navigation: any
};

class LoginScreen extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      date_birth: 'Нажмите что бы выбрать дату',
      isInvalidPassword: false,
      isInvalidEmail: false,
      password: '',
      repeatPassword: '',
      email: '',
      name: '',
      iosDate: new Date(),
      renderIosPicker: false,
      errors: []
    };
  }

  formatIosDate(date) {
    this.setState({ date_birth: formatShowDate(date, 'DD-MM-YYYY'), iosDate: date });
  }

  checkForm = () => {
    if (this.state.password.length === 0 || this.state.password !== this.state.repeatPassword) {
      return this.setState({ isInvalidPassword: true });
    }
    if (this.state.password === this.state.repeatPassword) {
      this.setState({ isInvalidPassword: false });
    }

    if (!validateEmail(this.state.email)) {
      return this.setState({ isInvalidEmail: true });
    }
    this.setState({ isInvalidEmail: false });

    if (!this.state.isInvalidEmail && !this.state.isInvalidPassword) {
      const { email, password, name, date_birth } = this.state;
      let date = '';

      date = formatDateForMongo(date_birth, 'DD-MM-YYYY');

      this.props
        .mutate({
          variables: { email: _.trim(email.toLowerCase()), password: _.trim(password), name, dateBirth: date },
          refetchQueries: [{ query }]
        })
        .then(({ data }) => {
          calculateNotifications(date);
        })
        .catch(e => {
          if (e && e.graphQLErrors) {
            const errors = e.graphQLErrors.map(eer => eer.message);
            this.setState({ errors });
          }
        });
    }
    return null;
  };

  checkDataPicker = () => {
    if (Platform.OS !== 'ios') {
      return this.renderDataPicker();
    }

    return this.setState({ renderIosPicker: true });
  };

  renderDataPicker = async () => {
    const { action, year, month, day } = await DatePickerAndroid.open({ date: new Date() });

    if (action !== DatePickerAndroid.dismissedAction) {
      const date = formatShowDate(new Date(year, month, day), 'DD-MM-YYYY');

      this.setState({ date_birth: date });
    }
  };

  render() {
    return (
      <ImageBackground
        style={styles.imgContainer}
        source={require('../../assets/images/background.png')}
        resizeMode="stretch"
      >
        <Content padder>
          <Image
            style={{ height: 124, width: 100, marginVertical: 20, alignSelf: 'center' }}
            source={require('../../assets/images/romantic-teddy-bear.png')}
            resizeMode="contain"
          />
          <Form style={{ marginTop: -20 }}>
            <Item error={this.state.isInvalidEmail}>
              <Label style={styles.text}>Email:</Label>
              <Input
                keyboardType="email-address"
                style={styles.text}
                onChangeText={text => this.setState({ email: text })}
              />
            </Item>
            <Item error={this.state.isInvalidPassword}>
              <Label style={styles.text}>Пароль:</Label>
              <Input style={styles.text} onChangeText={text => this.setState({ password: text })} secureTextEntry />
            </Item>
            <Item error={this.state.isInvalidPassword}>
              <Label style={styles.text}>Повторите пароль:</Label>
              <Input
                style={styles.text}
                onChangeText={text => this.setState({ repeatPassword: text })}
                secureTextEntry
              />
            </Item>
            <Item>
              <Label style={styles.text}>Имя ребенка:</Label>
              <Input style={styles.text} onChangeText={text => this.setState({ name: text })} />
            </Item>
            <Item onPress={() => this.checkDataPicker()}>
              <Label style={styles.text}>Дата рождения:</Label>
              <Input style={styles.text} disabled placeholder={this.state.date_birth} placeholderTextColor="#fff" />
            </Item>
            {this.state.renderIosPicker ? (
              <DatePickerIOS mode="date" date={this.state.iosDate} onDateChange={date => this.formatIosDate(date)} />
            ) : null}
            <Item style={{ borderBottomWidth: 0, marginTop: 6 }} onPress={() => this.props.navigation.navigate('term')}>
              <Label style={[styles.text, { fontSize: 12 }]}>
                {'Совершая регистрацию в данном приложении вы соглашаетесь c политикой конфидициальности.'}
              </Label>
            </Item>
          </Form>
          <Label error style={[styles.text, { color: 'red', alignSelf: 'center', marginTop: 10 }]}>
            {this.state.errors}
          </Label>

          <Button
            block
            transparent
            style={styles.btn}
            onPress={() => {
              this.checkForm();
            }}
          >
            <Text style={styles.text}>Регистрация</Text>
          </Button>
          <Button
            block
            transparent
            style={styles.btn}
            onPress={() => {
              this.props.navigation.goBack();
            }}
          >
            <Text style={styles.text}>Вернуться назад</Text>
          </Button>
        </Content>
      </ImageBackground>
    );
  }
}

const styles = {
  imgContainer: { flex: 1 },
  btn: {
    backgroundColor: '#5c9fbc',
    width: '80%',
    alignSelf: 'center',
    marginTop: 10
  },
  text: {
    color: '#fff',
    fontFamily: 'qunelas-bold'
  },
  imgIcon: {
    alignSelf: 'center',
    marginVertical: Platform.OS === 'ios' ? '-25%' : '-15%'
  }
};
export default compose(
  graphql(query),
  graphql(mutation),
  graphql(addEvent, { name: 'addEvent' })
)(LoginScreen);
