import React, { Component } from 'react';
import { View, Text, Image, AsyncStorage } from 'react-native';
import { Icon, Button, H2, Container, Content } from 'native-base';
import { Camera, Permissions } from 'expo';
import Tip from '../components/Tip';

class PhotoScreen extends Component {
  static navigationOptions = ({ navigation, screenProps }) => {
    return {
      headerTintColor: '#525252',
      headerStyle: { backgroundColor: '#fff' },
      title: 'Вернуться',
      headerLeft: (
        <Icon
          name="arrow-back"
          style={{ marginLeft: 20, color: '#525252', fontSize: 32 }}
          onPress={() => navigation.navigate('medical')}
        />
      ),
      tabBarIcon: ({ tintColor }) => (
        <Image source={require('../../assets/images/school-calendar.png')} style={[{ tintColor }]} />
      )
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      hasCameraPermission: null,
      type: Camera.Constants.Type.back,
      photo: null
    };
  }

  async componentWillMount() {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    const photo = await AsyncStorage.getItem('@Settings:photo');
    if (photo !== null) {
      this.setState({ photo: { uri: photo } });
    }
    this.setState({ hasCameraPermission: status === 'granted' });
  }

  onPressTake = async () => {
    if (this.camera) {
      const photo = await this.camera.takePictureAsync({ quality: 0.5, exif: true });
      await AsyncStorage.setItem('@Settings:photo', photo.uri);
      this.setState({ photo });
    }
  };

  renderCamera() {
    const { hasCameraPermission } = this.state;
    if (hasCameraPermission === null) {
      return <View />;
    } else if (hasCameraPermission === false) {
      return <Text style={styles.txtBtn}>Нет доступа к камере</Text>;
    } else if (this.state.photo) {
      return <Image style={styles.containerPhoto} source={this.state.photo} />;
    }
    return (
      <Camera
        ref={ref => {
          this.camera = ref;
        }}
        style={styles.containerPhoto}
        type={this.state.type}
      />
    );
  }
  render() {
    return (
      <Container style={{ backgroundColor: '#fff' }}>
        <Content padder>
          <H2 style={styles.txtHeader}>График работы прививочного кабиента</H2>
          <View style={{ alignItems: 'center' }}>{this.renderCamera()}</View>
          <View style={styles.containerButtons}>
            <Button large style={{ backgroundColor: '#e7f1ff', width: '40%' }} onPress={this.onPressTake}>
              <Text style={styles.txtBtn}>Сделать фото</Text>
            </Button>
            <Button
              large
              style={{ backgroundColor: '#e5e5e6', width: '40%' }}
              onPress={() => {
                AsyncStorage.removeItem('@Settings:photo');
                this.setState({ photo: null });
              }}
            >
              <Text style={styles.txtBtn}>Удалить фото</Text>
            </Button>
          </View>
          <Tip
            style={{ marginTop: 10 }}
            textTip="Сфотографируйте график работы прививочного кабинета при посещении поликлиники."
          />
        </Content>
      </Container>
    );
  }
}

const styles = {
  containerPhoto: {
    borderWidth: 1,
    borderColor: '#e7f1ff',
    borderRadius: 4,
    alignItems: 'center',
    justifyContent: 'center',
    height: 250,
    width: '100%'
  },
  containerButtons: {
    flexDirection: 'row',
    marginTop: 10,
    justifyContent: 'space-around'
  },
  txtHeader: {
    fontFamily: 'qunelas-semi-bold',
    textAlign: 'center',
    color: '#4b4b4b'
  },
  txtBtn: {
    fontFamily: 'qunelas-semi-bold',
    fontSize: 16,
    color: '#4b4b4b',
    textAlign: 'center',
    flex: 1
  }
};

export default PhotoScreen;
