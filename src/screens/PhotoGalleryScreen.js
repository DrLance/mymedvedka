/* eslint-disable global-require */
import React, { Component } from 'react';
import { Text, Image, View, TouchableOpacity, Modal } from 'react-native';
import { Container, Content, Icon } from 'native-base';
import { Query } from 'react-apollo';

import query from '../queries/GetPhotoGallery';
import mainStyles from '../config/styles';

type Props = {
  navigation: any
};

const HOST = 'http://188.225.56.67:4000/event_image/';

class PhotoGalleryScreen extends Component<Props> {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTintColor: '#525252',
      headerStyle: { backgroundColor: '#fff' },
      title: 'Вернуться',
      tabBarVisible: false,
      headerLeft: <Icon name="arrow-back" style={mainStyles.headerBack} onPress={() => navigation.goBack(null)} />
    };
  };

  state = {
    imageUri: null,
    isModal: false
  };

  onPressImage = imageId => {
    this.setState({ imageUri: `${HOST}${imageId}`, isModal: true });
  };

  onRequestClose = () => {
    this.setState({ isModal: false });
  };

  photoQuery = () => (
    <Query query={query} variables={{ folder_id: this.props.navigation.state.params.folder_id }}>
      {({ loading, error, data }) => {
        if (loading) return null;
        if (error) return `Error!: ${error}`;

        return data.user.getFiles.map(e => (
          <TouchableOpacity key={e.file_id} onPress={() => this.onPressImage(e.file_id)}>
            <View>
              <Image
                resizeMode="center"
                style={{ height: 150, width: 250 }}
                source={{ uri: `${HOST}${e.file_id}`, cache: 'force-cache' }}
              />
              <Text style={[mainStyles.txtAddEventName, { textAlign: 'center' }]}>{e.name}</Text>
            </View>
          </TouchableOpacity>
        ));
      }}
    </Query>
  );

  render() {
    return (
      <Container style={{ backgroundColor: '#fff' }}>
        <Content contentContainerStyle={styles.contentContainer}>{this.photoQuery()}</Content>
        <Modal onRequestClose={this.onRequestClose} animationType="fade" visible={this.state.isModal}>
          <View style={{ flex: 1 }}>
            <TouchableOpacity style={{ flex: 1 }} onPress={this.onRequestClose}>
              <Image resizeMode="contain" source={{ uri: this.state.imageUri }} style={{ flex: 1 }} />
            </TouchableOpacity>
          </View>
        </Modal>
      </Container>
    );
  }
}

const styles = {
  contentContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'space-around'
  }
};

export default PhotoGalleryScreen;
