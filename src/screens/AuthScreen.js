/* eslint-disable global-require */
import React, { Component } from 'react';
import { ImageBackground, Text, Image, AsyncStorage, View } from 'react-native';
import { graphql } from 'react-apollo';
import { Button, Form, Item, Label, Input, Content, CheckBox, Body, ListItem } from 'native-base';
import mutation from '../mutations/Login';
import query from '../queries/CurrentUser';

type Props = {
  navigation: any,
  data: Object,
  mutate: Function
};

class AuthScreen extends Component<Props> {
  state = {
    errors: [],
    acceptTerms: false,
    password: '',
    email: ''
  };

  async componentDidMount() {
    const email = await AsyncStorage.getItem('@Auth:email');
    const password = await AsyncStorage.getItem('@Auth:password');
    const acceptTerms = await AsyncStorage.getItem('@Auth:acceptTerms');

    if (email !== null && password !== null && acceptTerms !== null) {
      this.setState({ email, password, acceptTerms: true }, () => this.onSubmit());
    }
  }

  onChangeHadler = () => {
    this.setState({ acceptTerms: !this.state.acceptTerms }, () =>
      AsyncStorage.setItem('@Auth:acceptTerms', this.state.acceptTerms.toString()));
  };

  onSubmit = () => {
    const { email, password, acceptTerms } = this.state;
    if (email && password && this.props.data && acceptTerms) {
      this.props
        .mutate({
          variables: { email, password },
          refetchQueries: [{ query }]
        })
        .then(({ data }) => {
          if (data) {
            this.setState({ errors: [] });
            AsyncStorage.setItem('@Auth:email', email);
            AsyncStorage.setItem('@Auth:password', password);
            AsyncStorage.setItem('@Auth:id', data.login.id);
            this.props.navigation.navigate('main');
          }
        })
        .catch(res => {
          if (res && res.graphQLErrors) {
            const errors = res.graphQLErrors.map(error => error.message);
            this.setState({ errors });
          }
        });
    }
  };

  render() {
    return (
      <ImageBackground style={styles.imgContainer} source={require('../../assets/images/background.png')}>
        <Content padder>
          <View style={{ alignSelf: 'center' }}>
            <Image
              style={{ height: 124, width: 100, marginVertical: 65 }}
              source={require('../../assets/images/romantic-teddy-bear.png')}
              resizeMode="contain"
            />
          </View>
          <Form style={{ marginTop: -40 }}>
            <Item error={this.state.isInvalidEmail}>
              <Label style={styles.text}>Email:</Label>
              <Input
                keyboardType="email-address"
                style={styles.text}
                onChangeText={email => this.setState({ email })}
                value={this.state.email}
              />
            </Item>
            <Item error={this.state.isInvalidPassword}>
              <Label style={styles.text}>Пароль:</Label>
              <Input
                value={this.state.password}
                style={styles.text}
                onChangeText={password => this.setState({ password })}
                secureTextEntry
              />
            </Item>
            <ListItem style={{ borderBottomWidth: 0 }}>
              <CheckBox checked={this.state.acceptTerms} onPress={this.onChangeHadler} />
              <Body>
                <Label onPress={() => this.props.navigation.navigate('term')} style={[styles.text, { marginLeft: 6 }]}>
                  Соглашаюсь с политикой конфидициальности
                </Label>
              </Body>
            </ListItem>
          </Form>
          <Label error style={[styles.text, { color: '#ff6974', alignSelf: 'center', marginTop: 10 }]}>
            {this.state.errors}
          </Label>
          <Button block transparent style={styles.btn} onPress={this.onSubmit}>
            <Text style={styles.text}>Войти</Text>
          </Button>
          <Button
            block
            transparent
            style={styles.btn}
            onPress={() => {
              this.props.navigation.navigate('welcome');
            }}
          >
            <Text style={styles.text}>Вернуться назад</Text>
          </Button>
        </Content>
      </ImageBackground>
    );
  }
}

const styles = {
  imgContainer: { flex: 1 },
  btn: {
    backgroundColor: '#5c9fbc',
    width: '80%',
    alignSelf: 'center',
    marginTop: 20
  },
  text: {
    color: '#fff',
    backgroundColor: 'transparent',
    fontFamily: 'qunelas-bold'
  },
  imgIcon: {
    alignSelf: 'center',
    marginVertical: -70
  }
};

export default graphql(query)(graphql(mutation)(AuthScreen));
