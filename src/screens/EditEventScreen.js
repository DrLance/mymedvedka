import React, { Component } from 'react';
import { View, Switch, Text, Platform, DatePickerAndroid, DatePickerIOS, Modal } from 'react-native';
import _ from 'lodash';
import { Icon, H2, Button, Container, Content } from 'native-base';
import { graphql, compose } from 'react-apollo';
import moment from 'moment';

import SwitchCustom from '../components/Switch';
import Tip from '../components/Tip';

import mainStyles from '../config/styles';
import { formatShowDate, addNotificationDate, addNotification } from '../utils/util';

import query from '../queries/CurrentUser';

import { delEvent } from '../mutations/Event';

type Props = {
  navigation: Function,
  delEvent: Function,
  data: Object
};

class EditEventScreen extends Component<Props> {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTintColor: '#525252',
      headerStyle: { backgroundColor: '#fff' },
      title: 'Вернуться',
      headerLeft: <Icon name="arrow-back" style={mainStyles.headerBack} onPress={() => navigation.goBack(null)} />,
      tabBarVisible: false
    };
  };

  state = {
    isRemind: false,
    datePicker: new Date(),
    isModal: false,
    renderIosPicker: true,
    reminder_date: '',
    remind24: {
      name: 'Напомнить за 24 часа',
      age: '',
      ageMod: '',
      isEnable: false
    },
    remind3: {
      name: 'Напомнить за 3 дня',
      age: '',
      ageMod: '',
      isEnable: false
    }
  };

  onPressDel = () => {
    const { eventId } = this.props.navigation.state.params;
    this.props.delEvent({ variables: { id: eventId }, refetchQueries: [{ query }] }).then(({ data }) => {
      if (data) {
        this.props.navigation.goBack(null);
      }
    });

    return null;
  };

  onChangeDateIOS = date => {
    const newDate = moment(date).add(2, 'minutes');

    this.setState({ datePicker: date, reminder_date: formatShowDate(newDate, 'DD-MM-YYYY HH:MM') });
  };

  changeNotificationHandler = async (type, value) => {
    const { user } = this.props.data;
    if (!user) {
      return null;
    }
    const { eventId } = this.props.navigation.state.params;
    const event = _.find(user.events, element => element.id === eventId);
    let currentDateNotify = addNotificationDate(event.start_date, type, type === 24);
    const newDate = new Date(currentDateNotify);
    let isRemind = false;
    const typeEvent = event.type_event == 1 ? 'Назначена прививка ' : 'Назначен прием врача ';

    const { remind3, remind24 } = this.state;

    if (type === 3) {
      remind3.isEnable = value;
    } else if (type === 24) {
      remind24.isEnable = value;
    } else {
      isRemind = value;
      currentDateNotify = addNotificationDate(event.start_date, type, type === 24);
    }

    if (value && newDate > new Date()) {
      addNotification(newDate, typeEvent + event.name, formatShowDate(newDate, 'DD-MM-YYYY'));

      return this.setState({
        remind24,
        remind3,
        isRemind,
        reminder_date: currentDateNotify.format('DD-MM-YYYY HH:MM')
      });
    }

    this.checkDataPicker(event.name);
    this.setState({
      remind24,
      remind3,
      isRemind,
      reminder_date: formatShowDate(this.state.datePicker, 'DD-MM-YYYY HH:MM')
    });
  };

  checkDataPicker = name => {
    if (Platform.OS !== 'ios') {
      return this.renderDataPicker(name);
    }

    return this.setState({ renderIosPicker: true, isModal: true });
  };

  closeModal = async () => {
    this.setState({ isModal: false });

    const { user } = this.props.data;
    if (!user) {
      return null;
    }
    const { eventId } = this.props.navigation.state.params;
    const event = _.find(user.events, element => element.id === eventId);
    const typeEvent = event.type_event == 1 ? 'Назначена прививка ' : 'Назначен прием врача ';

    addNotification(this.state.datePicker, typeEvent + event.name, formatShowDate(this.state.datePicker, 'DD-MM-YYYY'));
  };

  renderDataPicker = async name => {
    const { eventId } = this.props.navigation.state.params;
    const { user } = this.props.data;
    const event = _.find(user.events, element => element.id === eventId);
    const typeEvent = event.type_event == 1 ? 'Назначена прививка ' : 'Назначен прием врача ';

    const { action, year, month, day } = await DatePickerAndroid.open({ date: new Date(), mode: 'spinner' });
    const current = new Date();
    const date = new Date(year, month, day, current.getHours(), current.getMinutes() + 2);

    addNotification(date, typeEvent + name, formatShowDate(date, 'DD-MM-YYYY'));

    if (action !== DatePickerAndroid.dismissedAction) {
      this.setState({
        datePicker: new Date(year, month, day),
        reminder_date: formatShowDate(date, 'DD-MM-YYYY HH:MM')
      });
    }
  };

  renderDeleteButton = () => {
    const { user } = this.props.data;
    if (!user.events) {
      return null;
    }
    const { eventId } = this.props.navigation.state.params;

    const event = _.find(user.events, element => element.id === eventId);

    if (!event) {
      return null;
    }

    if (!event.description) {
      return (
        <Button block transparent style={{ backgroundColor: '#e7f1ff', marginTop: 10 }} onPress={this.onPressDel}>
          <Text style={mainStyles.txtAddChildBtn}>
            {event.type_event == 1 ? 'Удалить прививку' : 'Удалить прием врача'}
          </Text>
        </Button>
      );
    }
    return (
      <Tip
        style={{ paddingTop: 10 }}
        textTip="Назначить прививки можно самостоятельно в разделе &quot;Календарь&quot;"
      />
    );
  };

  render() {
    const { user } = this.props.data;
    if (!user.events) {
      return null;
    }

    const { eventId } = this.props.navigation.state.params;

    const event = _.find(user.events, element => element.id === eventId);

    if (!event) {
      return null;
    }

    return (
      <Container backgroundColor="#fff">
        <Content padder contentContainerStyle={{ flex: 1 }} scrollEnabled>
          <H2 style={styles.h2}>{formatShowDate(event.start_date)}</H2>

          <View style={[styles.tableContainer, { height: '10%', backgroundColor: '#e7f1ff', marginTop: 5 }]}>
            <Text style={mainStyles.txtAddChild}>Название</Text>
            <Text numberOfLines={1} style={mainStyles.txtAddEventName}>
              {event.name}
            </Text>
          </View>
          <View style={[styles.tableContainer, { height: '15%', backgroundColor: '#fbfbfb' }]}>
            <View>
              <Text style={mainStyles.txtAddChild}>Рекомендуемая дата</Text>
            </View>
            <View style={{ flexDirection: 'column', flex: 0.5, alignItems: 'center', justifyContent: 'center' }}>
              <Text style={mainStyles.txtAddEventName}>{formatShowDate(event.start_date)}</Text>
              <Text style={[mainStyles.txtAddEventSmall, { textAlign: 'center' }]}>
                Можно изменить в разделе "Календарь"
              </Text>
            </View>
          </View>
          <SwitchCustom item={this.state.remind24} onValueChange={item => this.changeNotificationHandler(24, item)} />
          <SwitchCustom item={this.state.remind3} onValueChange={item => this.changeNotificationHandler(3, item)} />
          <View style={styles.containerSwitch}>
            <Text style={mainStyles.txtAddEventName}>Напомнить о сдаче анализов</Text>
            <Switch
              onTintColor="#49d060"
              thumbTintColor="#fefefe"
              tintColor="#e7e7e7"
              value={this.state.isRemind}
              onValueChange={item => this.changeNotificationHandler(7, item)}
            />
          </View>
          <Text style={[mainStyles.txtAddEventSmall, { width: '80%' }]}>
            Напомнить о необходимости сдать анализы и записаться на прием педиатра
          </Text>
          <Text style={mainStyles.txtAddEventSmallUnder}>Дата напоминания: {this.state.reminder_date}</Text>
          {this.renderDeleteButton()}
          <Modal onRequestClose={this.closeModal} visible={this.state.isModal}>
            <View style={{ justifyContent: 'flex-end' }}>
              {this.state.renderIosPicker ? (
                <DatePickerIOS
                  timeZoneOffsetInMinutes={180}
                  mode="datetime"
                  date={this.state.datePicker}
                  onDateChange={this.onChangeDateIOS}
                />
              ) : null}
              <Button full style={{ backgroundColor: '#e3f6ef' }} onPress={this.closeModal}>
                <Text style={mainStyles.txtAddChildBtn}>Выбрать и закрыть</Text>
              </Button>
            </View>
          </Modal>
        </Content>
      </Container>
    );
  }
}

const styles = {
  tableContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  containerSwitch: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingBottom: 10,
    paddingTop: 10
  },
  h2: {
    color: '#4b4b4b',
    fontFamily: 'qunelas-semi-bold'
  }
};

export default compose(
  graphql(query),
  graphql(delEvent, { name: 'delEvent' })
)(EditEventScreen);
