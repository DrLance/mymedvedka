/* eslint-disable global-require */
import React, { Component } from 'react';
import { View, Switch, Text, AsyncStorage } from 'react-native';
import { Button, H2, Icon, Container, Content } from 'native-base';
import mainStyles from '../config/styles';

class SettingsScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerTintColor: '#525252',
    headerStyle: { backgroundColor: '#fff' },
    title: 'Вернуться',
    headerLeft: <Icon name="arrow-back" style={mainStyles.headerBack} onPress={() => navigation.navigate('calendar')} />
  });

  constructor(props) {
    super(props);
    this.state = { enableNotification: false, enableAutoFill: true };
  }

  async componentDidMount() {
    const enableNotif = await AsyncStorage.getItem('@Settings:enableNotification');
    const enableAutofill = await AsyncStorage.getItem('@Settings:enableAutoFill');

    await this.setState({ enableAutoFill: enableAutofill === '1', enableNotification: enableNotif === '1' });
  }

  changeNotification = value => {
    this.setState({ enableNotification: value });
    AsyncStorage.setItem('@Settings:enableNotification', value ? '1' : '0');
  };

  changeAutoFill = value => {
    this.setState({ enableAutoFill: value });
    AsyncStorage.setItem('@Settings:enableAutoFill', value ? '1' : '0');
  };

  render() {
    return (
      <Container backgroundColor="#fff">
        <Content padder>
          <H2
            style={{
              color: '#4b4b4b',
              marginBottom: 20,
              fontFamily: 'qunelas-semi-bold'
            }}
          >
            Настройки
          </H2>
          <View style={styles.containerSwitch}>
            <Text style={styles.txtStyle}>Разрешить уведомления</Text>
            <Switch
              onTintColor="#49d060"
              value={this.state.enableNotification}
              thumbTintColor="#fefefe"
              tintColor="#e7e7e7"
              onValueChange={value => this.changeNotification(value)}
            />
          </View>
          <View style={styles.containerSwitch}>
            <Text style={styles.txtStyle}>Автоматическое заполнения графика</Text>
            <Switch
              onTintColor="#49d060"
              value={this.state.enableAutoFill}
              thumbTintColor="#fefefe"
              tintColor="#e7e7e7"
              onValueChange={value => this.changeAutoFill(value)}
            />
          </View>
          <Button
            transparent
            block
            style={[styles.btnStyle]}
            onPress={() => {
              this.props.navigation.navigate('addchild');
            }}
          >
            <Text style={{ color: '#4b4b4b', fontFamily: 'qunelas-semi-bold' }}>Добавить профиль ребенка</Text>
          </Button>
          <Button
            transparent
            block
            style={[styles.btnStyle, { backgroundColor: '#e7f1ff' }]}
            onPress={() => {
              this.props.navigation.navigate('chageprofile');
            }}
          >
            <Text style={{ color: '#4b4b4b', fontFamily: 'qunelas-semi-bold' }}>Сменить профиль ребенка</Text>
          </Button>
          <Button
            transparent
            block
            style={[styles.btnStyle, { backgroundColor: '#e7e7e7' }]}
            onPress={() => {
              this.props.navigation.navigate('writedevelop');
            }}
          >
            <Text style={{ color: '#4b4b4b', fontFamily: 'qunelas-semi-bold' }}>Написать разработчикам</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    backgroundColor: '#fff'
  },
  btnStyle: {
    marginTop: 15,
    backgroundColor: '#e7ffee'
  },
  txtStyle: {
    color: '#4c5864',
    fontSize: 16,
    alignSelf: 'center',
    fontFamily: 'qunelas-semi-bold'
  },
  containerSwitch: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 20,
    paddingTop: 20,
    borderBottomWidth: 1,
    borderBottomColor: '#dbdbdb'
  }
};

export default SettingsScreen;
