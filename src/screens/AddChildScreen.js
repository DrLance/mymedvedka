import React, { Component } from 'react';
import { View, TextInput, Text, AsyncStorage } from 'react-native';
import { graphql, compose } from 'react-apollo';
import { Icon, H2, Button, Container, Content } from 'native-base';

import mainStyles from '../config/styles';
import Tip from '../components/Tip';
import { addKid } from '../mutations/Kid';
import { addEvent } from '../mutations/Event';
import query from '../queries/CurrentUser';
import { addYears, calculateEvents, calculateNotifications } from '../utils/util';

type Props = {
  addKid: Function,
  addEvent: Function,
  data: Object,
  navigation: Object
};

class AddChildScreen extends Component<Props> {
  static navigationOptions = ({ navigation, screenProps }) => ({
    headerTintColor: '#525252',
    headerStyle: {
      backgroundColor: '#fff'
    },
    title: 'Вернуться',
    headerLeft: <Icon name="arrow-back" style={mainStyles.headerBack} onPress={() => navigation.goBack()} />
  });
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      date_birth: '',
      height: '',
      weight: '',
      enableAutoFill: false
    };
  }

  async componentDidMount() {
    const enableFill = await AsyncStorage.getItem('@Settings:enableAutoFill');
    console.log('====================================');
    console.log(enableFill);
    console.log('====================================');
    if (enableFill !== null) {
      await this.setState({ enableAutoFill: enableFill === '1' });
    }
  }
  onPress = () => {
    this.props
      .addKid({
        variables: {
          user_id: this.props.data.user.id,
          name: this.state.name,
          weight: this.state.weight,
          growth: this.state.height,
          date_birth: this.state.date_birth
        },
        refetchQueries: [{ query }]
      })
      .then(({ data }) => {
        if (this.state.enableAutoFill) {
          if (data.addKid) {
            const { date_birth, id } = data.addKid;
            const events = calculateEvents(date_birth);
            const notifications = calculateNotifications(date_birth);

            events.push(...notifications);

            events.forEach(element => {
              this.props
                .addEvent({
                  variables: {
                    user_id: this.props.data.user.id,
                    kid_id: id,
                    start_date: element.start_event,
                    name: element.name,
                    type_event: element.type_event ? element.type_event : 1,
                    isActive: false,
                    description: element.type
                  }
                })
                .catch(res => {
                  //const errors = res.graphQLErrors.map(error => error.message);
                });
            });
          }
        }
        this.props.navigation.goBack();
      })
      .catch(res => {
        //const errors = res.graphQLErrors.map(error => error.message);
      });
  };
  render() {
    return (
      <Container backgroundColor="#fff">
        <Content padder>
          <H2 style={styles.h2}>Добавить ребенка</H2>
          <View style={[styles.backTwo, styles.containerSwitch]}>
            <Text style={[mainStyles.txtAddChild, { paddingLeft: 100 }]}>Имя</Text>
            <TextInput
              style={[
                mainStyles.txtAddChild,
                {
                  marginRight: 90,
                  width: 80,
                  borderBottomColor: '#4c5864',
                  borderBottomWidth: 1
                }
              ]}
              autoGrow
              underlineColorAndroid="transparent"
              onChangeText={text => this.setState({ name: text })}
              value={this.state.name}
            />
          </View>
          <View style={styles.containerSwitch}>
            <Text style={[mainStyles.txtAddChild, { paddingLeft: 100 }]}>Возраст</Text>
            <TextInput
              keyboardType="numeric"
              underlineColorAndroid="transparent"
              autoGrow
              style={[
                mainStyles.txtAddChild,
                {
                  marginRight: 100,
                  borderBottomColor: '#4c5864',
                  borderBottomWidth: 1,
                  width: 30,
                  textAlign: 'center'
                }
              ]}
              placeholder="мес"
              onChangeText={text => this.setState({ date_birth: addYears(text) })}
              value={this.state.age}
            />
          </View>
          <View style={[styles.backTwo, styles.containerSwitch]}>
            <Text style={[mainStyles.txtAddChild, { paddingLeft: 100 }]}>Рост</Text>
            <TextInput
              keyboardType="numeric"
              underlineColorAndroid="transparent"
              style={[
                mainStyles.txtAddChild,
                {
                  marginRight: 100,
                  borderBottomColor: '#4c5864',
                  borderBottomWidth: 1,
                  width: 30,
                  textAlign: 'center'
                }
              ]}
              onChangeText={text => this.setState({ height: text })}
              value={this.state.height}
              placeholder="см"
            />
          </View>
          <View style={styles.containerSwitch}>
            <Text style={[mainStyles.txtAddChild, { paddingLeft: 100 }]}>Вес</Text>
            <TextInput
              keyboardType="numeric"
              style={[
                mainStyles.txtAddChild,
                {
                  marginRight: 100,
                  borderBottomColor: '#4c5864',
                  borderBottomWidth: 1,
                  width: 30,
                  textAlign: 'center'
                }
              ]}
              underlineColorAndroid="transparent"
              onChangeText={text => this.setState({ weight: text })}
              value={this.state.weight}
              placeholder="грамм"
            />
          </View>
          <Tip textTip="Чтобы перейти в профиль другого ребенка выберите нужный пункт в &quot;Настройках&quot;" />
          <Button
            transparent
            block
            large
            onPress={() => this.onPress()}
            style={{ alignSelf: 'center', backgroundColor: '#e7f1ff', width: '100%', marginTop: 15 }}
          >
            <Text style={mainStyles.txtAddChildBtn}>Добавить ребенка</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}
const styles = {
  containerSwitch: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingBottom: 20,
    paddingTop: 20
  },
  inputStyle: { paddingRight: 20 },
  questionContainer: {
    flex: 0.2,
    backgroundColor: '#e7f1ff',
    alignItems: 'center',
    height: 100,
    justifyContent: 'center'
  },
  backTwo: { backgroundColor: '#e7f1ff' },
  h2: {
    color: '#4b4b4b',
    marginBottom: 20,
    fontFamily: 'qunelas-semi-bold'
  }
};

export default compose(graphql(query), graphql(addKid, { name: 'addKid' }), graphql(addEvent, { name: 'addEvent' }))(
  AddChildScreen
);
