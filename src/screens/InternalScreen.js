import React, { Component } from 'react';
import { View, Image, Text } from 'react-native';

import SwitchCustom from '../components/Switch';
import Tip from '../components/Tip';

const data24 = { name: 'Напомнить за 24 часа', age: '', ageMod: '', isEnable: true };
const data3 = { name: 'Напомнить за 3 дня', age: '', ageMod: '', isEnable: false };
class InternalScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={{ margin: 10 }}>
          <Text h4>13.12.2018</Text>
          <Text h3>Привика от гриппа сделана</Text>
          <View style={[styles.tableContainer, { height: 80, backgroundColor: '#e7f1ff', marginTop: 5 }]}>
            <Text style={styles.txtStyleL}>Прививка</Text>
            <Text style={styles.txtStyleR}>Гипатит Б</Text>
          </View>
          <View style={[styles.tableContainer, { height: 80, backgroundColor: '#fbfbfb' }]}>
            <Text style={styles.txtStyleL}>Рекомендуемая дата</Text>
            <Text style={styles.txtStyleR}>13.04.2017</Text>
          </View>
          <View style={[styles.tableContainer, { height: 80, backgroundColor: '#e7f1ff', marginBottom: 20 }]}>
            <Text style={styles.txtStyleL}>Прививка</Text>
            <Text style={styles.txtStyleR}>Гипатит Б</Text>
          </View>
          <Text style={styles.txtStyle}>Реакция:</Text>
          <Text style={styles.txtSmalStyle}>
            Повышена температураПовышена температураПовышена температураПовышена температураПовышена температураПовышена
            температура:
          </Text>
          <Text style={styles.txtStyle}>Реакция:</Text>
          <Text style={styles.txtSmalStyle}>
            Повышена температураПовышена температураПовышена температураПовышена температураПовышена температураПовышена
            температура:
          </Text>
          <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', marginTop: 10 }}>
            <View style={styles.blockImage}>
              <Image source={require('../../assets/images/photo-camera-list.png')} />
            </View>
            <View style={styles.blockImage}>
              <Image source={require('../../assets/images/photo-camera-list.png')} />
            </View>
            <View style={styles.blockImage}>
              <Image source={require('../../assets/images/photo-camera-list.png')} />
            </View>
            <View style={styles.blockImage}>
              <Image source={require('../../assets/images/photo-camera-list.png')} />
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    backgroundColor: '#fff'
  },
  tableContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexWrap: 'wrap'
  },
  txtStyle: {
    color: '#4c5864',
    fontSize: 16
  },
  txtStyleL: {
    color: '#4c5864',
    fontSize: 16,
    marginLeft: 40
  },
  txtStyleR: {
    color: '#4c5864',
    fontSize: 16,
    marginRight: 40
  },
  txtSmalStyle: {
    color: '#919191',
    fontSize: 14
  },
  blockImage: {
    height: 100,
    width: 100,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#e7f1ff',
    borderWidth: 1,
    margin: 5
  }
};

export default InternalScreen;
