import React, { Component } from 'react';

import { Icon, Container } from 'native-base';

import TableBlood from '../components/Tables/TableBlood';
import TablePee from '../components/Tables/TablePee';
import TableMonth from '../components/Tables/TableMonth';
import TableSleep from '../components/Tables/TableSleep';
import TableTooth from '../components/Tables/TableTooth';
import TableSize from '../components/Tables/TableSize';
import TableShoes from '../components/Tables/TableShoes';
import TableWeight from '../components/Tables/TableWeight';
import TableVisits from '../components/Tables/TableVisits';
import mainStyles from '../config/styles';

type Props = {
  navigation: any
};

class TableScreen extends Component<Props> {
  static navigationOptions = ({ navigation }) => ({
    headerTintColor: '#525252',
    headerStyle: { backgroundColor: '#fff' },
    title: 'Вернуться',
    headerLeft: <Icon name="arrow-back" style={mainStyles.headerBack} onPress={() => navigation.navigate('normal')} />
  });

  renderTable() {
    const { state } = this.props.navigation;

    if (state.params) {
      switch (state.params.pathName) {
        case 'pee':
          return <TablePee kidInfo={state.params.kidInfo} />;
        case 'blood':
          return <TableBlood kidInfo={state.params.kidInfo} />;
        case 'month':
          return <TableMonth />;
        case 'sleep':
          return <TableSleep />;
        case 'tooth':
          return <TableTooth />;
        case 'size':
          return <TableSize />;
        case 'shoes':
          return <TableShoes />;
        case 'weight':
          return <TableWeight />;
        case 'visits':
          return <TableVisits />;
        default:
          break;
      }
    }
    return <TableMonth />;
  }

  render() {
    return <Container style={{ backgroundColor: '#fff' }}>{this.renderTable()}</Container>;
  }
}

export default TableScreen;
