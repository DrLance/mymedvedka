/* eslint-disable global-require */
import React, { PureComponent } from 'react';
import { Text, Image, ImageBackground, AsyncStorage } from 'react-native';
import { Button } from 'native-base';

type Props = {
  navigation: any
};

class WelcomeScreen extends PureComponent<Props> {
  async componentDidMount() {
    const password = await AsyncStorage.getItem('@Auth:password');
    const acceptTerms = await AsyncStorage.getItem('@Auth:acceptTerms');

    if (password !== null && acceptTerms !== null) {
      this.props.navigation.navigate('auth');
    }
  }

  render() {
    return (
      <ImageBackground
        style={styles.imgContainer}
        source={require('../../assets/images/background.png')}
        resizeMode="stretch"
      >
        <Image
          style={{ height: 224, width: 180, marginBottom: 130 }}
          source={require('../../assets/images/romantic-teddy-bear.png')}
        />
        <Button
          full
          style={styles.btnEnter}
          onPress={() => {
            this.props.navigation.navigate('auth');
          }}
        >
          <Text style={styles.text}>Войти</Text>
        </Button>
        <Button
          onPress={() => {
            this.props.navigation.navigate('login');
          }}
          full
          style={styles.btn}
        >
          <Text style={styles.text}>Регистрация</Text>
        </Button>
        <Text
          onPress={() => {
            this.props.navigation.navigate('main');
          }}
          style={[styles.text, { marginTop: 10 }]}
        >
          Войти без регистрации
        </Text>
      </ImageBackground>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    justifyContent: 'center'
  },
  text: {
    color: '#fff',
    backgroundColor: 'transparent',
    fontFamily: 'qunelas-bold'
  },
  imgContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  imgIcon: { resizeMode: 'center' },
  btn: {
    backgroundColor: '#5c9fbc',
    width: '80%',
    alignSelf: 'center',
    margin: 5
  },
  btnEnter: {
    backgroundColor: '#7bbf76',
    width: '80%',
    alignSelf: 'center'
  }
};

export default WelcomeScreen;
