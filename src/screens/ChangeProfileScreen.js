import React, { Component } from 'react';
import { View, Image, Switch, FlatList, Text } from 'react-native';
import { Icon, H2, Button, Container, Content } from 'native-base';
import { graphql, compose } from 'react-apollo';
import moment from 'moment';

import mainStyles from '../config/styles';

import { changeActiveKid, removeKid } from '../mutations/Kid';
import query from '../queries/CurrentUser';

type Props = {
  сhangeActiveKid: Function,
  removeKid: Function,
  data: Object,
  navigation: Object
};

class ChangeProfileScreen extends Component<Props> {
  static navigationOptions = ({ navigation }) => ({
    headerTintColor: '#525252',
    headerStyle: { backgroundColor: '#fff' },
    title: 'Вернуться',
    headerLeft: <Icon name="arrow-back" style={mainStyles.headerBack} onPress={() => navigation.goBack(null)} />
  });
  onChange(item, value) {
    this.props
      .сhangeActiveKid({
        variables: {
          id: item.id,
          isActive: value
        },
        refetchQueries: [{ query }]
      })
      .catch(res => console.log(res));
  }

  onDelete() {
    const activeKid = this.props.data.user.kids.filter(i => i.isActive);
    if (activeKid) {
      this.props
        .removeKid({
          variables: { id: activeKid[0].id },
          refetchQueries: [{ query }]
        })
        .catch(res => console.log(res));
    }
  }

  formatDate = date => {
    const dd = new Date(date);
    let resultDate = '';
    const years = moment().diff(dd, 'years');
    const days = moment().diff(dd, 'days');
    const months = moment().diff(dd, 'months');

    if (days < 365) {
      resultDate = `${months} мес`;
    } else {
      resultDate = `${years} лет`;
    }
    return resultDate;
  };

  render() {
    return (
      <Container>
        <Content padder style={{ backgroundColor: '#fff' }}>
          <H2 style={{ color: '#4b4b4b', fontFamily: 'qunelas-semi-bold' }}>Сменить профиль ребенка</H2>
          <FlatList
            keyExtractor={item => item.id}
            data={this.props.data.user.kids}
            showsVerticalScrollIndicator={false}
            renderItem={({ item }) => (
              <View style={styles.containerSwitch}>
                <Text style={styles.txtStyle}>
                  {item.name}, {this.formatDate(item.date_birth)}
                </Text>
                <Switch
                  onValueChange={value => this.onChange(item, value)}
                  onTintColor="#49d060"
                  thumbTintColor="#fefefe"
                  tintColor="#e7e7e7"
                  value={item.isActive}
                />
              </View>
            )}
          />
          <Button
            transparent
            large
            block
            style={{ backgroundColor: '#e7ffee', marginTop: 15 }}
            onPress={() => {
              this.props.navigation.navigate('addchild');
            }}
          >
            <Text style={mainStyles.txtAddChildBtn}>Добавить профиль ребенка</Text>
          </Button>
          <Button
            onPress={() => this.onDelete()}
            transparent
            large
            block
            style={{ backgroundColor: '#e7f1ff', marginTop: 15 }}
          >
            <Text style={mainStyles.txtAddChildBtn}>Удалить</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}
const styles = {
  btnStyle: {
    marginTop: 15
  },
  txtStyle: {
    color: '#4c5864',
    fontSize: 16
  },
  containerSwitch: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingBottom: 20,
    paddingTop: 20,
    borderBottomWidth: 1,
    borderBottomColor: '#dbdbdb'
  },
  inputStyle: {
    paddingRight: 20
  },
  questionContainer: {
    flex: 0.2,
    backgroundColor: '#e7f1ff',
    alignItems: 'center',
    height: 100,
    justifyContent: 'center'
  },
  backTwo: {
    backgroundColor: '#e7f1ff'
  }
};

export default compose(
  graphql(query),
  graphql(removeKid, { name: 'removeKid' }),
  graphql(changeActiveKid, { name: 'сhangeActiveKid' })
)(ChangeProfileScreen);
