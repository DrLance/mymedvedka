/* eslint-disable global-require */
import React, { Component } from 'react';
import { Image, Text, DatePickerAndroid, Platform, Modal, DatePickerIOS, View } from 'react-native';
import { Icon, Button, Container, Content, Grid, Row } from 'native-base';
import { graphql } from 'react-apollo';

import Tip from '../components/Tip';
import mainStyles from '../config/styles';
import { formatShowDate, formatDateForMongo } from '../utils/util';
import { updateEvent } from '../mutations/Event';
import query from '../queries/CurrentUser';

type Props = {
  navigation: any,
  mutate: Function
};

class UpdateEventScreen extends Component<Props> {
  static navigationOptions = ({ navigation }) => ({
    headerTintColor: '#525252',
    headerStyle: { backgroundColor: '#fff' },
    title: 'Вернуться',
    headerLeft: (
      <Icon name="arrow-back" style={mainStyles.headerBack} onPress={() => navigation.navigate('calendar')} />
    ),
    tabBarVisible: false
  });

  state = {
    renderIOSDatePicker: false,
    dateEvent: null,
    modalVisible: true,
    iosDate: new Date()
  };

  onPressSave = () => {
    const { event } = this.props.navigation.state.params;
    this.props
      .mutate({
        variables: {
          event_id: event.id,
          start_date: formatDateForMongo(this.state.dateEvent, 'DD-MM-YYYY')
        },
        refetchQueries: [{ query }]
      })
      .then(({ data }) => {
        if (data) {
          this.props.navigation.goBack(null);
        }
      });
  };

  closeModal() {
    this.setState({ modalVisible: false });
  }

  checkDataPicker = () => {
    if (Platform.OS !== 'ios') {
      return this.renderDataPicker();
    }
    return this.setState({ renderIOSDatePicker: true, modalVisible: true });
  };

  formatIosDate(date) {
    this.setState({ dateEvent: formatShowDate(date, 'DD-MM-YYYY'), iosDate: date });
  }

  renderIOSDate = () => {
    return (
      <Modal visible={this.state.modalVisible} animationType="slide" onRequestClose={() => this.closeModal()}>
        <Content contentContainerStyle={{ justifyContent: 'center', flex: 1 }}>
          <DatePickerIOS mode="date" date={this.state.iosDate} onDateChange={date => this.formatIosDate(date)} />
          <Button block onPress={() => this.closeModal()}>
            <Text style={mainStyles.txtAddChild}>Выбрать и закрыть</Text>
          </Button>
        </Content>
      </Modal>
    );
  };

  renderDataPicker = async () => {
    const { action, year, month, day } = await DatePickerAndroid.open({
      date: new Date(this.props.navigation.state.params.event.start_date)
    });

    if (action !== DatePickerAndroid.dismissedAction) {
      const date = formatShowDate(new Date(year, month, day), 'DD-MM-YYYY');
      this.setState({ dateEvent: date });
    }
  };

  render() {
    const { event } = this.props.navigation.state.params;

    return (
      <Container backgroundColor="#fff">
        <Content padder contentContainerStyle={styles.container}>
          <Grid style={{ flex: 0.3 }}>
            <Row backgroundColor="#e7f1ff" style={styles.row}>
              <Text style={mainStyles.txtAddChild}>Название</Text>
              <Text style={mainStyles.txtAddChild}>{event.name}</Text>
            </Row>
            <Row backgroundColor="#fbfbfb" style={styles.row}>
              <Text style={mainStyles.txtAddChild}>Рекомендуемая дата</Text>
              <Text onPress={this.checkDataPicker} style={[mainStyles.txtAddChild, { textAlign: 'center', flex: 0.6 }]}>
                {this.state.dateEvent === null ? formatShowDate(event.start_date) : this.state.dateEvent}
              </Text>
            </Row>
          </Grid>
          {this.state.renderIOSDatePicker ? this.renderIOSDate() : null}
          <Tip style={{ paddingTop: 10 }} textTip="Здесь можно изменить дату прививки нажав на дату." />
          <Button
            block
            transparent
            large
            backgroundColor="#e7f1ff"
            style={{ marginTop: 10 }}
            onPress={this.onPressSave}
          >
            <Text style={mainStyles.txtAddChildBtn}>Сохранить</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    backgroundColor: '#fff'
  },
  row: {
    justifyContent: 'space-around',
    alignItems: 'center'
  }
};

export default graphql(query)(graphql(updateEvent)(UpdateEventScreen));
