import { Platform } from 'react-native';

const CalendarTabCfg = {
  initialRouteName: 'calendar',
  swipeEnabled: false,
  animationEnabled: true,
  tabBarOptions: {
    upperCaseLabel: false,
    indicatorStyle: { backgroundColor: 'transparent' },
    activeTintColor: '#4dbdfd',
    inactiveTintColor: '#9f9f9f',
    activeBackgroundColor: '#fbfbfb',
    inactiveBackgroundColor: '#fbfbfb',
    showIcon: true,
    labelStyle: {
      fontSize: 9,
      fontFamily: 'qunelas-medium',
      paddingBottom: 10
    },
    style: {
      backgroundColor: '#fbfbfb',
      borderWidth: 1,
      borderColor: '#dbdbdb',

      height: 62
    },
    iconStyle: {
      height: 23.5,
      width: 22.5,
      marginTop: -30
    }
  }
};

const MainTabCfg = {
  swipeEnabled: false,
  animationEnabled: true,
  navigationOptions: { tabBarVisible: false }
};

export { CalendarTabCfg, MainTabCfg };
