import { Dimensions, PixelRatio } from 'react-native';

const HEIGHT = Dimensions.get('window').height;
const WIDTH = Dimensions.get('window').width;
//const ELYPS = 84; //HEIGHT * 0.146;

const FONT_SCALE = PixelRatio.getFontScale();
const PIXEL_RATIO = PixelRatio.get();

const ELYPS = 0.219 * WIDTH;

const mainStyles = {
  headerBack: {
    marginLeft: 20,
    color: '#525252',
    fontSize: 35 * FONT_SCALE
  },
  txtModalContent: {
    marginLeft: 20,
    color: '#525252',
    fontSize: 25 * FONT_SCALE,
    fontFamily: 'qunelas-semi-bold'
  },
  txtToday: {
    color: '#648cdf',
    fontSize: 28 * FONT_SCALE,
    fontFamily: 'qunelas-bold'
  },
  txtTodayDay: {
    color: '#648cdf',
    fontSize: 42 * FONT_SCALE,
    fontFamily: 'qunelas-bold'
  },
  txtBtnNotify: {
    fontFamily: 'qunelas-semi-bold',
    color: '#fff',
    fontSize: 10 * FONT_SCALE
  },
  txtHeaderToday: {
    color: '#fff',
    fontSize: 18.75 * FONT_SCALE,
    fontFamily: 'qunelas-bold'
  },
  txtHeader: {
    color: 'white',
    fontSize: 14,
    fontFamily: 'qunelas-bold',
    marginLeft: 20,
    backgroundColor: 'transparent'
  },
  txtNormContent: {
    color: 'white',
    fontSize: HEIGHT * 0.032,
    fontFamily: 'qunelas-semi-bold',
    backgroundColor: 'transparent'
  },
  txtNormContentValue: {
    color: '#fff',
    fontSize: HEIGHT * 0.032,
    fontFamily: 'qunelas-semi-bold'
  },
  txtEventHeader: {
    color: '#969697',
    fontFamily: 'qunelas-medium',
    fontSize: HEIGHT * 0.0166
  },
  txtEventDescription: {
    color: '#4c5864',
    fontFamily: 'qunelas-semi-bold',
    fontSize: 12
  },
  txtFolderHeader: {
    color: '#4c5864',
    fontFamily: 'qunelas-semi-bold',
    fontSize: 12.5
  },
  calendar: {
    marginTop: -30,
    height: HEIGHT / 2,
    width: WIDTH / 1.5
  },
  imageHeaderMedical: { height: 50, justifyContent: 'center', alignItems: 'flex-start' },
  elipseStyle: {
    width: ELYPS,
    height: ELYPS,
    borderRadius: ELYPS / 2,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 10,
    shadowColor: '#e3f6ef',
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.8,
    shadowRadius: 20,
    marginTop: 5
  },
  txtMedicalMain: {
    fontFamily: 'qunelas-semi-bold',
    color: '#4b4b4b',
    fontSize: HEIGHT * 0.031,
    marginTop: 10,
    textAlign: 'center'
  },
  txtMedicaNotify: {
    color: '#4b4b4b',
    fontSize: 14.5,
    marginHorizontal: 10,
    textAlign: 'left',
    fontFamily: 'qunelas-semi-bold',
    flexWrap: 'wrap',
    flex: 1
  },
  txtBtn: {
    fontFamily: 'qunelas-medium',
    fontSize: 11,
    color: '#fff'
  },
  txtChart: {
    fontFamily: 'qunelas-semi-bold',
    color: '#3d3d3d',
    paddingLeft: 30
  },
  txtAddChild: {
    fontFamily: 'qunelas-medium',
    color: '#4c5864',
    fontSize: 33.33 / 2 //HEIGHT * 0.024
  },
  txtAddChildBtn: {
    fontFamily: 'qunelas-semi-bold',
    fontSize: HEIGHT * 0.021,
    color: '#4b4b4b'
  },
  txtTip: {
    color: '#878787',
    fontSize: HEIGHT * 0.018,
    fontFamily: 'qunelas-medium'
  },
  txtTableItemHead: {
    color: '#4c5864',
    fontFamily: 'qunelas-thin',
    fontSize: HEIGHT / 80
  },
  txtTableItemDescr: {
    color: '#4c5864',
    fontFamily: 'qunelas-medium',
    fontSize: HEIGHT / 80
  },
  txtAddEventSmall: {
    color: '#919191',
    fontFamily: 'qunelas-medium',
    fontSize: 12 //HEIGHT / 53.36
  },
  txtAddEventName: {
    fontFamily: 'qunelas-semi-bold',
    fontSize: 33.33 / 2, //HEIGHT * 0.024,
    color: '#4c5864'
  },
  txtAddEventSmallUnder: {
    color: '#4c5864',
    fontFamily: 'qunelas-semi-bold',
    fontSize: HEIGHT / 53.36,
    textDecorationLine: 'underline'
  }
};

export default mainStyles;
