import { Dimensions, PixelRatio } from 'react-native';
import { LocaleConfig } from 'react-native-calendars';

const HEIGHT = Dimensions.get('window').height;
const WIDTH = Dimensions.get('window').width;

const PIXEL_RATIO = PixelRatio.get();

/*const FILLER_HEIGHT = 37.25 / 2;
const FILLER_WIDTH = 37.37 / 2;*/
const FILLER_HEIGHT = 43.25 / 2;
const FILLER_WIDTH = 43.37 / 2;
/*
const FILLER_HEIGHT = 17.85;
const FILLER_WIDTH = 17.37;
*/
LocaleConfig.locales.ru = {
  monthNames: [
    'январь',
    'февраль',
    'март',
    'апрель',
    'май',
    'июнь',
    'июль',
    'август',
    'сентябрь',
    'октябрь',
    'ноябрь',
    'декабрь'
  ],
  monthNamesShort: ['янв', 'фев', 'мар', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'нов', 'дек'],
  dayNames: ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'],
  dayNamesShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб']
};

LocaleConfig.defaultLocale = 'ru';

const calendarTheme = () => ({
  backgroundColor: 'transparent',
  calendarBackground: 'transparent',
  dayTextColor: '#fff',
  textDayFontFamily: 'qunelas-medium',
  textMonthFontFamily: 'qunelas',
  textDayHeaderFontFamily: 'qunelas',
  textSectionTitleColor: '#fff',
  selectedDayTextColor: '#648cdf',
  todayTextColor: '#fff',
  selectedDayBackgroundColor: '#fff',
  monthTextColor: '#fff',
  arrowColor: '#fff',
  'stylesheet.calendar.header': {
    header: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center'
    },
    week: {
      flexDirection: 'row',
      justifyContent: 'space-around'
    },
    dayHeader: {
      textAlign: 'center',
      color: '#fff',
      fontFamily: 'qunelas-bold',
      marginTop: 0,
      fontSize: 32.35 / 2 //HEIGHT * 0.022
    }
  },
  'stylesheet.calendar.main': {
    week: {
      marginTop: 2,
      marginBottom: 2,
      flexDirection: 'row',
      justifyContent: 'space-around'
    }
  },
  'stylesheet.day.period': {
    base: {
      marginTop: 2,
      width: FILLER_WIDTH,
      height: FILLER_HEIGHT
    },
    fillers: {
      height: FILLER_HEIGHT,
      flexDirection: 'row',
      flex: 1
    },
    leftFiller: {
      height: FILLER_HEIGHT,
      flex: 1
    },
    rightFiller: {
      height: FILLER_HEIGHT,
      flex: 1
    },
    text: {
      fontSize: 16, //HEIGHT * 0.022,
      fontFamily: 'qunelas-medium',
      color: '#fff',
      backgroundColor: 'rgba(255, 255, 255, 0)',
      textAlign: 'center'
    }
  }
});

export { LocaleConfig, calendarTheme };
