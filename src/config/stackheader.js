import React from 'react';
import { Image } from 'react-native';
import { Icon } from 'native-base';

const StackHeader = navigation => ({
  headerTintColor: '#525252',
  headerStyle: {
    backgroundColor: '#fff'
  },
  title: 'Вернуться',
  headerLeft: (
    <Icon
      name="arrow-left"
      type="feather"
      size={32}
      iconStyle={{
        marginLeft: 15
      }}
      color="#525252"
      onPress={this.props.onPress}
    />
  ),
  tabBarIcon: ({ tintColor }) => (
    <Image
      source={require('../../assets/school-calendar.png')}
      style={[
        {
          tintColor: tintColor
        }
      ]}
    />
  )
});

export default StackHeader;
