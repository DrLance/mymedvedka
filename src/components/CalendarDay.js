import React, { Component } from 'react';
import { View, Text } from 'react-native';

class CalendarDay extends Component {
  state = {};

  colorDay = state => {
    switch (state) {
      case 'today':
        return '#648cdf';
        break;
      case 'disabled':
        return '#d0d0d0';
      case 'selected':
        return 'red';
      default:
        return '#fff';
    }
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <Text
          style={{
            textAlign: 'center',
            color: this.colorDay(this.props.state),
            fontFamily: 'qunelas-medium',
            fontSize: 16
          }}>
          {this.props.date.day}
        </Text>
      </View>
    );
  }
}

export default CalendarDay;
