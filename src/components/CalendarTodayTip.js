import React, { PureComponent } from 'react';
import { View, Text, Platform } from 'react-native';
import { LocaleConfig } from '../config/calendar';

import mainStyles from '../config/styles';

const monthName = LocaleConfig.locales.ru.monthNamesShort[new Date().getMonth()];

type Props = {
  textTip: String
};

class CalendarTodayTip extends PureComponent<Props> {
  state = {};
  render() {
    return (
      <View style={styles.container}>
        <Text style={mainStyles.txtHeaderToday}>СЕГОДНЯ</Text>
        <View style={mainStyles.elipseStyle}>
          <Text style={mainStyles.txtTodayDay}>{new Date().getDate()}</Text>
          <Text style={[mainStyles.txtToday, { marginTop: -15 }]}>{monthName}</Text>
        </View>
        <Text style={styles.txtTip}>{this.props.textTip}</Text>
      </View>
    );
  }
}
const styles = {
  container: {
    flexDirection: 'column',
    width: '35%',
    alignItems: 'center',
    marginTop: 5
  },
  txtTip: {
    color: '#fff',
    fontSize: 12,
    fontFamily: 'qunelas-medium',
    textAlign: 'center',
    marginTop: 10
  }
};
export default CalendarTodayTip;
