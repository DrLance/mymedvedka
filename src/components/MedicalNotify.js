import React, { PureComponent } from 'react';
import { Image, Text, View } from 'react-native';
import { Button } from 'native-base';
import mainStyles from '../config/styles';

class MedicalNotify extends PureComponent {
  state = {};
  render() {
    return (
      <View style={styles.container}>
        <Image
          source={require('../../assets/images/medical-kit.png')}
          style={{ tintColor: '#4dbdfd', marginLeft: 20, height: 21, width: 26 }}
        />
        <Text style={mainStyles.txtMedicaNotify}>График работы прививочного кабинета</Text>
        <Button rounded style={styles.btn} small onPress={this.props.onPress}>
          <Text style={[mainStyles.txtBtn, { marginTop: -2 }]}>Перейти</Text>
        </Button>
      </View>
    );
  }
}

const styles = {
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    borderWidth: 2,
    borderColor: '#e7f1ff',
    borderRadius: 4,
    width: '100%',
    paddingVertical: 20
  },
  btnContainer: {
    borderRadius: 21,
    width: null,
    elevation: 20
  },
  btn: {
    backgroundColor: '#6184d7',
    alignSelf: 'center',
    justifyContent: 'center',
    right: 10,
    elevation: 5,
    width: 90.5,
    height: 21,
    shadowColor: 'rgba(97, 132, 215,1)',
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.5,
    shadowRadius: 3
  }
};

export default MedicalNotify;
