import React, { PureComponent } from 'react';
import { View, Image, Text, Dimensions } from 'react-native';
import mainStyles from '../config/styles';

const HEIGHT = Dimensions.get('window').height;

class Tip extends PureComponent {
  static defaultProps = { textTip: 'Подсказка' };
  render() {
    return (
      <View style={[{ flexDirection: 'row' }, this.props.style]}>
        <View style={styles.questionContainer}>
          <Image
            style={{ height: 30, width: 30 }}
            source={require('../../assets/images/question.png')}
            resizeMode="contain"
          />
        </View>
        <View style={styles.txtContainer}>
          <Text style={mainStyles.txtAddChild}>Совет:</Text>
          <Text style={mainStyles.txtTip}>{this.props.textTip}</Text>
        </View>
      </View>
    );
  }
}

const styles = {
  questionContainer: {
    flex: 0.15,
    backgroundColor: '#e7f1ff',
    alignItems: 'center',
    height: HEIGHT * 0.128,
    justifyContent: 'center'
  },
  txtContainer: {
    flexDirection: 'column',
    flex: 0.85,
    paddingLeft: 10,
    alignItems: 'flex-start',
    justifyContent: 'center',
    backgroundColor: '#fbfbfb'
  }
};

export default Tip;
