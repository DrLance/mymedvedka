import React, { Component } from 'react';
import { View } from 'react-native';

class WrappedMainContainer extends Component {
  componentDidCatch(error) {
    console.log('!!!!!');
  }
  render() {
    return <View style={{ flex: 1 }}>{this.props.children}</View>;
  }
}

export default WrappedMainContainer;
