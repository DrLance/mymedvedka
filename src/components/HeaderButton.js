import React, { PureComponent } from 'react';
import { Image, TouchableOpacity } from 'react-native';

type Props = {
  style: Object,
  onPress: Function,
  right: Boolean
};

class HeaderButton extends PureComponent<Props> {
  render() {
    return (
      <TouchableOpacity onPress={this.props.onPress} style={this.props.style}>
        <Image source={this.props.image} style={this.props.right ? styles.imgRight : styles.img} />
      </TouchableOpacity>
    );
  }
}

const styles = {
  img: {
    height: 24,
    width: 26,
    tintColor: '#fff'
  },
  imgRight: {
    height: 25,
    width: 24.5,
    tintColor: '#fff'
  }
};

export default HeaderButton;
