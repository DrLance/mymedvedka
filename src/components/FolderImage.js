import React, { PureComponent } from 'react';
import { TouchableOpacity, Image, Text } from 'react-native';
import mainStyles from '../config/styles';

/* eslint-disable global-require */

class FolderImage extends PureComponent {
  render() {
    return (
      <TouchableOpacity
        onLongPress={this.props.onLongPress}
        style={styles.itemPhoto}
        onLayout={this.props.onLayout}
        onPress={this.props.onPress}
      >
        <Image
          source={require('../../assets/images/folder-icon.png')}
          style={{
            width: 96,
            height: 75.5,
            marginBottom: 10
          }}
        />
        <Text style={mainStyles.txtFolderHeader}>{this.props.data.name}</Text>
        <Text style={mainStyles.txtEventHeader}>{this.props.data.count} объектов</Text>
      </TouchableOpacity>
    );
  }
}

const styles = {
  itemPhoto: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  }
};
export default FolderImage;
