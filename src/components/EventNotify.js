import React, { PureComponent } from 'react';
import { Text, View, Image, Dimensions, PixelRatio } from 'react-native';
import { Button } from 'native-base';
import mainStyles from '../config/styles';

const PIXEL_RATIO = PixelRatio.get();
const HEIGHT = Dimensions.get('window').height;
const WIDTH = Dimensions.get('window').width;

type Props = {
  shadow: Boolean,
  txtDescription: String,
  txtHeader: String,
  onPress: Function,
  image: Object,
  style: Object
};

class EventNotify extends PureComponent<Props> {
  limitText = text => {
    if (text.length <= 35) {
      return text;
    }

    return `${text.substring(0, 35)}...`;
  };
  renderShadow = () => {
    if (this.props.shadow) {
      return {
        shadowColor: '#0f55fc',
        shadowOffset: {
          width: 0,
          height: 2
        },
        shadowOpacity: 0.6,
        shadowRadius: 8,
        elevation: 5
      };
    }

    return null;
  };

  renderTxt() {
    if (this.props.txtDescription && this.props.txtHeader) {
      return (
        <View
          style={{
            flex: 1,
            flexWrap: 'wrap',
            alignContent: 'center',
            justifyContent: 'flex-start'
          }}
        >
          <Text style={mainStyles.txtEventHeader}>{this.props.txtHeader}</Text>
          <Text style={mainStyles.txtEventDescription}>{this.limitText(this.props.txtDescription)}</Text>
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <Text style={mainStyles.txtEventDescription}>{this.limitText(this.props.txtDescription)}</Text>
      </View>
    );
  }
  render() {
    return (
      <View style={[styles.notifyContainer, this.props.style, this.renderShadow()]}>
        <View style={styles.imgContainer}>
          <Image source={this.props.image} style={styles.imgSize} />
          {this.renderTxt()}
          <Button rounded style={styles.btn} small onPress={this.props.onPress}>
            <Text style={[mainStyles.txtBtnNotify, { marginTop: -1 }]}>Подробнее</Text>
          </Button>
        </View>
      </View>
    );
  }
}

const styles = {
  container: {
    alignItems: 'flex-start',
    justifyContent: 'space-around',
    flexWrap: 'wrap',
    flex: 1
  },
  notifyContainer: {
    width: WIDTH * 0.97,
    height: 62.5,
    borderRadius: 6,
    borderWidth: 1,
    borderColor: '#fff',
    zIndex: 10,
    backgroundColor: '#fff',
    justifyContent: 'space-around',
    alignSelf: 'center'
  },
  imgContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  imgSize: {
    width: 30,
    height: 30,
    marginHorizontal: 15
  },
  btn: {
    backgroundColor: '#6184d7',
    width: 90.5,
    height: 21,
    alignSelf: 'center',
    justifyContent: 'center',
    marginRight: 25,
    shadowColor: '#0f55fc',
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.5,
    shadowRadius: 3,
    elevation: 5
  }
};

export default EventNotify;
