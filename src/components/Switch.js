import React, { PureComponent } from 'react';
import { Text, View, Switch } from 'react-native';
import mainStyles from '../config/styles';

class SwitchCustom extends PureComponent {
  state = {};
  render() {
    return (
      <View style={styles.containerSwitch}>
        <Text style={mainStyles.txtAddEventName}>
          {this.props.item.name} {this.props.item.age} {this.props.item.ageMod}
        </Text>
        <Switch
          onTintColor="#49d060"
          onValueChange={this.props.onValueChange}
          thumbTintColor="#fefefe"
          tintColor="#e7e7e7"
          value={this.props.item.isEnable}
        />
      </View>
    );
  }
}

const styles = {
  txtStyle: {
    color: '#4c5864',
    fontSize: 16,
    fontFamily: 'qunelas-semi-bold'
  },
  containerSwitch: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingBottom: 20,
    paddingTop: 20,
    borderBottomWidth: 1,
    borderBottomColor: '#dbdbdb'
  }
};

export default SwitchCustom;
