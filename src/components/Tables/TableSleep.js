import React, { Component } from 'react';
import { View, ScrollView, Image, TouchableOpacity, Text, Dimensions, TextInput } from 'react-native';
import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component';
import { Icon } from 'native-base';
import mainStyles from '../../config/styles';

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

const tableTitle = [
  '1-2 мес',
  '3-4 мес',
  '5-6 мес',
  '7-9 мес',
  '10-12 мес',
  '1-1.5 года',
  '2-3 года',
  '3-7 лет',
  'после 7 лет'
];

const tableData2 = ['', '', '', '', '', '', '', '', ''];

const colorCell = ['#ffbaba', '#ffc3d4', '#ffefb7', '#ddffb9', '#faffb0', '#acddff'];

class TableSleep extends Component {
  state = {
    rowData: [
      ['спит', 'спит', 'спит', 'спит', 'спит', '2 раза', '1 раз', '1 раз', ''],
      ['спит', 'спит', 'спит', 'спит', 'спит', '4 ч', '2,5 - 3 ч', '2 ч', ''],
      ['спит', 'спит', 'спит', 'спит', 'спит', '10 - 11 ч', '10 - 11 ч', '10 ч', '8 - 9 ч'],
      ['18 ч', '17 - 18 ч', '16 ч', '15 ч', '13 ч', '', '', '', '']
    ]
  };
  row(col, row) {
    return this.state.rowData[col][row];
  }

  render() {
    return (
      <ScrollView style={{ backgroundColor: 'transparent' }}>
        <View style={{ flexDirection: 'row' }}>
          <View>
            <Table style={{ flexDirection: 'row', flex: 1 }} borderStyle={{ borderColor: '#332efa', borderWidth: 1 }}>
              <TableWrapper style={styles.tableWrapMain}>
                <Cell data={['Возраст']} style={styles.head} textStyle={styles.textMainHead} />
                {tableTitle.map((title, i) => (
                  <Cell key={i} data={title} style={styles.title} textStyle={styles.textHead} />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['Сколько раз в день']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    textStyle={styles.textRow}
                    data={this.row(0, i)}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#ffbaba'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['Днем продолжительность']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    data={this.row(1, i)}
                    textStyle={styles.textRow}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#ffc3d4'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['Ночью продолжительность сна']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    data={this.row(2, i)}
                    textStyle={styles.textRow}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#ffefb7'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['Всего']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    data={this.row(3, i)}
                    textStyle={styles.textRow}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#ddffb9'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
            </Table>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const mainH = 50;
const heightHead = (WIDTH - mainH) / 14;
const oneCol = HEIGHT / 5;
const mainCol = oneCol;
const tableWrapWidthMain = mainCol;
const tableWrapWidth = (HEIGHT - mainCol) / 4;

const styles = {
  head: {
    backgroundColor: '#3888ff',
    height: mainH
  },
  textHead: {
    textAlign: 'center',
    color: '#fff',
    fontFamily: 'qunelas-medium',
    fontSize: HEIGHT / 72
  },
  textMainHead: {
    textAlign: 'center',
    color: '#fff',
    fontFamily: 'qunelas-semi-bold',
    fontSize: HEIGHT / 55
  },
  textSecondHead: {
    textAlign: 'center',
    color: '#fff',
    fontFamily: 'qunelas-semi-bold',
    fontSize: HEIGHT / 71
  },
  textRow: {
    textAlign: 'center',
    color: '#000',
    fontFamily: 'qunelas-semi-bold',
    fontSize: HEIGHT / 71
  },
  title: {
    height: heightHead,
    backgroundColor: '#3888ff'
  },
  row: {
    height: heightHead
  },
  text: {
    textAlign: 'center',
    color: '#fff',
    fontFamily: 'qunelas-medium',
    alignSelf: 'center'
  },
  tableWrapMain: {
    width: tableWrapWidthMain
  },
  tableWrap: {
    width: tableWrapWidth
  }
};

export default TableSleep;
