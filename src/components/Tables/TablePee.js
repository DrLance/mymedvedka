import React, { Component } from 'react';
import { View, Modal, Text, Dimensions, TextInput } from 'react-native';
import { Table, TableWrapper, Cell } from 'react-native-table-component';
import { Icon, Container, Content, Button } from 'native-base';
import _ from 'lodash';

import mainStyles from '../../config/styles';
import dataPee from '../../data/dataPee';

const HEIGHT = Dimensions.get('window').height;

type Props = {
  kidInfo: Object
};

const tableTitle = [
  'Эритроциты BLD в/пз',
  'Лейкоциты LEO в/пз',
  'Билирубин BIL',
  'Уробилиноген URO мг/л',
  'Белок PRO г/л',
  'Кислотность  pH',
  'Плотность\n (Удельный вес) S.G',
  'Кетоновые тела KET',
  'Нитриты NIT',
  'Глюкоза GLU',
  'Цвет  COLOR',
  'Цилиндры в/пз',
  'Эпителий',
  'Слизь',
  'Бактерии и Грибки'
];

const tableData2 = ['', '', '', '', '', '', '', '', '', '', '', '', '', '', ''];

class TablePee extends Component<Props> {
  static navigationOptions = ({ navigation, screenProps }) => ({
    headerTintColor: '#525252',
    headerStyle: { backgroundColor: '#fff' },
    title: 'Вернуться',
    headerLeft: <Icon name="arrow-back" style={mainStyles.headerBack} onPress={() => navigation.navigate('normal')} />
  });

  state = {
    modalVisible: false,
    selectedIndex: null,
    rowData: [
      [
        'Отсутствуют',
        'Не более 3 в п/зр.',
        'Отсутствует',
        'Отсутствует',
        'Отсутствует или до 0.002 г/л',
        '4,5-7,7',
        '1,001-1,005',
        'Отсутствуют',
        'Отсутствуют',
        'Отсутствует',
        'Соломенно желтый',
        'Отсутствуют',
        'Отсутствует или небольшое кол-во',
        'Отсутствует или небольшое кол-во',
        'Отсутствуют'
      ],
      [
        '0-2 в поле зр.',
        '0-6 в поле зр.',
        'Отсутствует',
        '5-10 мг/л',
        'Отсутствует или до 0.036 г/л',
        '4,7-8,0',
        '1-2 года 1,004-1,006\n  2-5 лет 1,012-1,020\n 5-12 лет 1,012-1,025',
        'Отсутствуют',
        'Отсутствуют',
        'Отсутствует',
        'Соломенно желтый',
        'Отсутствуют',
        'Отсутствует или небольшое кол-во',
        'Отсутствует или небольшое кол-во',
        'Отсутствуют'
      ],
      [
        '1-3 в п/зр.',
        '3-6 в п/зр.',
        'Отсутствует',
        '5-10 мг/л',
        'Отсутствует или до 0.03 г/л',
        '5-6',
        '1,012-1,025',
        'Отсутствуют',
        'Отсутствуют',
        'Отсутствует',
        'Соломенно желтый',
        'Отсутствуют',
        'Отсутствует или небольшое кол-во',
        'Отсутствует или небольшое кол-во',
        'Отсутствуют'
      ]
    ],
    summ: ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '']
  };

  onChangeInput(val, i) {
    if (!this.props.kidInfo) {
      return;
    }

    const newSumm = this.state.summ;
    const { kidInfo } = this.props;
    let indexYear = 0;

    if (kidInfo.age > 12) {
      indexYear = 1;
    }
    if (kidInfo.age > 25) {
      indexYear = 2;
    }

    const tableValue = this.state.rowData[indexYear][i].split('-');
    let minValue = 0;
    let maxValue = 0;

    if (tableValue.length > 1) {
      minValue = parseFloat(tableValue[0].replace(',', '.'));
      maxValue = parseFloat(tableValue[1].replace(',', '.'));
    }

    if (val && (val < minValue || val > maxValue)) {
      newSumm[i] = '1';
    } else {
      newSumm[i] = '';
    }

    this.setState({ summ: newSumm });
  }

  onPressInfo = index => {
    this.setState({ modalVisible: true, selectedIndex: index });
  };

  closeModal = () => {
    this.setState({ modalVisible: false });
  };

  row(col, row) {
    return this.state.rowData[col][row];
  }

  renderInput(i) {
    return (
      <TextInput
        keyboardType="numeric"
        style={[
          styles.textRow,
          {
            borderBottomWidth: 0.5,
            borderBottomColor: 'white',
            width: '60%',
            alignSelf: 'center'
          }
        ]}
        underlineColorAndroid="transparent"
        onChangeText={val => {
          this.onChangeInput(val, i);
        }}
      />
    );
  }

  renderItogo = index => (
    <Button
      small
      backgroundColor={this.state.summ[index] ? 'red' : '#3888ff'}
      block
      style={{ alignSelf: 'center', width: '80%' }}
      onPress={() => this.onPressInfo(index)}
    >
      <Text style={styles.textRow}>Посмотреть</Text>
    </Button>
  );

  renderInfoText = () => {
    const txtObj = _.find(dataPee, el => {
      if (el.index === this.state.selectedIndex) {
        return el;
      }
      return null;
    });

    if (txtObj) {
      return (
        <View>
          <Text style={mainStyles.headerBack}>{txtObj.secondHeader}</Text>
          <Text style={mainStyles.txtEventDescription}>{txtObj.description}</Text>
        </View>
      );
    }
    return null;
  };

  render() {
    return (
      <Container>
        <Modal visible={this.state.modalVisible} animationType="slide" onRequestClose={() => this.closeModal()}>
          <Content padder contentContainerStyle={{ justifyContent: 'center', flex: 1, alignItems: 'center' }}>
            {this.renderInfoText()}
            <Button
              transparent
              block
              onPress={this.closeModal}
              style={{ alignSelf: 'center', marginTop: 20, width: '30%' }}
            >
              <Text style={mainStyles.txtAddChild}>Закрыть</Text>
            </Button>
          </Content>
        </Modal>
        <Content contentContainerStyle={{ flexDirection: 'row' }}>
          <View>
            <Table style={{ flexDirection: 'row', flex: 1 }} borderStyle={{ borderColor: '#332efa', borderWidth: 1 }}>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['Показатели']} style={styles.head} textStyle={styles.textMainHead} />
                {tableTitle.map((title, i) => (
                  <Cell key={i} data={title} style={styles.title} textStyle={styles.textHead} />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['До 1 года']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    textStyle={styles.textRow}
                    data={this.row(0, i)}
                    style={[styles.row, { backgroundColor: '#ffefb7' }]}
                  />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['После 1 года']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    data={this.row(1, i)}
                    textStyle={styles.textRow}
                    style={[styles.row, { backgroundColor: '#ffefb7' }]}
                  />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['Для взрослых']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    data={this.row(2, i)}
                    textStyle={styles.textRow}
                    style={[styles.row, { backgroundColor: '#ffefb7' }]}
                  />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['Ваши параметры']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    data={this.renderInput(i)}
                    textStyle={styles.textRow}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#acddff'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['Итого']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    data={this.renderItogo(i)}
                    textStyle={styles.textRow}
                    style={[styles.row, { backgroundColor: '#acddff' }]}
                  />
                ))}
              </TableWrapper>
            </Table>
          </View>
        </Content>
      </Container>
    );
  }
}

const heightHead = 60;
const styles = {
  head: {
    backgroundColor: '#3888ff',
    height: 50
  },
  textHead: {
    textAlign: 'center',
    color: '#fff',
    fontFamily: 'qunelas-medium',
    fontSize: HEIGHT / 72
  },
  textMainHead: {
    textAlign: 'center',
    color: '#fff',
    fontFamily: 'qunelas-semi-bold',
    fontSize: HEIGHT / 55
  },
  textSecondHead: {
    textAlign: 'center',
    color: '#fff',
    fontFamily: 'qunelas-semi-bold',
    fontSize: HEIGHT / 71
  },
  textRow: {
    textAlign: 'center',
    color: '#000',
    fontFamily: 'qunelas-semi-bold',
    fontSize: HEIGHT / 71
  },
  title: { height: heightHead, backgroundColor: '#3888ff' },
  row: { height: heightHead },
  text: { textAlign: 'center', color: '#fff', fontFamily: 'qunelas-medium', alignSelf: 'center' },
  tableWrap: { width: HEIGHT / 6 }
};

export default TablePee;
