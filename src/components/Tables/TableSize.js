import React, { Component } from 'react';
import { View, ScrollView, Image, TouchableOpacity, Text, Dimensions, TextInput } from 'react-native';
import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component';
import { Icon } from 'native-base';
import mainStyles from '../../config/styles';

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

const tableTitle = [
  '1 месяц',
  '2 месяца',
  '3 месяца',
  '3-6 месяцев',
  '6-9 месяцев',
  '12 месяцев',
  '1,5 года',
  '2 года',
  '3 года',
  '4 года',
  '5 лет',
  '6 лет',
  '7 лет'
];

const tableData2 = ['', '', '', '', '', '', '', '', '', '', '', '', ''];

const colorCell = ['#ffbaba', '#ffc3d4', '#ffefb7', '#ddffb9', '#faffb0', '#acddff'];

class TableSize extends Component {
  state = {
    rowData: [
      ['50', '56', '62', '68', '74', '80', '86', '92', '98', '104', '110', '116', '122'],
      ['18', '18', '20', '22', '24', '24', '24', '26', '26', '28', '28', '30', '30'],
      [
        '3-4 кг',
        '3-4 кг',
        '4-5 кг',
        '5-7 кг',
        '7-9 кг',
        '9-11 кг',
        '11-12 кг',
        '12-14,5 кг',
        '13,5-15 кг',
        '15-18 кг',
        '19-21 кг',
        '22-25 кг',
        '25-28 кг'
      ],
      [
        '41-43',
        '43-45',
        '45-47',
        '47-49',
        '49-51',
        '51-53',
        '52-54',
        '53-55',
        '54-56',
        '55-57',
        '56-58',
        '57-59',
        '58-62'
      ],
      [
        '41-43',
        '43-45',
        '45-47',
        '47-49',
        '49-51',
        '51-53',
        '52-54',
        '53-56',
        '55-58',
        '57-60',
        '59-62',
        '61-64',
        '63-67'
      ],
      ['14', '16', '19', '21', '23', '26', '28', '31', '33', '36', '38', '41', '43'],
      [
        '50/56',
        '50/56',
        '62/68',
        '62/68',
        '74/80',
        '74/80',
        '86/92',
        '86/92',
        '98/104',
        '98/104',
        '110/116',
        '110/116',
        '122/128'
      ],
      [
        '46-50',
        '51-56',
        '57-62',
        '63-68',
        '67-74',
        '75-80',
        '81-86',
        '87-92',
        '93-98',
        '99-104',
        '105-110',
        '111-116',
        '117-122'
      ]
    ],
    summ: ['', '', '', '', '', '', '', '', '', '', '', '']
  };
  row(col, row) {
    return this.state.rowData[col][row];
  }

  renderInput(i) {
    return (
      <TextInput
        style={styles.textRow}
        onChangeText={val => {
          this.onChangeInput(val, i);
        }}
      />
    );
  }

  onChangeInput(val, i) {
    let newSumm = this.state.summ;
    let newVal = parseInt(val, 10) * 1.2;
    newSumm[i] = isNaN(newVal) ? '' : newVal;

    this.setState({ summ: newSumm });
  }

  render() {
    return (
      <ScrollView style={{ backgroundColor: 'transparent' }}>
        <View style={{ flexDirection: 'row' }}>
          <View>
            <Table style={{ flexDirection: 'row', flex: 1 }} borderStyle={{ borderColor: '#332efa', borderWidth: 1 }}>
              <TableWrapper style={styles.tableWrapMain}>
                <Cell data={['Возраст']} style={styles.head} textStyle={styles.textMainHead} />
                {tableTitle.map((title, i) => (
                  <Cell key={i} data={title} style={styles.title} textStyle={styles.textHead} />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['Рост']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    textStyle={styles.textRow}
                    data={this.row(0, i)}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#ffbaba'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['Размер']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    data={this.row(1, i)}
                    textStyle={styles.textRow}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#ffc3d4'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['Вес']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    data={this.row(2, i)}
                    textStyle={styles.textRow}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#ffefb7'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['Объем груди, см']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    data={this.row(3, i)}
                    textStyle={styles.textRow}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#ddffb9'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['Объем бедер, см']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    data={this.row(4, i)}
                    textStyle={styles.textRow}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#faffb0'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['Длина рукава, см']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    data={this.row(5, i)}
                    textStyle={styles.textRow}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#acddff'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['Двойные размеры']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    data={this.row(6, i)}
                    textStyle={styles.textRow}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#acddff'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['Размеры по Юла']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    data={this.row(7, i)}
                    textStyle={styles.textRow}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#acddff'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
            </Table>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const mainH = 50;
const heightHead = (WIDTH - mainH) / 14;
const oneCol = HEIGHT / 9;
const mainCol = oneCol;
const tableWrapWidthMain = mainCol;
const tableWrapWidth = (HEIGHT - mainCol) / 8;

const styles = {
  head: {
    backgroundColor: '#3888ff',
    height: mainH
  },
  textHead: {
    textAlign: 'center',
    color: '#fff',
    fontFamily: 'qunelas-medium',
    fontSize: HEIGHT / 72
  },
  textMainHead: {
    textAlign: 'center',
    color: '#fff',
    fontFamily: 'qunelas-semi-bold',
    fontSize: HEIGHT / 55
  },
  textSecondHead: {
    textAlign: 'center',
    color: '#fff',
    fontFamily: 'qunelas-semi-bold',
    fontSize: HEIGHT / 71
  },
  textRow: {
    textAlign: 'center',
    color: '#000',
    fontFamily: 'qunelas-semi-bold',
    fontSize: HEIGHT / 71
  },
  title: { height: heightHead, backgroundColor: '#3888ff' },
  row: { height: heightHead },
  text: { textAlign: 'center', color: '#fff', fontFamily: 'qunelas-medium', alignSelf: 'center' },
  tableWrapMain: {
    width: tableWrapWidthMain
  },
  tableWrap: {
    width: tableWrapWidth
  }
};

export default TableSize;
