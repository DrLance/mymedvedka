import React, { Component } from 'react';
import { View, ScrollView, Image, Text, Dimensions } from 'react-native';
import { Table, TableWrapper, Cell } from 'react-native-table-component';

import mainStyles from '../../config/styles';

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

const tableTitle = ['8-10 мес', '9-12 мес', '18-20 мес', '14-19 мес', '24-30 мес'];
const tableTitle2 = ['6-10 мес', '7-11 мес', '16-23 мес', '12-18 мес', '20-31 мес'];

const tableData2 = ['', '', '', '', ''];

const colorCell = ['#ffbaba', '#ffc3d4', '#ffefb7', '#ddffb9', '#faffb0', '#acddff'];

class TableTooth extends Component {
  state = {
    rowData: [
      ['A1,B1', 'A2,B2', 'A3,B3', 'A4,B4', 'A5,B5'],
      ['Центральный резец', 'Боковой резец', 'Клык', 'Первый моляр', 'Второй моляр'],
      ['7-8 лет', '8-9 лет', '11-12 лет', '6-8 лет', '12-13 лет'],
      ['C1,D1', 'C2,D2', 'C3,D3', 'C4,D4', 'C5,D5'],
      ['Центральный резец', 'Боковой резец', 'Клык', 'Первый моляр', 'Второй моляр'],
      ['6-7 лет', '7-8 лет', '9-10 лет', '5-7лет', '11-13 лет']
    ]
  };
  row(col, row) {
    return this.state.rowData[col][row];
  }

  render() {
    return (
      <ScrollView style={{ backgroundColor: 'transparent' }}>
        <View style={{ flexDirection: 'row' }}>
          <View>
            <Text style={[mainStyles.txtEventDescription, { left: '35%' }]}>Верхняя челюсть</Text>
            <View style={{ flexDirection: 'row' }}>
              <Table style={{ flexDirection: 'row' }} borderStyle={{ borderColor: '#332efa', borderWidth: 1.5 }}>
                <TableWrapper style={styles.tableWrapMain}>
                  <Cell data={['Возраст ребенка']} style={styles.head} textStyle={styles.textMainHead} />
                  {tableTitle.map((title, i) => (
                    <Cell key={i} data={title} style={styles.title} textStyle={styles.textHead} />
                  ))}
                </TableWrapper>
                <TableWrapper style={styles.tableWrap}>
                  <Cell data={['Номер зуба']} style={styles.head} textStyle={styles.textSecondHead} />
                  {tableData2.map((title, i) => (
                    <Cell
                      key={i}
                      textStyle={styles.textRow}
                      data={this.row(0, i)}
                      style={[
                        styles.row,
                        {
                          backgroundColor: '#ffbaba'
                        }
                      ]}
                    />
                  ))}
                </TableWrapper>
                <TableWrapper style={styles.tableWrap}>
                  <Cell data={['Название']} style={styles.head} textStyle={styles.textSecondHead} />
                  {tableData2.map((title, i) => (
                    <Cell
                      key={i}
                      data={this.row(1, i)}
                      textStyle={styles.textRow}
                      style={[
                        styles.row,
                        {
                          backgroundColor: '#ffc3d4'
                        }
                      ]}
                    />
                  ))}
                </TableWrapper>
                <TableWrapper style={styles.tableWrap}>
                  <Cell data={['Возраст ребенка']} style={styles.head} textStyle={styles.textSecondHead} />
                  {tableData2.map((title, i) => (
                    <Cell
                      key={i}
                      data={this.row(2, i)}
                      textStyle={styles.textRow}
                      style={[
                        styles.row,
                        {
                          backgroundColor: '#ffefb7'
                        }
                      ]}
                    />
                  ))}
                </TableWrapper>
              </Table>
            </View>
            <Text style={[mainStyles.txtEventDescription, { left: '35%' }]}>Нижняя челюсть</Text>
            <View style={{ flexDirection: 'row' }}>
              <Table style={{ flexDirection: 'row' }} borderStyle={{ borderColor: '#332efa', borderWidth: 1.5 }}>
                <TableWrapper style={styles.tableWrapMain}>
                  <Cell data={['Возраст ребенка']} style={styles.head} textStyle={styles.textMainHead} />
                  {tableTitle2.map((title, i) => (
                    <Cell key={i} data={title} style={styles.title} textStyle={styles.textHead} />
                  ))}
                </TableWrapper>
                <TableWrapper style={styles.tableWrap}>
                  <Cell data={['Номер зуба']} style={styles.head} textStyle={styles.textSecondHead} />
                  {tableData2.map((title, i) => (
                    <Cell
                      key={i}
                      textStyle={styles.textRow}
                      data={this.row(3, i)}
                      style={[
                        styles.row,
                        {
                          backgroundColor: '#ffbaba'
                        }
                      ]}
                    />
                  ))}
                </TableWrapper>
                <TableWrapper style={styles.tableWrap}>
                  <Cell data={['Название']} style={styles.head} textStyle={styles.textSecondHead} />
                  {tableData2.map((title, i) => (
                    <Cell
                      key={i}
                      data={this.row(4, i)}
                      textStyle={styles.textRow}
                      style={[
                        styles.row,
                        {
                          backgroundColor: '#ffc3d4'
                        }
                      ]}
                    />
                  ))}
                </TableWrapper>
                <TableWrapper style={styles.tableWrap}>
                  <Cell data={['Возраст ребенка']} style={styles.head} textStyle={styles.textSecondHead} />
                  {tableData2.map((title, i) => (
                    <Cell
                      key={i}
                      data={this.row(5, i)}
                      textStyle={styles.textRow}
                      style={[
                        styles.row,
                        {
                          backgroundColor: '#ffefb7'
                        }
                      ]}
                    />
                  ))}
                </TableWrapper>
              </Table>
            </View>
          </View>
          <Image
            source={require('../../../assets/images/tooth-table.png')}
            resizeMode="contain"
            style={{ alignSelf: 'center' }}
          />
        </View>
      </ScrollView>
    );
  }
}

const mainH = 40;
const heightHead = (WIDTH - mainH) / 14;
const oneCol = HEIGHT / 7;
const mainCol = oneCol;
const tableWrapWidthMain = mainCol;
const tableWrapWidth = (HEIGHT - mainCol) / 6;

const styles = {
  head: {
    backgroundColor: '#3888ff',
    height: mainH
  },
  textHead: {
    textAlign: 'center',
    color: '#fff',
    fontFamily: 'qunelas-medium',
    fontSize: HEIGHT / 72
  },
  textMainHead: {
    textAlign: 'center',
    color: '#fff',
    fontFamily: 'qunelas-semi-bold',
    fontSize: HEIGHT / 55
  },
  textSecondHead: {
    textAlign: 'center',
    color: '#fff',
    fontFamily: 'qunelas-semi-bold',
    fontSize: HEIGHT / 71
  },
  textRow: {
    textAlign: 'center',
    color: '#000',
    fontFamily: 'qunelas-semi-bold',
    fontSize: HEIGHT / 71
  },
  title: {
    height: heightHead,
    backgroundColor: '#3888ff'
  },
  row: {
    height: heightHead
  },
  text: {
    textAlign: 'center',
    color: '#fff',
    fontFamily: 'qunelas-medium',
    alignSelf: 'center'
  },
  tableWrapMain: {
    width: tableWrapWidthMain
  },
  tableWrap: {
    width: tableWrapWidth
  }
};

export default TableTooth;
