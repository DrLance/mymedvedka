import React, { Component } from 'react';
import { View, ScrollView, Image, TouchableOpacity, Text, Dimensions, TextInput } from 'react-native';
import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component';
import { Icon } from 'native-base';
import mainStyles from '../../config/styles';

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

const tableTitle = ['1-я, 2а\n (здоровые дети)', '2б, 3-я (недоношеные)'];
const tableTitle2 = ['1-я, 2а\n (здоровые дети)', '2б, 3-я (недоношеные)'];
const tableTitle3 = ['Все дети', ''];

const tableData2 = ['', ''];

const colorCell = ['#ffbaba', '#ffc3d4', '#ffefb7', '#ddffb9', '#faffb0', '#acddff'];

class TableVisits extends Component {
  state = {
    rowData: [
      [
        '1-й патронаж - в первые 3 дня, далее 14, 21, 28-30й дни жизни',
        '1-й патронаж - в первые 3 дня, далее ежедневно до 10-го дня, далее по индивидуальной программе реабилитации'
      ],
      ['1 раз в месяц', '2 раза в месяц'],
      ['1 раз в месяц', '2 раза в месяц'],
      ['1 раз в месяц', '2 раза в месяц'],
      ['1 раз в месяц', '1 раз в месяц'],
      ['1 раз в 3 месяца', '1 раз в 3 месяца'],
      ['1 раз в 6 месяцев', '1 раз в 6 месяцев'],
      [
        '1-й патронаж в 1-й день после выписки из роддома, далее ежедневно до 10го дня жизни, далее ежедневно',
        '1-й патронаж - в 1-й день после выписки из роддома, ежедневно до 10го дня жизни, далее 2 раза в неделю'
      ],
      ['2 раза в месяц', '3 раза в месяц'],
      ['2 раза в месяц', '3 раза в месяц'],
      ['2 раза в месяц', '2 раза в месяц'],
      ['1 раз в месяц', '3 раза в месяц'],
      ['1 раз в 3 месяца', '1 раз в 3 месяца'],
      ['1 раз в 6 месяца', '1 раз в 6 месяца'],
      [
        'В роддоме: забор крови на ФКУ, ВГ, АГС, МВ, галактоземию, УЗИ внутренних органов (сердца и др.), НСГ',
        'Ортопед, невролог, детский хирург, офтальмолог, ТБС, НСГ'
      ],
      ['', ''],
      ['Невролог, анализ крови, моли', ''],
      ['В 6 месяцев невролог', ''],
      [
        'В 9 месяцев: детский хирург, детский стоматолог',
        'В 12 месяцев: анализ крови, мочи, ЭКГ, невролог, детский хирург, ортопед, детский стоматолог, офтальмолог, лор-врач'
      ],
      ['Анализ крови, мочи, кала на яйцеглист', ''],
      [
        'Анализ крови, мочи, кала на яйцеглист, невролог, ортопед, детский хирург, лор-врач, детский стоматолог, офтальмолог',
        ''
      ]
    ]
  };
  row(col, row) {
    return this.state.rowData[col][row];
  }

  render() {
    return (
      <ScrollView style={{ backgroundColor: 'transparent' }}>
        <View>
          <Text style={[mainStyles.txtEventDescription, { alignSelf: 'center' }]}>Врач педиатр</Text>
          <View style={{ flexDirection: 'row' }}>
            <Table style={{ flexDirection: 'row' }} borderStyle={{ borderColor: '#332efa', borderWidth: 1.5 }}>
              <TableWrapper style={styles.tableWrapMain}>
                <Cell data={['Группа здоровья']} style={styles.head} textStyle={styles.textMainHead} />
                {tableTitle.map((title, i) => (
                  <Cell key={i} data={title} style={styles.title} textStyle={styles.textHead} />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['1 месяц']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    textStyle={styles.textRow}
                    data={this.row(0, i)}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#ffbaba'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['2 месяц']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    data={this.row(1, i)}
                    textStyle={styles.textRow}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#ffc3d4'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['3 месяц']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    data={this.row(2, i)}
                    textStyle={styles.textRow}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#ffefb7'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['4 - 6 месяц']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    data={this.row(3, i)}
                    textStyle={styles.textRow}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#ddffb9'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['7 - 12 месяц']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    data={this.row(4, i)}
                    textStyle={styles.textRow}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#faffb0'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['2 года']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    data={this.row(5, i)}
                    textStyle={styles.textRow}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#acddff'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['3 года']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    data={this.row(6, i)}
                    textStyle={styles.textRow}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#acddff'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
            </Table>
          </View>
          <Text style={[mainStyles.txtEventDescription, { alignSelf: 'center' }]}>Патронажная медицинская сестра</Text>
          <View style={{ flexDirection: 'row' }}>
            <Table style={{ flexDirection: 'row' }} borderStyle={{ borderColor: '#332efa', borderWidth: 1.5 }}>
              <TableWrapper style={styles.tableWrapMain}>
                <Cell data={['Группа здоровья']} style={styles.head} textStyle={styles.textMainHead} />
                {tableTitle2.map((title, i) => (
                  <Cell key={i} data={title} style={styles.title} textStyle={styles.textHead} />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['1 месяц']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    textStyle={styles.textRow}
                    data={this.row(7, i)}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#ffbaba'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['2 месяц']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    data={this.row(8, i)}
                    textStyle={styles.textRow}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#ffc3d4'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['3 месяц']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    data={this.row(9, i)}
                    textStyle={styles.textRow}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#ffefb7'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['4 - 6 месяц']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    data={this.row(10, i)}
                    textStyle={styles.textRow}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#ddffb9'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['7 - 12 месяц']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    data={this.row(11, i)}
                    textStyle={styles.textRow}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#faffb0'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['2 года']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    data={this.row(12, i)}
                    textStyle={styles.textRow}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#acddff'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['3 года']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    data={this.row(13, i)}
                    textStyle={styles.textRow}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#acddff'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
            </Table>
          </View>
          <Text style={[mainStyles.txtEventDescription, { alignSelf: 'center' }]}>
            Осмотры специалистов и лабораторные, функциональные исследования
          </Text>
          <View style={{ flexDirection: 'row' }}>
            <Table style={{ flexDirection: 'row' }} borderStyle={{ borderColor: '#332efa', borderWidth: 1.5 }}>
              <TableWrapper style={styles.tableWrapMain}>
                <Cell data={['Группа здоровья']} style={styles.head} textStyle={styles.textMainHead} />
                {tableTitle3.map((title, i) => (
                  <Cell key={i} data={title} style={styles.title} textStyle={styles.textHead} />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['1 месяц']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    textStyle={styles.textRow}
                    data={this.row(14, i)}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#ffbaba'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['2 месяц']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    data={this.row(15, i)}
                    textStyle={styles.textRow}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#ffc3d4'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['3 месяц']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    data={this.row(16, i)}
                    textStyle={styles.textRow}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#ffefb7'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['4 - 6 месяц']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    data={this.row(17, i)}
                    textStyle={styles.textRow}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#ddffb9'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['7 - 12 месяц']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    data={this.row(18, i)}
                    textStyle={styles.textRow}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#faffb0'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['2 года']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    data={this.row(19, i)}
                    textStyle={styles.textRow}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#acddff'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['3 года']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    data={this.row(20, i)}
                    textStyle={styles.textRow}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#acddff'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
            </Table>
          </View>
          <Text style={[mainStyles.txtEventDescription, { alignSelf: 'center', textAlign: 'center' }]}>
            Примечание. АГС - адреногенитальный синдром, ВГ - вирусный гепатит, МВ - муковисцидоз, НСГ -
            нейросонография, ТБС - тазобедренный сустав.
          </Text>
        </View>
      </ScrollView>
    );
  }
}

const mainH = 40;
const heightHead = (WIDTH - mainH) / 3;
const oneCol = HEIGHT / 8;
const mainCol = oneCol;
const tableWrapWidthMain = mainCol;
const tableWrapWidth = (HEIGHT - mainCol) / 7;

const styles = {
  head: {
    backgroundColor: '#3888ff',
    height: mainH
  },
  textHead: {
    textAlign: 'center',
    color: '#fff',
    fontFamily: 'qunelas-medium',
    fontSize: HEIGHT / 72
  },
  textMainHead: {
    textAlign: 'center',
    color: '#fff',
    fontFamily: 'qunelas-semi-bold',
    fontSize: HEIGHT / 55
  },
  textSecondHead: {
    textAlign: 'center',
    color: '#fff',
    fontFamily: 'qunelas-semi-bold',
    fontSize: HEIGHT / 71
  },
  textRow: {
    textAlign: 'center',
    color: '#000',
    fontFamily: 'qunelas-semi-bold',
    fontSize: HEIGHT / 71
  },
  title: {
    height: heightHead,
    backgroundColor: '#3888ff'
  },
  row: {
    height: heightHead
  },
  text: {
    textAlign: 'center',
    color: '#fff',
    fontFamily: 'qunelas-medium',
    alignSelf: 'center'
  },
  tableWrapMain: {
    width: tableWrapWidthMain
  },
  tableWrap: {
    width: tableWrapWidth
  }
};

export default TableVisits;
