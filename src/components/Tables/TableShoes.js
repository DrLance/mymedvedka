import React, { Component } from 'react';
import { View, ScrollView, Image, TouchableOpacity, Text, Dimensions, TextInput } from 'react-native';
import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component';
import mainStyles from '../../config/styles';

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

const tableTitle = [
  '0 - 3 месяца',
  '3 - 6 месяца',
  '6 - 12 месяца',
  '12 - 18 месяца',
  '18 - 24 месяца',
  '2 - 2,5 года',
  '2,5 - 3 года',
  '3 - 3,5 года',
  '4 года',
  '4 - 4,5 года',
  '5 лет',
  '6 лет',
  '7 лет'
];

const tableData2 = ['', '', '', '', '', '', '', '', '', '', '', '', ''];

const colorCell = ['#ffbaba', '#ffc3d4', '#ffefb7', '#ddffb9', '#faffb0', '#acddff'];

class TableShoes extends Component {
  state = {
    rowData: [
      [
        '9.5',
        '10.5',
        '11-11.6',
        '12.3',
        '13-13.7',
        '14.3-14.9',
        '15.5',
        '16.2',
        '17.4',
        '18.1',
        '18.7',
        '19.5',
        '20.1'
      ],
      ['16', '17', '18-19', '20', '21-22', '23-24', '25', '26', '28', '29', '30', '31', '32'],
      ['10', '10', '12', '12', '14', '16', '16', '16', '18', '18', '20', '20', '20'],
      ['9.5', '10.5', '11.7', '12.5', '13.4', '14.3', '15.2', '16', '17.5', '18', '18.5', '19.5', '20.3'],
      ['16-17', '17-18', '19', '20', '21-22', '23-24', '25', '26', '27', '28', '29', '30', '31'],
      [
        '0-2',
        '2.5-3.5',
        '4-4.5',
        '5-5.5',
        '6-6.5',
        '7-8.1',
        '8-8.5',
        '9-9.5',
        '10-10.5',
        '11-11.5',
        '12-12.5',
        '13-13.5',
        '1-1.5'
      ],
      ['0.5-1', '1.5-2', '3', '4', '4.5-5.5', '6-7.1', '8', '10', '10.5', '11', '11-12.1', '12.5', '13']
    ],
    summ: ['', '', '', '', '', '', '', '', '', '', '', '']
  };
  row(col, row) {
    return this.state.rowData[col][row];
  }

  renderInput(i) {
    return (
      <TextInput
        style={styles.textRow}
        onChangeText={val => {
          this.onChangeInput(val, i);
        }}
      />
    );
  }

  onChangeInput(val, i) {
    let newSumm = this.state.summ;
    let newVal = parseInt(val, 10) * 1.2;
    newSumm[i] = isNaN(newVal) ? '' : newVal;

    this.setState({ summ: newSumm });
  }

  render() {
    return (
      <ScrollView style={{ backgroundColor: 'transparent' }}>
        <View style={{ flexDirection: 'row' }}>
          <View>
            <Table style={{ flexDirection: 'row', flex: 1 }} borderStyle={{ borderColor: '#332efa', borderWidth: 1 }}>
              <TableWrapper style={styles.tableWrapMain}>
                <Cell data={['Возраст']} style={styles.head} textStyle={styles.textMainHead} />
                {tableTitle.map((title, i) => (
                  <Cell key={i} data={title} style={styles.title} textStyle={styles.textHead} />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['Длина стопы, см']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    textStyle={styles.textRow}
                    data={this.row(0, i)}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#ffbaba'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['Российский р-р']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    data={this.row(1, i)}
                    textStyle={styles.textRow}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#ffc3d4'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['Носки р-р']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    data={this.row(2, i)}
                    textStyle={styles.textRow}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#ffefb7'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['Длинна стопы, см']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    data={this.row(3, i)}
                    textStyle={styles.textRow}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#ddffb9'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['Европ р-р']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    data={this.row(4, i)}
                    textStyle={styles.textRow}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#faffb0'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['США р-р']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    data={this.row(5, i)}
                    textStyle={styles.textRow}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#acddff'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['Английский р-р']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    data={this.row(6, i)}
                    textStyle={styles.textRow}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#acddff'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
            </Table>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const mainH = 50;
const heightHead = (WIDTH - mainH) / 14;
const oneCol = HEIGHT / 8;
const mainCol = oneCol;
const tableWrapWidthMain = mainCol;
const tableWrapWidth = (HEIGHT - mainCol) / 7;

const styles = {
  head: {
    backgroundColor: '#3888ff',
    height: mainH
  },
  textHead: {
    textAlign: 'center',
    color: '#fff',
    fontFamily: 'qunelas-medium',
    fontSize: HEIGHT / 72
  },
  textMainHead: {
    textAlign: 'center',
    color: '#fff',
    fontFamily: 'qunelas-semi-bold',
    fontSize: HEIGHT / 55
  },
  textSecondHead: {
    textAlign: 'center',
    color: '#fff',
    fontFamily: 'qunelas-semi-bold',
    fontSize: HEIGHT / 71
  },
  textRow: {
    textAlign: 'center',
    color: '#000',
    fontFamily: 'qunelas-semi-bold',
    fontSize: HEIGHT / 71
  },
  title: { height: heightHead, backgroundColor: '#3888ff' },
  row: { height: heightHead },
  text: { textAlign: 'center', color: '#fff', fontFamily: 'qunelas-medium', alignSelf: 'center' },
  tableWrapMain: {
    width: tableWrapWidthMain
  },
  tableWrap: {
    width: tableWrapWidth
  }
};

export default TableShoes;
