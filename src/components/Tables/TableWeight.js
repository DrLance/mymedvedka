import React, { Component } from 'react';
import { View, ScrollView, Dimensions, TextInput } from 'react-native';
import { Table, TableWrapper, Cell } from 'react-native-table-component';
import mainStyles from '../../config/styles';
import { tableData2, tableTitle, rowData } from '../../data/dataWeight';

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

const colorCell = ['#ffbaba', '#ffc3d4', '#ffefb7', '#ddffb9', '#faffb0', '#acddff'];

class TableWeight extends Component {
  row(col, row) {
    return rowData[col][row];
  }

  renderInput(i) {
    return (
      <TextInput
        style={styles.textRow}
        onChangeText={val => {
          this.onChangeInput(val, i);
        }}
      />
    );
  }

  render() {
    return (
      <ScrollView style={{ backgroundColor: 'transparent' }}>
        <View style={{ flexDirection: 'row' }}>
          <View>
            <Table style={{ flexDirection: 'row', flex: 1 }} borderStyle={{ borderColor: '#332efa', borderWidth: 1 }}>
              <TableWrapper style={styles.tableWrapMain}>
                <Cell data={['Возраст']} style={styles.head} textStyle={styles.textMainHead} />
                {tableTitle.map((title, i) => (
                  <Cell key={i} data={title} style={styles.title} textStyle={styles.textHead} />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['Диапазон нормы вес(гр)']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    textStyle={styles.textRow}
                    data={this.row(0, i)}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#ffbaba'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['Прибавка за месяц(гр)']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    data={this.row(1, i)}
                    textStyle={styles.textRow}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#ffc3d4'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['Диапазон нормы рост(см)']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    data={this.row(2, i)}
                    textStyle={styles.textRow}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#ffefb7'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['Прибавка за месяц(см)']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    data={this.row(3, i)}
                    textStyle={styles.textRow}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#ddffb9'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['Окружность головы(см)']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    data={this.row(4, i)}
                    textStyle={styles.textRow}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#faffb0'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
              <TableWrapper style={styles.tableWrap}>
                <Cell data={['Окружность груди(см)']} style={styles.head} textStyle={styles.textSecondHead} />
                {tableData2.map((title, i) => (
                  <Cell
                    key={i}
                    data={this.row(5, i)}
                    textStyle={styles.textRow}
                    style={[
                      styles.row,
                      {
                        backgroundColor: '#acddff'
                      }
                    ]}
                  />
                ))}
              </TableWrapper>
            </Table>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const mainH = 50;
const heightHead = (WIDTH - mainH) / 14;
const oneCol = HEIGHT / 7;
const mainCol = oneCol;
const tableWrapWidthMain = mainCol;
const tableWrapWidth = (HEIGHT - mainCol) / 6;

const styles = {
  head: {
    backgroundColor: '#3888ff',
    height: mainH
  },
  textHead: {
    textAlign: 'center',
    color: '#fff',
    fontFamily: 'qunelas-medium',
    fontSize: HEIGHT / 72
  },
  textMainHead: {
    textAlign: 'center',
    color: '#fff',
    fontFamily: 'qunelas-semi-bold',
    fontSize: HEIGHT / 55
  },
  textSecondHead: {
    textAlign: 'center',
    color: '#fff',
    fontFamily: 'qunelas-semi-bold',
    fontSize: HEIGHT / 71
  },
  textRow: {
    textAlign: 'center',
    color: '#000',
    fontFamily: 'qunelas-semi-bold',
    fontSize: HEIGHT / 71
  },
  title: { height: heightHead, backgroundColor: '#3888ff' },
  row: { height: heightHead },
  text: { textAlign: 'center', color: '#fff', fontFamily: 'qunelas-medium', alignSelf: 'center' },
  tableWrapMain: {
    width: tableWrapWidthMain
  },
  tableWrap: {
    width: tableWrapWidth
  }
};

export default TableWeight;
