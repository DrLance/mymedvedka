import React, { Component } from 'react';
import { View, Modal, Text, Dimensions, TextInput } from 'react-native';
import { Table, TableWrapper, Cell } from 'react-native-table-component';
import { Button, Container, Content } from 'native-base';
import _ from 'lodash';
import mainStyles from '../../config/styles';
import dataPee from '../../data/dataBlood';

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

const tableTitle = [
  'Гемоглобин Hb,г/л',
  'Эритроциты RBC/л',
  'Цвет. показатеель МСНС,%',
  'Ретикулоциты RTC,%',
  'Тромбоциты PLT/л',
  'СОЭ ESR  мм/час',
  'Лейкоциты WBC/л',
  'Палочкоядерные %',
  'Сегментоядерные %',
  'Эозинофилы EOS %',
  'Базофилы BAS %',
  'Лимфоциты LYM %',
  'Моноциты MON %'
];

const tableData2 = ['', '', '', '', '', '', '', '', '', '', '', '', ''];

type Props = {
  kidInfo: Object
};

class TableBlood extends Component<Props> {
  state = {
    modalVisible: false,
    selectedIndex: null,
    rowData: [
      [
        '180-240',
        '4,3-7,6',
        '0,85-1,15',
        '3-51',
        '180-490',
        '2-4',
        '8,5-24,5',
        '1-17',
        '45-80',
        '0,5-6',
        '0-1',
        '12-36',
        '2-12'
      ],
      [
        '115-175',
        '3,8-5,6',
        '0,85-1,15',
        '3-15',
        '180-400',
        '4-8',
        '6,5-13,8',
        '0,5-4',
        '15-45',
        '0,5-7',
        '0-1',
        '40-76',
        '2-12'
      ],
      [
        '110-140',
        '3,5-4,8',
        '0,85-1,15',
        '3-15',
        '180-400',
        '4-10',
        '5,5-12,5',
        '0,5-4',
        '15-45',
        '0,5-7',
        '0-1',
        '42-74',
        '2-12'
      ],
      [
        '110-135',
        '3,6-4,9',
        '0,85-1,15',
        '3-15',
        '180-400',
        '4-12',
        '6-12',
        '0,5-4',
        '15-45',
        '0,5-7',
        '0-1',
        '38-72',
        '2-12'
      ],
      [
        '110-140',
        '3,5-4,5',
        '0,85-1,15',
        '3-12',
        '160-390',
        '4-12',
        '5-12',
        '0,5-5',
        '25-60',
        '0,5-7',
        '0-1',
        '26-60',
        '2-10'
      ],
      [
        '110-145',
        '3,5-4,7',
        '0,85-1,15',
        '3-12',
        '160-380',
        '4-12',
        '5,5-10',
        '0,5-5',
        '35-65',
        '0,5-7',
        '0-1',
        '24-54',
        '2-10'
      ]
    ],
    summ: ['', '', '', '', '', '', '', '', '', '', '', '']
  };

  onChangeInput(val, i) {
    if (!this.props.kidInfo) {
      return;
    }

    const newSumm = this.state.summ;
    const { kidInfo } = this.props;

    let indexYear = 0;

    if (kidInfo.age >= 1) {
      indexYear = 1;
    }
    if (kidInfo.age >= 6) {
      indexYear = 2;
    }
    if (kidInfo.age >= 12) {
      indexYear = 3;
    }
    if (kidInfo.age > 12 && kidInfo.age <= 72) {
      indexYear = 4;
    }
    if (kidInfo.age >= 84 && kidInfo.age <= 144) {
      indexYear = 5;
    }

    const tableValue = this.state.rowData[indexYear][i].split('-');
    let minValue = 0;
    let maxValue = 0;

    if (tableValue.length > 1) {
      minValue = parseFloat(tableValue[0].replace(',', '.'));
      maxValue = parseFloat(tableValue[1].replace(',', '.'));
    }

    if (val && (val < minValue || val > maxValue)) {
      newSumm[i] = '1';
    } else {
      newSumm[i] = '';
    }

    this.setState({ summ: newSumm });
  }

  onPressInfo = index => {
    this.setState({ modalVisible: true, selectedIndex: index });
  };

  closeModal = () => {
    this.setState({ modalVisible: false });
  };

  row(col, row) {
    return this.state.rowData[col][row];
  }

  renderInput(i) {
    return (
      <TextInput
        style={styles.textRow}
        onChangeText={val => {
          this.onChangeInput(val, i);
        }}
      />
    );
  }

  renderItogo = index => {
    return (
      <Button
        block
        transparent
        backgroundColor={this.state.summ[index] ? 'red' : null}
        style={{ alignSelf: 'center', alignItems: 'center', width: '100%' }}
        onPress={() => this.onPressInfo(index)}
      >
        <Text style={styles.textRow}>Посмотреть</Text>
      </Button>
    );
  };

  renderInfoText = () => {
    const txtObj = _.find(dataPee, el => {
      if (el.index === this.state.selectedIndex) {
        return el;
      }
      return null;
    });

    if (txtObj) {
      return (
        <View>
          <Text style={mainStyles.headerBack}>{txtObj.secondHeader}</Text>
          <Text style={mainStyles.txtEventDescription}>{txtObj.description}</Text>
        </View>
      );
    }
    return null;
  };

  render() {
    return (
      <Container>
        <Modal visible={this.state.modalVisible} animationType="slide" onRequestClose={() => this.closeModal()}>
          <Content padder contentContainerStyle={{ justifyContent: 'center', flex: 1, alignItems: 'center' }}>
            {this.renderInfoText()}
            <Button
              transparent
              block
              onPress={this.closeModal}
              style={{ alignSelf: 'center', marginTop: 20, width: '30%' }}
            >
              <Text style={mainStyles.txtAddChild}>Закрыть</Text>
            </Button>
          </Content>
        </Modal>
        <Content>
          <Table style={{ flexDirection: 'row', flex: 1 }} borderStyle={{ borderColor: '#332efa', borderWidth: 1 }}>
            <TableWrapper style={styles.tableWrapMain}>
              <Cell data={['Показатели']} style={styles.head} textStyle={styles.textMainHead} />
              {tableTitle.map((title, i) => (
                <Cell key={i} data={title} style={styles.title} textStyle={styles.textHead} />
              ))}
            </TableWrapper>
            <TableWrapper style={styles.tableWrap}>
              <Cell data={['1 день']} style={styles.head} textStyle={styles.textSecondHead} />
              {tableData2.map((title, i) => (
                <Cell
                  key={i}
                  textStyle={styles.textRow}
                  data={this.row(0, i)}
                  style={[
                    styles.row,
                    {
                      backgroundColor: '#ffbaba'
                    }
                  ]}
                />
              ))}
            </TableWrapper>
            <TableWrapper style={styles.tableWrap}>
              <Cell data={['1 мес']} style={styles.head} textStyle={styles.textSecondHead} />
              {tableData2.map((title, i) => (
                <Cell
                  key={i}
                  data={this.row(1, i)}
                  textStyle={styles.textRow}
                  style={[
                    styles.row,
                    {
                      backgroundColor: '#ffbaba'
                    }
                  ]}
                />
              ))}
            </TableWrapper>
            <TableWrapper style={styles.tableWrap}>
              <Cell data={['6 мес']} style={styles.head} textStyle={styles.textSecondHead} />
              {tableData2.map((title, i) => (
                <Cell
                  key={i}
                  data={this.row(2, i)}
                  textStyle={styles.textRow}
                  style={[
                    styles.row,
                    {
                      backgroundColor: '#ffbaba'
                    }
                  ]}
                />
              ))}
            </TableWrapper>
            <TableWrapper style={styles.tableWrap}>
              <Cell data={['12 мес']} style={styles.head} textStyle={styles.textSecondHead} />
              {tableData2.map((title, i) => (
                <Cell
                  key={i}
                  data={this.row(3, i)}
                  textStyle={styles.textRow}
                  style={[
                    styles.row,
                    {
                      backgroundColor: '#ffbaba'
                    }
                  ]}
                />
              ))}
            </TableWrapper>
            <TableWrapper style={styles.tableWrap}>
              <Cell data={['1-6 лет']} style={styles.head} textStyle={styles.textSecondHead} />
              {tableData2.map((title, i) => (
                <Cell
                  key={i}
                  data={this.row(4, i)}
                  textStyle={styles.textRow}
                  style={[
                    styles.row,
                    {
                      backgroundColor: '#ffbaba'
                    }
                  ]}
                />
              ))}
            </TableWrapper>
            <TableWrapper style={styles.tableWrap}>
              <Cell data={['7-12 лет']} style={styles.head} textStyle={styles.textSecondHead} />
              {tableData2.map((title, i) => (
                <Cell
                  key={i}
                  data={this.row(5, i)}
                  textStyle={styles.textRow}
                  style={[
                    styles.row,
                    {
                      backgroundColor: '#ffbaba'
                    }
                  ]}
                />
              ))}
            </TableWrapper>
            <TableWrapper style={styles.tableWrap}>
              <Cell data={['Ваши параметры']} style={styles.head} textStyle={styles.textSecondHead} />
              {tableData2.map((title, i) => (
                <Cell
                  key={i}
                  data={this.renderInput(i)}
                  textStyle={styles.textRow}
                  style={[
                    styles.row,
                    {
                      backgroundColor: '#acddff'
                    }
                  ]}
                />
              ))}
            </TableWrapper>
            <TableWrapper style={styles.tableWrap}>
              <Cell data={['Итого']} style={styles.head} textStyle={styles.textSecondHead} />
              {tableData2.map((title, i) => (
                <Cell
                  key={i}
                  data={this.renderItogo(i)}
                  textStyle={styles.textRow}
                  style={[
                    styles.row,
                    {
                      backgroundColor: '#acddff'
                    }
                  ]}
                />
              ))}
            </TableWrapper>
          </Table>
        </Content>
      </Container>
    );
  }
}

const mainH = 50;
const heightHead = (WIDTH - mainH) / 14;
const oneCol = HEIGHT / 9;
const mainCol = oneCol * 2;
const tableWrapWidthMain = mainCol;
const tableWrapWidth = (HEIGHT - mainCol) / 8;

const styles = {
  head: {
    backgroundColor: '#3888ff',
    height: mainH
  },
  textHead: {
    textAlign: 'center',
    color: '#fff',
    fontFamily: 'qunelas-medium',
    fontSize: HEIGHT / 72
  },
  textMainHead: {
    textAlign: 'center',
    color: '#fff',
    fontFamily: 'qunelas-semi-bold',
    fontSize: HEIGHT / 55
  },
  textSecondHead: {
    textAlign: 'center',
    color: '#fff',
    fontFamily: 'qunelas-semi-bold',
    fontSize: HEIGHT / 71
  },
  textRow: {
    textAlign: 'center',
    color: '#000',
    fontFamily: 'qunelas-semi-bold',
    fontSize: HEIGHT / 71
  },
  title: { height: heightHead, backgroundColor: '#3888ff' },
  row: { height: heightHead },
  text: { textAlign: 'center', color: '#fff', fontFamily: 'qunelas-medium', alignSelf: 'center' },
  tableWrapMain: {
    width: tableWrapWidthMain
  },
  tableWrap: {
    width: tableWrapWidth
  }
};

export default TableBlood;
