/* eslint-disable global-require */
import React, { PureComponent } from 'react';
import { View, Text, FlatList, Image, TouchableOpacity } from 'react-native';
import mainStyles from '../config/styles';
import { formatShowDate } from '../utils/util';

const typeArr = [
  require('../../assets/images/add.png'),
  require('../../assets/images/edit.png'),
  require('../../assets/images/checked_green.png'),
  require('../../assets/images/photo-camera-list.png'),
  require('../../assets/images/stethoscope.png'),
  require('../../assets/images/urine.png'),
  require('../../assets/images/blood-sample.png'),
  require('../../assets/images/food.png'),
  require('../../assets/images/sleep.png'),
  require('../../assets/images/tooth.png'),
  require('../../assets/images/size.png'),
  require('../../assets/images/foots.png'),
  require('../../assets/images/weigth.png'),
  require('../../assets/images/question.png')
];

type Props = {
  data: Array,
  onPress: Function,
  header: String
};

class EventList extends PureComponent<Props> {
  checkDate = date => {
    const result = formatShowDate(date, 'DD.MM.YYYY, h:mm');

    if (result) {
      return result;
    }

    return date;
  };

  limitText = text => {
    if (text.length <= 35) {
      return text;
    }

    return `${text.substring(0, 35)}...`;
  };
  render() {
    return (
      <View style={{ flex: 1 }}>
        <FlatList
          contentContainerStyle={styles.containerContent}
          showsVerticalScrollIndicator={false}
          keyExtractor={item => item.id.toString()}
          data={this.props.data}
          renderItem={({ item }) => (
            <TouchableOpacity onPress={this.props.onPress ? () => this.props.onPress(item) : null}>
              <View style={styles.containerList}>
                <Image
                  source={typeArr[item.type_event - 1]}
                  style={{ marginLeft: 15, width: 24, height: 24, marginRight: 20 }}
                />
                <View style={styles.containerItem}>
                  {this.props.header ? (
                    <Text style={mainStyles.txtEventHeader}>{this.checkDate(item.start_date)}</Text>
                  ) : null}
                  <Text style={[mainStyles.txtEventDescription, { marginTop: 5 }]}>{this.limitText(item.name)}</Text>
                </View>
              </View>
            </TouchableOpacity>
          )}
        />
      </View>
    );
  }
}
const styles = {
  container: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'column',
    justifyContent: 'center'
  },
  containerList: {
    borderBottomWidth: 1,
    borderBottomColor: '#dbdbdb',
    flex: 1,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    height: 62
  },
  containerContent: {
    marginHorizontal: 10,
    paddingVertical: '10%',
    justifyContent: 'center',
    alignItems: 'stretch'
  },
  containerItem: {
    flexDirection: 'column',
    marginLeft: 10,
    justifyContent: 'flex-start'
  },
  txt: {
    color: '#4c5864',
    fontFamily: 'qunelas-semi-bold'
  }
};
export default EventList;
