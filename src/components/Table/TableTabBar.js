/* eslint-disable global-require */
import React, { PureComponent } from 'react';
import { View, Text, Image, Dimensions, TouchableOpacity } from 'react-native';

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

class TableTabBar extends PureComponent {
  render() {
    return (
      <View
        style={{
          backgroundColor: '#fbfbfb',
          width: HEIGHT / 10.8,
          justifyContent: 'space-around'
        }}
      >
        <TouchableOpacity onPress={() => this.props.navigation.navigate('calendar')}>
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Image
              style={{ tintColor: '#9f9f9f', width: 21.5, height: 23.5 }}
              source={require('../../../assets/images/school-calendar.png')}
            />
            <Text
              style={{
                fontFamily: 'qunelas-medium',
                color: '#9f9f9f',
                fontSize: HEIGHT / 80
              }}
            >
              Календарь
            </Text>
          </View>
        </TouchableOpacity>
        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
          <Image
            style={{ tintColor: '#4cbafc', width: 24, height: 24 }}
            source={require('../../../assets/images/insert-table.png')}
          />
          <Text
            style={{
              fontFamily: 'qunelas-medium',
              color: '#4cbafc',
              fontSize: HEIGHT / 80,
              textAlign: 'center'
            }}
          >
            График прививок
          </Text>
        </View>
        <TouchableOpacity onPress={() => this.props.navigation.navigate('normal')}>
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Image
              style={{ tintColor: '#9f9f9f', width: 18.5, height: 21 }}
              source={require('../../../assets/images/prescription.png')}
            />
            <Text
              style={{
                fontFamily: 'qunelas-medium',
                color: '#9f9f9f',
                fontSize: HEIGHT / 80,
                textAlign: 'center'
              }}
            >
              Нормы и анализы
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => this.props.navigation.navigate('medical')}>
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Image
              style={{ tintColor: '#9f9f9f', width: 25, height: 19.5 }}
              source={require('../../../assets/images/photo-camera.png')}
            />
            <Text
              style={{
                fontFamily: 'qunelas-medium',
                color: '#9f9f9f',
                fontSize: HEIGHT / 80
              }}
            >
              Мед. карта
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

export default TableTabBar;
