import React, { PureComponent } from 'react';
import { View, TouchableOpacity, Image, Text } from 'react-native';
import mainStyles from '../../config/styles';

class TableItem extends PureComponent {
  rederItem() {
    return (
      <View style={styles.container}>
        <Image
          style={{
            marginHorizontal: 5,
            tintColor: this.props.done ? '#54e350' : '#fd804f',
            width: this.props.done ? 10.5 : 10,
            height: this.props.done ? 8 : 10
          }}
          source={
            this.props.done
              ? require('../../../assets/images/checked.png')
              : require('../../../assets/images/letter-x.png')
          }
          resizeMethod="scale"
        />
        <View style={styles.containerTxt}>
          {this.props.txtHead ? <Text style={mainStyles.txtTableItemHead}>{this.props.txtHead}</Text> : null}
          <Text style={mainStyles.txtTableItemDescr}>{this.props.txtDescription}</Text>
        </View>
      </View>
    );
  }

  render() {
    return <TouchableOpacity>{this.rederItem()}</TouchableOpacity>;
  }
}

const styles = {
  container: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#fff',
    height: '100%'
  },
  containerTxt: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  }
};

export default TableItem;
