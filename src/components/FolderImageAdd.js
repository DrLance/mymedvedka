/* eslint-disable global-require */
import React, { PureComponent } from 'react';
import { TouchableOpacity, Text, ImageBackground, Image } from 'react-native';
import mainStyles from '../config/styles';

class FolderImageAdd extends PureComponent {
  render() {
    return (
      <TouchableOpacity style={styles.itemPhoto} onPress={this.props.onPress}>
        <ImageBackground
          source={require('../../assets/images/folder-icon_dis.png')}
          style={{
            width: 96,
            height: 75.5,
            justifyContent: 'center',
            marginBottom: 10,
            alignItems: 'center'
          }}
          resizeMode="cover"
        >
          <Image
            source={require('../../assets/images/plus-symbol.png')}
            style={{ height: 23.5, width: 23, marginTop: 10 }}
          />
        </ImageBackground>
        <Text style={[mainStyles.txtFolderHeader, { textAlign: 'center' }]}>Добавить папку</Text>
      </TouchableOpacity>
    );
  }
}

const styles = {
  itemPhoto: {
    marginBottom: 11,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  }
};

export default FolderImageAdd;
