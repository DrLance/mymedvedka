import React, { PureComponent } from 'react';
import { View, Image, Dimensions, TouchableOpacity } from 'react-native';

const WIDTH_SCREEN = Dimensions.get('screen').width;

const imgHeight = 39; //0.24 * WIDTH_SCREEN;
const imgWidth = 51; //0.24 * WIDTH_SCREEN;
const hw = WIDTH_SCREEN / 3.5;

class MedicalPhoto extends PureComponent {
  state = {};
  render() {
    return (
      <TouchableOpacity onPress={this.props.onPress}>
        <View style={[styles.imgContainer, this.props.style]}>
          <Image
            style={{
              height: imgHeight,
              width: imgWidth
            }}
            resizeMode="stretch"
            source={this.props.image}
          />
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = {
  imgContainer: {
    borderWidth: 2,
    height: hw,
    width: hw,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#e7f1ff',
    borderRadius: 4
  }
};

export default MedicalPhoto;
