import gql from 'graphql-tag';

const addEvent = gql`
  mutation AddEvent(
    $user_id: ID
    $kid_id: ID!
    $start_date: String
    $end_date: String
    $name: String
    $description: String
    $type_event: String
    $isActive: Boolean
  ) {
    addEvent(
      user_id: $user_id
      kid_id: $kid_id
      start_date: $start_date
      end_date: $end_date
      name: $name
      description: $description
      type_event: $type_event
      isActive: $isActive
    ) {
      id
    }
  }
`;

const delEvent = gql`
  mutation DelEvent($id: ID!) {
    delEvent(id: $id) {
      id
    }
  }
`;

const updateEvent = gql`
  mutation UpdateEvent($event_id: ID!, $start_date: String) {
    updateEvent(event_id: $event_id, start_date: $start_date) {
      id
    }
  }
`;

export { addEvent, delEvent, updateEvent };
