import gql from 'graphql-tag';

export default gql`
  mutation Register($email: String, $password: String, $name: String, $dateBirth: String) {
    register(email: $email, password: $password, name: $name, dateBirth: $dateBirth) {
      id
      email
      kids {
        id
      }
    }
  }
`;
