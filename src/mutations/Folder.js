import gql from 'graphql-tag';

const addFolder = gql`
  mutation AddFolder($user_id: ID!, $name: String, $description: String) {
    addFolder(user_id: $user_id, name: $name, description: $description) {
      id
    }
  }
`;

const delFolder = gql`
  mutation DelFolder($id: ID!) {
    delFolder(id: $id) {
      id
    }
  }
`;

const renameFolder = gql`
  mutation RenameFolder($folder_id: ID!, $name: String) {
    renameFolder(folder_id: $folder_id, name: $name) {
      id
      name
    }
  }
`;

const uploadFile = gql`
  mutation UploadFile($folder_id: ID!, $file: Upload) {
    uploadFile(folder_id: $folder_id, file: $file) {
      id
    }
  }
`;

const uploadFile2 = gql`
  mutation UploadFile2($file: Upload!) {
    uploadFile2(file: $file) {
      id
    }
  }
`;

export { addFolder, delFolder, uploadFile, uploadFile2, renameFolder };
