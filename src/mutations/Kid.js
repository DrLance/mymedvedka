import gql from 'graphql-tag';

const addKid = gql`
  mutation AddKid($user_id: ID!, $name: String!, $weight: String, $growth: String, $date_birth: String!) {
    addKid(user_id: $user_id, name: $name, weight: $weight, growth: $growth, date_birth: $date_birth) {
      id
      date_birth
    }
  }
`;

const changeActiveKid = gql`
  mutation ChangeActiveKid($id: ID!, $isActive: Boolean) {
    changeActiveKid(id: $id, isActive: $isActive) {
      id
    }
  }
`;

const removeKid = gql`
  mutation RemoveKid($id: ID!) {
    removeKid(id: $id) {
      id
    }
  }
`;

export { addKid, changeActiveKid, removeKid };
