/* eslint-disable global-require */
import React from 'react';
import { ApolloClient } from 'apollo-client';
import { ApolloLink } from 'apollo-link';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloProvider } from 'react-apollo';
import { createUploadLink } from 'apollo-upload-client';
import { Font } from 'expo';

import Routes from './src/routes';

const prodHost = 'http://188.225.56.67:4000/graphql';

const uploadLink = createUploadLink({ uri: prodHost });

/*
Object.setPrototypeOf =
  Object.setPrototypeOf ||
  function(obj, proto) {
    obj.__proto__ = proto;
    return obj;
  };

console.disableYellowBox = true;
*/

const link = ApolloLink.from([uploadLink]);

const client = new ApolloClient({
  link,
  cache: new InMemoryCache(),
  onError: (e, { graphQLErrors, networkError }) => {
    if (graphQLErrors) {
      graphQLErrors.map(({ message, locations, path }) => console.log(`[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`));
    }
    if (networkError) console.log(`[Network error]: ${networkError}`);
  }
});

class App extends React.Component {
  state = { fontLoaded: false };

  async componentDidMount() {
    await Font.loadAsync({
      qunelas: require('./assets/fonts/Qanelas-Black.otf'),
      'qunelas-bold': require('./assets/fonts/Qanelas-Bold.otf'),
      'qunelas-semi-bold': require('./assets/fonts/Qanelas-SemiBold.otf'),
      'qunelas-medium': require('./assets/fonts/Qanelas-Medium.otf'),
      'qunelas-thin': require('./assets/fonts/Qanelas-Thin.otf')
    });
    this.setState({ fontLoaded: true });
  }

  render() {
    return this.state.fontLoaded ? (
      <ApolloProvider client={client}>
        <Routes />
      </ApolloProvider>
    ) : null;
  }
}

export default App;
